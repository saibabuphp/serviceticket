<!--Right sidebar component -->
					<div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

						<!-- Sidebar content -->
						<div class="sidebar-content"> 

							<!-- Search -->
							<!-- <div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">Search ticket</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body">
									<form action="#">
										<div class="form-group-feedback form-group-feedback-right">
											<input type="search" class="form-control" placeholder="type and hit Enter">
											<div class="form-control-feedback">
												<i class="icon-search4 font-size-base text-muted"></i>
											</div>
										</div>
									</form>
								</div> 
							</div> -->
							<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
								<span class="text-uppercase font-size-sm font-weight-semibold">Create Form</span>
								<div class="header-elements">
									<div class="list-icons">
								         <a class="list-icons-item" data-action="collapse"></a>
								    </div>
								</div>
								</div>

								<div class="card-body p-0">
									<div class="nav nav-sidebar mb-2">
									<!--<li class="nav-item-header font-size-xs text-uppercase">Development</li>-->
									 
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/create_employee';?>" class="nav-link"> <i class="icon-user-plus"></i> Create Employee </a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/create_client';?>" class="nav-link"> <i class="icon-googleplus5"></i> Create Client </a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/create_project';?>" class="nav-link"> <i class="icon-folder-plus4"></i> Create Project </a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/create_roles';?>" class="nav-link"> <i class="icon-eye-plus"></i> Create User Roles </a>
									</li>
								</div>
							</div>
						</div>
						<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
								<span class="text-uppercase font-size-sm font-weight-semibold">Basic Details</span>
								<div class="header-elements">
									<div class="list-icons">
								         <a class="list-icons-item" data-action="collapse"></a>
								    </div>
								</div>
								</div>

								<div class="card-body p-0">
									<div class="nav nav-sidebar mb-2"> 
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/Aview_employee';?>" class="nav-link"><i class="icon-user-tie"></i> Employee
											<span class="badge badge-primary badge-pill ml-auto"><?php echo $this->session->userdata('a_emps_count'); ?></span>
										</a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/Aview_clients';?>" class="nav-link"><i class="icon-users4"></i>Clients
										<span class="badge badge-primary badge-pill ml-auto"><?php echo $this->session->userdata('a_clients_count'); ?></span> </a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/Aview_projects';?>" class="nav-link"><i class="icon-briefcase3"></i> Projects
										<span class="badge badge-primary badge-pill ml-auto"><?php echo $this->session->userdata('a_projectcs_count'); ?></span> </a>
									</li>
									
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/Aview_roles';?>" class="nav-link"> <i class="icon-user-lock"></i> User Roles 
										<span class="badge badge-primary badge-pill ml-auto"><?php echo $this->session->userdata('a_userrole_count'); ?></span> </a>
									</li>
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/Aview_prority';?>" class="nav-link"> <i class="icon-equalizer"></i>Prority List
										<span class="badge badge-primary badge-pill ml-auto"><?php echo $this->session->userdata('a_priority_count'); ?></span></a>
									</li> 
									<li class="nav-item">
										<a href="<?php echo base_url().'Admin/Admin_bankprofile';?>" class="nav-link"> <i class="icon-cash3"></i>Company Bank details
										<span class="badge badge-primary badge-pill ml-auto"><?php echo $this->session->userdata('a_compbank_info_count'); ?></span></a>
									</li> 
									</div>
								</div>
							</div>
							<!-- /search -->
							<!-- Completeness stats -->
							<!-- calcilations for display prograss chat in rightside start -->
							<?php
							 /*echo $this->session->userdata('a_total_tickets_count'); 
							 echo $this->session->userdata('a_shedule_tickets_count'); */
							 //prograss bar for prograss tickets
							 $pb_Progress=round((100*$this->session->userdata('a_shedule_tickets_count'))/$this->session->userdata('a_total_tickets_count'));

							 $pb_resolved=round((100*$this->session->userdata('a_resolved_count'))/$this->session->userdata('a_total_tickets_count'));
							 $pb_closed=round((100*$this->session->userdata('a_closed_tickets_count'))/$this->session->userdata('a_total_tickets_count'));
							?>
							<!-- calcilations for display prograss chat ends -->
							
					 	<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="text-uppercase font-size-sm font-weight-semibold">Completeness stats </span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body">
									<ul class="list-unstyled mb-0">
							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Admin/scheduled';?>"  >
							                <div class="d-flex align-items-center mb-1">Progress <span class="text-muted ml-auto"><?php echo $pb_Progress  ?>%</span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-grey-400" style="width: <?php echo $pb_Progress  ?>%">
													<span class="sr-only"><?php echo $pb_Progress  ?>% Complete</span>
												</div>
											</div></a>
							            </li>

							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Admin/resolved';?>"  >
							                <div class="d-flex align-items-center mb-1">Resolved tickets <span class="text-muted ml-auto"><?php echo $pb_resolved."%" ?></span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-warning-400" style="width: <?php echo $pb_resolved.'%' ?>">
													<span class="sr-only"><?php echo $pb_resolved."%" ?> Complete</span>
												</div>
											</div>
										</a>
							            </li>

							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Admin/closed';?>"  >
							                <div class="d-flex align-items-center mb-1">Closed tickets <span class="text-muted ml-auto"><?php echo $pb_closed."%" ?></span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-success-400" style="width: <?php echo $pb_closed.'%' ?>">
													<span class="sr-only"><?php echo $pb_closed.'%' ?> Complete</span>
												</div>
											</div></a>
							            </li>

							            
							        </ul>
								</div>
							</div> 
							<!-- /completeness stats -->

 

						</div>
						<!-- /sidebar content -->

					</div>
					<!-- /right sidebar component
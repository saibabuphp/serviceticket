<?php $this->load->view('Client/Headm_links_validations'); ?>
 <style>

  .error{
  display: none;
  margin-left: 10px;
}   

.error_show{
  color: red;
  margin-left: 10px;
}


input.invalid, textarea.invalid{
  border: 2px solid red; 
}

input.valid, textarea.valid{
  border: 2px solid green;
}
 </style>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
							<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Create New Employee</h5>
					</div>

					<div class="card-body">
						<form method="post" action="<?php echo base_url().'Admin/inst_emp'; ?>" enctype="multipart/form-data">
							<fieldset class="mb-3">
								<div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Employee Role <?php /*var_dump($roles_list);exit();*/ ?></label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" id="emp_role" name="emp_role" required>
			                                <option value="">Select Role</option>
                                            <?php 
                                                foreach($roles_list as $row)
                                                {
                                                    echo "<option value='$row->id'>".ucfirst($row->user_name)."</option>";        
                                                }
                                            ?>
			                                
			                            </select>
			                            <span style="color: red" id="emp_role_alert"></span>
		                            </div>
		                        </div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-2">Name</label>
									<div class="col-lg-10"> 
										<input type="text" class="form-control" name="emp_name" id="emp_name" placeholder="Enter employee name" value="" required>
										<span style="color: red" id="emp_name_alert"></span>
									</div>
								</div>
								<!-- <div class="form-group row">
									<label class="col-form-label col-lg-2">password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" value="" name="emp_pwd" id="emp_pwd" placeholder="Name" required>
										<input type="checkbox" onclick="myFunction()">Show Password
									</div>
								</div> -->

                                <div class="form-group row">
									<label class="col-form-label col-lg-2">Email</label>
									<div class="col-lg-10">
										
										<input type="text" class="form-control" name="emp_email" placeholder="Email" id="emp_email" value="" required >
										<span style="color: red" id="emp_email_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Gender</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" name="emp_gender" id="emp_gender" required>
			                                <option value="">Select Gender</option>
			                                <option value="1">Male</option>
			                                <option value="2">Female</option>
			                                <option value="3">Transgender</option>
			                            </select>
			                            <span style="color: red" id="emp_gender_alert"></span>
		                            </div>
		                        </div>
                                
                                <div class="form-group row">
									<label class="col-form-label col-lg-2" required>Phone Number</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" name="emp_phone" id="emp_phone"  value="" placeholder="Phone Number">
										 <span style="color: red" id="emp_phone_alert"></span>
									</div>
								</div>
                                 <!-- <div class="form-group row">
									<label class="col-form-label col-lg-2" required>Date of Birth</label>
									<div class="col-lg-10">
										<div class="input-group">
										<span class="input-group-prepend">
											<span class="input-group-text"><i class="icon-calendar22"></i></span>
										</span>
										<input type="text" class="form-control daterange-single" name="emp_dob">
									</div>
									</div>
								</div> -->
                                 <div class="form-group row">
									<label class="col-form-label col-lg-2" required>Address</label>
									<div class="col-lg-10">
										<textarea rows="3" cols="3" class="form-control" name="emp_addr" id="emp_addr" value="" placeholder="Enter your message here"></textarea>
										<span style="color: red" id="emp_addr_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-2" required>Employee Pic</label>
									<div class="col-lg-10">
										<input type="file" class="form-control h-auto" name="emp_pic" id="emp_pic" value="">
										<span style="color: red" id="emp_pic_alert"></span>
										<span style="color: red" id="semp_pic_alert"></span>
									</div>
								</div>
    					    </fieldset>

							<div class="text-right">
								<button type="submit" id ="submit" name="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
	

	
						<!-- Pagination -->
						<!--<div class="d-flex justify-content-center mt-3 mb-3">
							<ul class="pagination">
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
							</ul>
						</div>-->
						<!-- /pagination -->

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<script>
 
function myFunction() {
  var x = document.getElementById("emp_pwd");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
$(document).ready(function(){
    $("#emp_role").change(function(){
    var select = $("#emp_role option:selected").val();  
      validemp_role();  
});   });  
function validemp_role() 
{
 var emp_role = $("#emp_role");   
       if(emp_role.val() == "" || emp_role.val() == null || emp_role.val() == "Select Role")
       {    emp_role.focus();
          $("#emp_role_alert").html("emp role field reqired");
          return false; 
     }
     else { $("#emp_role_alert").html(""); }
}
/*$(document).ready(function(){  
    $("#emp_role").keyup(function(){
      return validatetext('emp_role','emp_role_alert'); 
});   });*/

$(document).ready(function(){  
    $("#emp_name").keyup(function(){
      return validatetext('emp_name','emp_name_alert'); 
});   });
$(document).ready(function(){  
    $("#emp_email").keyup(function(){
      return validemailid('emp_email','emp_email_alert'); 
});   });
$(document).ready(function(){  
    $("#emp_phone").keyup(function(){
      return validmobile('emp_phone','emp_phone_alert'); 
});   });

/*$(document).ready(function(){  
    $("#emp_gender").keyup(function(){
      return validatetext('emp_gender','emp_gender_alert'); 
});   });*/
$(document).ready(function(){
    $("#emp_gender").change(function(){
    var select = $("#emp_gender option:selected").val();  
      validemp_gender();  
});   });  
function validemp_gender() 
{
 var emp_gender = $("#emp_gender");   
       if(emp_gender.val() == "" || emp_gender.val() == null || emp_gender.val() == "Select Role")
       {    emp_gender.focus();
          $("#emp_gender_alert").html("emp role field reqired");
          return false; 
     }
     else { $("#emp_gender_alert").html(""); }
}

$(document).ready(function(){  
    $("#emp_addr").keyup(function(){
      return validatetext('emp_addr','emp_addr_alert'); 
});   });

$(document).ready(function(){
 $(document).on('change', '#emp_pic', function(){
  var name = document.getElementById("emp_pic").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase(); 
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
    $("#emp_pic_alert").html("Invalid Image format File"); 
  } else   {    $("#emp_pic_alert").html("");  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("emp_pic").files[0]);
  var f = document.getElementById("emp_pic").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 200000)
  {
    $("#semp_pic_alert").html("File Size is big"); 
  }
  else   {    $("#semp_pic_alert").html("");  }
 });
}); 

$(document).ready(function(){ 
    $("#submit").click(function(){

      var emp_role    = validatetext('emp_role','emp_role_alert');
      var emp_name     = validatetext('emp_name','emp_name_alert');
      var emp_email    = validemailid('emp_email','emp_email_alert');
      var emp_phone    =validmobile('emp_phone','emp_phone_alert');
      var emp_gender   = validatetext('emp_gender','emp_gender_alert');
      var emp_addr = validatetext('emp_addr','emp_addr_alert');
      //var path     = validatetext('path','path_alert');
      var emp_pic    = validate_fileupload('emp_pic','emp_pic_alert'); 

      if(emp_role == 0 || emp_name == 0 || emp_email == 0|| emp_phone == 0 || emp_gender == 0 || emp_pic == 0 || emp_addr == 0 )
      {
        return false;
      }

      });  
  });    

</script>

			<!-- /content area -->
<?php $this->load->view('Home/Footerm'); ?>
 

			
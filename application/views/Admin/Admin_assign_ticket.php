 <?php $this->load->view('Client/Headm_links_validations'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper"> 
 
			<!-- Page header --> 
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div> 

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div> 
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
	               <div class="card">
							<div class="card-header header-elements-md-inline">
								<h3 class="card-title text-primary">Client Name</h3><br>
                                
								<!--<div class="header-elements">
									<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right">Check in <b><i class="icon-alarm-check"></i></b></a>
			                	</div>-->
							</div>

							<div class="card-body"> 
								<div class="row container-fluid"> 
									<div class="col-md-6">
										<dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Project Name</dt>
				                            <dd><?php echo $assignsingleticketdet[0]['c_url_link']; ?></dd>				                               
				                        </dl>
										<div class="card">
											<div class="card-header bg-transparent header-elements-inline">
												<span class="card-title font-weight-semibold">Control panel Details</span>
												<div class="header-elements">
													<div class="list-icons">
								                		<a class="list-icons-item" data-action="collapse"></a>
							                		</div>
						                		</div>
											</div>
											<div class="card-body">
												<form action="#">
													<div class="form-group-feedback form-group-feedback-right">
														<dl>
				                                	<dt class="font-size-sm text-primary text-uppercase">File Manager Details</dt>	
				                                	<dd><?php echo $assignsingleticketdet[0]['url_config_file_uid']; ?></dd>
				                                	<dd><?php echo $assignsingleticketdet[0]['url_config_file_pwd']; ?></dd>
				                                
				                                	<dt class="font-size-sm text-primary text-uppercase">Database Details</dt>	
				                                	<dd><?php echo $assignsingleticketdet[0]['url_config_db_uid']; ?></dd>
				                                	<dd><?php echo $assignsingleticketdet[0]['url_config_db_pwd']; ?></dd>
				                                
				                            		</dl>
													</div>
												</form>
											</div>
										</div> 
									</div> 
									<div class="col-md-6"> 
											<dl>
				                                <dt class="font-size-sm text-primary text-uppercase">Image Information</dt>
				                                <dd><img src="<?php echo base_url(); ?>assets/images/ticket/<?php echo $assignsingleticketdet[0]['ticket_pics']; ?>" target="_blank" height="300" width="300"></dd>
				                            </dl>
			                        </div> 
			                        <div class="col-md-12"> 
			                        	<h3 class="card-title text-primary"><?php echo $assignsingleticketdet[0]['ticket_sub']; ?></h3>
				                        <h6 class="font-weight-semibold">Descritption</h6>
								<p class="mb-3"><?php echo $assignsingleticketdet[0]['ticket_discription']; ?></p>
								<h6 class="font-weight-semibold">Problem raised Path</h6>
								<p class="mb-4"><?php echo $assignsingleticketdet[0]['url_link']; ?></p> 
			                        </div> 
			                    </div>

                                
                <div class="card"> 
					<div class="card-body"> 
						<form action="<?php echo base_url().'Admin/assigningtoemp'; ?>" method="post">
							<fieldset class="mb-3">
							   <legend class="text-uppercase font-size-sm font-weight-bold">Assign to developer</legend>
							   
		                         
                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-4">Developer</label>
		                        	<div class="col-lg-8">
			                            <select class="form-control" name="emp_id" id="emp_id" required>
			                                <option value="">Select Developer</option>
			                                <option value="">Select Developer</option>
			                                <option value="1">ON HOLD</option>
                                            <?php 
                                                foreach($emp as $row)
                                                    {?>
			                                <option value="<?php echo $row['emp_id']; ?>"><?php echo $row['emp_name']; ?></option>
                                            <?php    }
                                            ?>
			                            </select>
			                            <span style="color: red" id="emp_id_alert"></span>
		                            </div>
		                        </div>
                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-4">Admin Priority</label>
		                        	<div class="col-lg-8">
			                            <select class="form-control" name="admin_priority" id="admin_priority" required>
			                                <option value="">Select Admin Priority</option>
                                            <?php 
                                                foreach($priority_list as $row)
                                                    {?>
			                                <option value="<?php echo $row['id']; ?>"><?php echo $row['priority_type']; ?></option>
                                            <?php    }
                                            ?>
			                            </select>
			                            <span style="color: red" id="admin_priority_alert"></span>
		                            </div>
		                        </div>
                                <input type="hidden" class="form-control" name="tic_id" id="clickable-label" value="<?php echo $assignsingleticketdet[0]['ticket_id']; ?>" required>
                                
								<div class="form-group row">
									<label class="col-form-label col-lg-4 cursor-pointer" for="clickable-label">Estimated Time (Min)</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" name="est_time" id="est_time" value="" placeholder="Time In Minutes Only" required>
										<span style="color: red" id="est_time_alert"></span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-4">Message</label>
									<div class="col-lg-8">
									<textarea rows="3" cols="3" name="admin_msg" id="admin_msg" class="form-control" placeholder="Message"></textarea>
									<span style="color: red" id="admin_msg_alert"></span>
									</div>
								</div>

                                 
                                
							
							</fieldset>
                            <div class="text-right">
								<button type="submit" id="a_assign_ticket_to_emp_submit" name="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
                                
	       		</div>

						 
						</div>
	 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts --> 
				
			</div>
			<!-- /content area -->
			<script type="text/javascript">
	$(document).ready(function(){
    $("#emp_id").change(function(){
    var select = $("#emp_id option:selected").val();  
      validemp_id();  
		});   });  
	$(document).ready(function(){  
    $("#admin_msg").keyup(function(){
    return validatetext('admin_msg','admin_msg_alert'); 
	});   });
	function validemp_id() 
	{
 		var emp_id = $("#emp_id");   
       if(emp_id.val() == "" ||emp_id.val() == null || emp_id.val() == "Select Developer")
       {    emp_id.focus();
          $("#emp_id_alert").html("Developer field reqired");
          return false; 
     	}
     	else { $("#emp_id_alert").html(""); }
	}
	$(document).ready(function(){
	    $("#admin_priority").change(function(){
	    var select = $("#admin_priority option:selected").val();  
	      validadmin_priority();  
	});   });  
	
	function validadmin_priority() 
	{
 	var admin_priority = $("#admin_priority");   
       if(admin_priority.val() == "" ||admin_priority.val() == null || admin_priority.val() == "Select Admin Priority")
       {    
       	  admin_priority.focus();
          $("#admin_priority_alert").html("Admin Priority field reqired");
          return false; 
     	}
     	else { $("#admin_priority_alert").html(""); }
	}
	
	$(document).ready(function(){  
    $("#est_time").keyup(function(){
    return validatenumber('est_time','est_time_alert'); 
	});   });
	
	$(document).ready(function(){ 
    $("#a_assign_ticket_to_emp_submit").click(function(){ 
      var emp_id    		= validatetext('emp_id','emp_id_alert');
      var admin_priority    = validatetext('admin_priority','admin_priority_alert');
      var est_time   = validatenumber('est_time','est_time_alert');     
      var admin_msg = validatetext('admin_msg','admin_msg_alert'); 
      if(emp_id == 0 || admin_priority == 0 || clickable_label == 0 || admin_msg == 0)
      {
        return false;
      }

      });  
  });    

</script>			
<?php $this->load->view('Home/Footerm'); ?>
 

			
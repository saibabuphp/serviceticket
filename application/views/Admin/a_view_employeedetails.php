<?php $this->load->view('Home/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						
				<!-- Rounded thumbs -->
				 
				<div class="row">
					 
 				<?php //var_dump($employeedetails) ;exit();
                        if(!empty($employeedetails)){ foreach($employeedetails as $det){ ?> 
			 

					<div class="col-xl-3 col-sm-6">
						<div class="card">
							<div class="card-body text-center">
								<div class="card-img-actions d-inline-block mb-3">
									<img class="img-fluid rounded-circle" src="<?php echo base_url().'assets/images/emp/'.$det['emp_pic'];?>" width="170" height="170" alt="">
									<div class="card-img-actions-overlay card-img rounded-circle">
										
										<a href="<?php echo base_url().'Admin/A_empinfoview/'.$det['emp_id'];?>" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
											<i class="icon-pencil"></i>
										</a>
									</div>
								</div>

					    		<h6 class="font-weight-semibold mb-0"><?php echo $det['emp_name']; ?></h6>
					    		<span class="d-block text-muted"><?php echo $det['user_name']; ?> </span>
					    		<ul class="list-inline list-inline-condensed mt-3 mb-0">
									<li class="list-inline-item"><a href="<?php echo base_url().'Admin/admin_emp_prfile_details_view_deactivate/'.$det['emp_id']; ?>" class="btn btn-outline bg-info btn-icon text-info border-info border-2 rounded-round">
									<i class="icon-eraser2"></i> Deactivate</a></li>
								</ul>
 

				    			 
					    	</div>
				    	</div>
					</div>
				<?php }}
				?>









				</div>
				


				
				<!-- /rounded thumbs-->
						 



			 

	
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
		</div></div>
			<!-- /content area -->
<?php $this->load->view('Home/Footerm'); ?>
 

			
 

			
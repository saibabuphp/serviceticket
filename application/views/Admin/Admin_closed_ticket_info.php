 <?php $this->load->view('Client/Headm_links_validations'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper"> 
 
			<!-- Page header --> 
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div> 

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
	               <div class="card">
							<div class="card-header header-elements-md-inline">
								<h3 class="card-title text-primary">Client Name</h3><br>
                                
								<!--<div class="header-elements">
									<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right">Check in <b><i class="icon-alarm-check"></i></b></a>
			                	</div>-->
							</div>

							<div class="card-body"> 
								<div class="row container-fluid"> 
									<div class="col-md-6">
										<dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Project Name</dt>
				                            <dd><?php echo $assignsingleticketdet['c_url_link']; ?></dd>				                               
				                        </dl>
										<div class="card">
											<div class="card-header bg-transparent header-elements-inline">
												<span class="card-title font-weight-semibold">Control panel Details</span>
												<div class="header-elements">
													<div class="list-icons">
								                		<a class="list-icons-item" data-action="collapse"></a>
							                		</div>
						                		</div>
											</div>
											<div class="card-body">
												<form action="#">
													<div class="form-group-feedback form-group-feedback-right">
														<dl>
				                                	<dt class="font-size-sm text-primary text-uppercase">File Manager Details</dt>	
				                                	<dd><?php echo $assignsingleticketdet['url_config_file_uid']; ?></dd>
				                                	<dd><?php echo $assignsingleticketdet['url_config_file_pwd']; ?></dd>
				                                
				                                	<dt class="font-size-sm text-primary text-uppercase">Database Details</dt>	
				                                	<dd><?php echo $assignsingleticketdet['url_config_db_uid']; ?></dd>
				                                	<dd><?php echo $assignsingleticketdet['url_config_db_pwd']; ?></dd>
				                                
				                            		</dl>
													</div>
												</form>
											</div>
										</div> 
									</div> 
									<div class="col-md-6"> 
											<dl>
				                                <dt class="font-size-sm text-primary text-uppercase">Image Information</dt>
				                                <dd><img src="<?php echo base_url(); ?>assets/images/ticket/<?php echo $assignsingleticketdet['ticket_pics']; ?>" target="_blank" height="300" width="300"></dd>
				                            </dl>
			                        </div> 
			                        <div class="col-md-12"> 
			                        	<h6 class="font-weight-semibold">Subject</h6>
			                        	<p class="card-title text-primary"><?php echo $assignsingleticketdet['ticket_sub']; ?></p>
				                        <h6 class="font-weight-semibold">Descritption</h6>
								<p class="mb-3"><?php echo $assignsingleticketdet['ticket_discription']; ?></p>
								<h6 class="font-weight-semibold">Problem raised Path</h6>
								<p class="mb-4"><?php echo $assignsingleticketdet['url_link']; ?></p>
								<h6 class="font-weight-semibold">Assigned Emp Name</h6>
								<p class="mb-4"><?php echo ucfirst($assignsingleticketdet['emp_name']); ?></p>
								<h6 class="font-weight-semibold">Admin Estimed Time</h6>
								<p class="mb-4"><?php echo $assignsingleticketdet['admin_time']." MIN"; ?></p> 
								<h6 class="font-weight-semibold">Emp Estimed Time</h6>
								<p class="mb-4"><?php echo ucfirst($assignsingleticketdet['emp_estimate_time']." MIN"); ?></p>
								<h6 class="font-weight-semibold">Emp Completed Time</h6>
								<p class="mb-4"><?php echo $assignsingleticketdet['emp_cmp_time']." MIN"; ?></p>
			                        </div> 
			                    </div>               
	       		</div>

							<!--<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
								<span class="d-flex align-items-center">
									<span class="badge badge-mark border-blue mr-2"></span>
									Status:
									<div class="dropdown ml-2">
										<a href="#" class="text-default font-weight-semibold dropdown-toggle" data-toggle="dropdown">Open</a>
										<div class="dropdown-menu">
											<a href="#" class="dropdown-item active">Open</a>
											<a href="#" class="dropdown-item">On hold</a>
											<a href="#" class="dropdown-item">Resolved</a>
											<a href="#" class="dropdown-item">Closed</a>
											<div class="dropdown-divider"></div>
											<a href="#" class="dropdown-item">Dublicate</a>
											<a href="#" class="dropdown-item">Invalid</a>
											<a href="#" class="dropdown-item">Wontfix</a>
										</div>
									</div>
								</span>

								<ul class="list-inline list-inline-condensed mb-0 mt-2 mt-sm-0">
									<li class="list-inline-item">
										<a href="#" class="text-default"><i class="icon-compose"></i></a>
									</li>
									<li class="list-inline-item">
										<a href="#" class="text-default"><i class="icon-trash"></i></a>
									</li>
									<li class="list-inline-item dropdown">
										<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-grid-alt"></i></a>

										<div class="dropdown-menu dropdown-menu-right">
											<a href="#" class="dropdown-item"><i class="icon-alarm-add"></i> Check in</a>
											<a href="#" class="dropdown-item"><i class="icon-attachment"></i> Attach screenshot</a>
											<a href="#" class="dropdown-item"><i class="icon-user-plus"></i> Assign users</a>
											<a href="#" class="dropdown-item"><i class="icon-warning2"></i> Report</a>
										</div>
									</li>
								</ul>
							</div>-->
						</div>
	 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
			<script type="text/javascript">
	$(document).ready(function(){
    $("#emp_id").change(function(){
    var select = $("#emp_id option:selected").val();  
      validemp_id();  
});   });  
	function validemp_id() 
{
 var emp_id = $("#emp_id");   
       if(emp_id.val() == "" ||emp_id.val() == null || emp_id.val() == "Select Developer")
       {    emp_id.focus();
          $("#emp_id_alert").html("Developer field reqired");
          return false; 
     }
     else { $("#emp_id_alert").html(""); }
}
$(document).ready(function(){
    $("#admin_priority").change(function(){
    var select = $("#admin_priority option:selected").val();  
      validadmin_priority();  
});   });  
	
	function validadmin_priority() 
	{
 	var admin_priority = $("#admin_priority");   
       if(admin_priority.val() == "" ||admin_priority.val() == null || admin_priority.val() == "Select Admin Priority")
       {    
       	  admin_priority.focus();
          $("#admin_priority_alert").html("Admin Priority field reqired");
          return false; 
     	}
     	else { $("#admin_priority_alert").html(""); }
	}
	
	$(document).ready(function(){  
    $("#est_time").keyup(function(){
    return validatenumber('est_time','est_time_alert'); 
	});   });
	
	$(document).ready(function(){ 
    $("#a_assign_ticket_to_emp_submit").click(function(){ 
      var emp_id    		= validatetext('emp_id','emp_id_alert');
      var admin_priority    = validatetext('admin_priority','admin_priority_alert');
      var est_time   = validatenumber('est_time','est_time_alert');     

      if(emp_id == 0 || admin_priority == 0 || clickable_label == 0)
      {
        return false;
      }

      });  
  });    

</script>			
<?php $this->load->view('Home/Footerm'); ?>
 

			
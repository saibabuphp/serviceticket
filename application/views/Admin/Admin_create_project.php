<?php $this->load->view('Client/Headm_links_validations'); ?>
 
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
							<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Create New Project</h5>
					</div>

					<div class="card-body">
						<form method="post" action="<?php echo base_url().'Admin/inst_project'; ?>" enctype="multipart/form-data">
							<fieldset class="mb-3">
                                <div class="form-group row">
		                        	<label class="col-form-label col-lg-3">Client Name</label>
		                        	<div class="col-lg-9">
			                            <select class="form-control" name="clint_id" id="clint_id">
			                                <option value="">Select Client Name</option>
                                            <?php 
                                                foreach($clint_list->result() as $row)
                                                {
                                                    echo "<option value='$row->clint_uid'>".ucfirst($row->clint_company_name)."</option>";
                                                }
                                            ?>
			                                
			                            </select>
			                            <span style="color: red" id="clint_id_alert"></span>
		                            </div>
		                        </div>
                                 <div class="form-group row">
									<label class="col-form-label col-lg-3">Project URL</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="proj_url" id="proj_url" value=""  placeholder="Project URL">
										<span style="color: red" id="proj_url_alert"></span>
									</div>
								</div>
								<fieldset  class="mb-3">
                                    <legend class="text-uppercase font-size-sm font-weight-bold">Server control panel details</legend>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">File User Id</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="file_id" id="file_id" value="" placeholder="File User Id">
										<span style="color: red" id="file_id_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">File Password</label>
									<div class="col-lg-9">
										<input type="password" class="form-control" name="file_pass" 
										id="file_pass" value="" placeholder="File Password">
										
										<input type="checkbox" onclick="file_pwdshowhide()">Show Password
										<span style="color: red" id="file_pass_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">DB User Name</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="db_name" id="db_name" value="" placeholder="DB User Name">
										<span style="color: red" id="db_name_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">DB Password</label>
									<div class="col-lg-9">
										<input type="password" class="form-control" name="db_pass" id="db_pass" value="" placeholder="DB Password">
										<input type="checkbox" onclick="db_pwdshowhide()">Show Password
										<span style="color: red" id="db_pass_alert"></span>
									</div>
								</div>
                                 </fieldset>
                                <fieldset  class="mb-3">
                                    <legend class="text-uppercase font-size-sm font-weight-bold">Local Head Contact Details</legend>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Name</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="lh_name" id="lh_name" value="" placeholder="Local Head Name">
										<span style="color: red" id="lh_name_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Email</label>
									<div class="col-lg-9">
										<input type="email" class="form-control" name="lh_email" id="lh_email" value="" placeholder="Local Head Email">
										<span style="color: red" id="lh_email_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Phone Number</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="lh_phone" id="lh_phone" value="" placeholder="Local Head Phone Number">
										<span style="color: red" id="lh_phone_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Address</label>
									<div class="col-lg-9">
										<textarea rows="3" cols="3" class="form-control" name="lh_addr"  
									id="lh_addr" value="" 	placeholder="Local Head Address"></textarea>
										<span style="color: red" id="lh_addr_alert"></span>
									</div>
								</div>
                               
    					    </fieldset>

							<div class="text-right">
								<button type="submit" name="submit" class="btn btn-primary" id="submit">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
	

	
						<!-- Pagination -->
						<!--<div class="d-flex justify-content-center mt-3 mb-3">
							<ul class="pagination">
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
							</ul>
						</div>-->
						<!-- /pagination -->

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
			<script type="text/javascript">
	$(document).ready(function(){
    $("#clint_id").change(function(){
    var select = $("#clint_id option:selected").val();  
      validclint_id();  
});   });  
	function validclint_id() 
{
 var clint_id = $("#clint_id");   
       if(clint_id.val() == "" ||clint_id.val() == null || clint_id.val() == "Client Name")
       {    clint_id.focus();
          $("#clint_id_alert").html("clint_id field reqired");
          return false; 
     }
     else { $("#clint_id_alert").html(""); }
}
$(document).ready(function(){
    $("#lh_addr").change(function(){
    var select = $("#lh_addr option:selected").val();  
      validlh_addr();  
});   });  
function validlh_addr() 
{
 var lh_addr = $("#lh_addr");   
       if(lh_addr.val() == "" ||lh_addr.val() == null || lh_addr.val() == "Address")
       {    lh_addr.focus();
          $("#lh_addr_alert").html("lh_addr field reqired");
          return false; 
     }
     else { $("#lh_addr_alert").html(""); }
}
/* $(document).ready(function(){
 	var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
  $("#proj_url").keyup(function(){
     if($("#proj_url").val() == "" || $("#proj_url").val()==null)
        {   
            $("#proj_url").focus();
            $("#proj_url_alert").html("Please enter proper URL...");
            n++;
        }
        else if(!regExp.test($("#path").val())){
            $("#proj_url").focus();
            $("#proj_url_alert").html("Please enter proper URL...");
            k++;
        }else{
            $("#proj_url_alert").html("");
        } 
  });
}); */
function file_pwdshowhide() {
  var x = document.getElementById("file_pass");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function db_pwdshowhide() {
  var x = document.getElementById("db_pass");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}

 
$(document).ready(function(){  
    $("#file_id").keyup(function(){
      return validatetext('file_id','file_id_alert'); 
});   });
$(document).ready(function(){  
    $("#file_pass").keyup(function(){
      return validatetext('file_pass','file_pass_alert'); 
});   });
$(document).ready(function(){  
    $("#db_name").keyup(function(){
      return validatetext('db_name','db_name_alert'); 
});   });
$(document).ready(function(){  
    $("#db_pass").keyup(function(){
      return validatetext('db_pass','db_pass_alert'); 
});   });
$(document).ready(function(){  
    $("#lh_name").keyup(function(){
      return validatetext('lh_name','lh_name_alert'); 
});   });
$(document).ready(function(){  
    $("#lh_email").keyup(function(){
      return validemailid('lh_email','lh_email_alert'); 
});   });
$(document).ready(function(){  
    $("#lh_phone").keyup(function(){
      return validmobile('lh_phone','lh_phone_alert'); 
});   });

$(document).ready(function(){ 
    $("#submit").click(function(){ 
      var  clint_id    = validatetext('clint_id','clint_id_alert');
      var lh_addr     = validatetext('lh_addr','lh_addr_alert');
      var file_id     = validatetext('file_id','file_id_alert');
       var file_pass     = validatetext('file_pass','file_pass_alert');
        var db_name     = validatetext('db_name','db_name_alert');
         var db_pass     = validatetext('db_pass','db_pass_alert');
          var lh_name     = validatetext('lh_name','lh_name_alert');
      var lh_email   = validemailid('lh_email','lh_email_alert');
      var lh_phone    =validmobile('lh_phone','lh_phone_alert');
     

      if(clint_id == 0 || lh_addr == 0 || proj_url == 0||  file_id==0 || file_pass == 0 || db_name == 0 || db_pass == 0 ||lh_name == 0 || lh_email== 0 || lh_phone == 0 )
      {
        return false;
      }

      });  
  });    


</script>			
<?php $this->load->view('Home/Footerm'); ?>
 

			
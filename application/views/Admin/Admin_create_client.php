<?php $this->load->view('Client/Headm_links_validations'); ?>
 <style>

  .error{
  display: none;
  margin-left: 10px;
}   

.error_show{
  color: red;
  margin-left: 10px;
}


input.invalid, textarea.invalid{
  border: 2px solid red; 
}
 
input.valid, textarea.valid{
  border: 2px solid green;
}
 </style>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
							<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Create New Client</h5>
					</div>

					<div class="card-body">
						<form method="post" action="<?php echo base_url().'Admin/inst_client'; ?>" enctype="multipart/form-data">
							<fieldset class="mb-3">
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Company Name</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" value="" id="comp_name" name="comp_name" placeholder="Company Name" required>
										<span style="color: red" id="comp_name_alert"></span>
									</div>
								</div>
                             <!--    <div class="form-group row">
									<label class="col-form-label col-lg-3">Password</label>
									<div class="col-lg-9">
										<input type="Password" class="form-control" name="pwd" placeholder="Password" required>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Re-Password</label>
									<div class="col-lg-9">
										<input type="Password" class="form-control" name="repwd" placeholder="Re-Password" required>
									</div>
								</div> -->
                                 <div class="form-group row">
									<label class="col-form-label col-lg-3">Company Address</label>
									<div class="col-lg-9">
                                        <textarea rows="3" cols="3" class="form-control" id="comp_addr"  value=""   name="comp_addr" placeholder="Enter your message here" required></textarea>
                                         <span style="color: red" id="comp_addr_alert"></span>
									</div>
								</div>
                                
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Company Logo</label>
									<div class="col-lg-9">
										<input type="file" class="form-control h-auto" id="comp_logo" value="" name="comp_logo" required>
										<span style="color: red" id="comp_logo_alert"></span>
										<span style="color: red" id="scomp_logo_alert"></span>
									</div>
								</div>
    					    </fieldset>
                            <fieldset class="mb-3">
								<legend class="text-uppercase font-size-sm font-weight-bold">Company Head Details</legend>
                                           <div class="form-group row">
									<label class="col-form-label col-lg-3">Name</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" id="comp_h_name" value="" name="comp_h_name" placeholder="Name" required>
										<span style="color: red" id="comp_h_name_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Email</label>
									<div class="col-lg-9">
										<input type="email" class="form-control" id="comp_h_email" value=""  name="comp_h_email" placeholder="Email" required>
										<span style="color: red" id="comp_h_email_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Phone Number</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" id="comp_h_phone" value="" name="comp_h_phone" placeholder="Phone Number" required>
										<span style="color: red" id="comp_h_phone_alert"></span>
									</div>
								</div>
                                                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Photo</label>
									<div class="col-lg-9">
										<input type="file" class="form-control h-auto" name="comp_h_pic" 
										id="comp_h_pic" value="" required>
										<span style="color: red" id="comp_h_pic_alert"></span>
										<span style="color: red" id="scomp_h_pic_alert"></span>
									</div>

								</div>
                             </fieldset>
                             <fieldset class="mb-3">
								<legend class="text-uppercase font-size-sm font-weight-bold">Company Contact Person Details</legend>
                                 <div class="form-group row">
									<label class="col-form-label col-lg-3">Name</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="comp_cont_pers_name" 
										id="comp_cont_pers_name" value=""  placeholder="Name" required>
										<span style="color: red" id="comp_cont_pers_name_alert"></span>
									</div>
								</div>
                                 <div class="form-group row">
									<label class="col-form-label col-lg-3">Email</label>
									<div class="col-lg-9">
										<input type="email" class="form-control" name="compy_cont_per_email"   
										id="compy_cont_per_email" value=""    placeholder="Email" required>
										<span style="color: red" id="compy_cont_per_email_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-3">Phone</label>
									<div class="col-lg-9">
										<input type="text" class="form-control" name="compy_cont_person_phone"  id="compy_cont_person_phone" value=""  placeholder="Number" required>
										<span style="color: red" id="compy_cont_person_phone_alert"></span>
									</div>
								</div>
                              
                             </fieldset>

							<div class="text-right">
								<button type="submit" name="submit"  id="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
	

	
						<!-- Pagination -->
						<!--<div class="d-flex justify-content-center mt-3 mb-3">
							<ul class="pagination">
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
							</ul>
						</div>-->
						<!-- /pagination -->

					</div>
					<!-- /left content -->

<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
			<script type="text/javascript">

	/*$(document).ready(function(){  
    $("#comp_addr").keyup(function(){
      return validatetext('comp_addr','comp_addr_alert'); 

});   }); */
$(document).ready(function(){
    $("#comp_addr").change(function(){
    var select = $("#comp_addr option:selected").val();  
      validcomp_addr();  
});   });  
	function validcomp_addr() 
{
 var comp_addr = $("#comp_addr");   
       if(comp_addr.val() == "" ||comp_addr.val() == null || comp_addr.val() == "Company Address")
       {    comp_addr.focus();
          $("#comp_addr_alert").html("comp_addr field reqired");
          return false; 
     }
     else { $("#comp_addr_alert").html(""); }
}
	
	$(document).ready(function(){  
    $("#comp_name").keyup(function(){
      return validatetext('comp_name','comp_name_alert'); 
});   });
	$(document).ready(function(){  
    $("#comp_h_name").keyup(function(){
      return validatetext('comp_h_name','comp_h_name_alert'); 
});   });
$(document).ready(function(){  
    $("#comp_h_email").keyup(function(){
      return validemailid('comp_h_email','comp_h_email_alert'); 
});   });
$(document).ready(function(){  
    $("#comp_cont_pers_name").keyup(function(){
      return validatetext('comp_cont_pers_name','comp_cont_pers_name_alert'); 
});   });
$(document).ready(function(){  
    $("#compy_cont_per_email").keyup(function(){
      return validemailid('compy_cont_per_email','compy_cont_per_email_alert'); 
});   });
$(document).ready(function(){  
    $("#compy_cont_person_phone").keyup(function(){
      return validmobile('compy_cont_person_phone','compy_cont_person_phone_alert'); 
});   });

				$(document).ready(function(){
 $(document).on('change', '#comp_logo', function(){
  var name = document.getElementById("comp_logo").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase(); 
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
    $("#comp_logo_alert").html("Invalid Image format File"); 
  } else   {    $("#comp_logo_alert").html("");  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("comp_logo").files[0]);
  var f = document.getElementById("comp_logo").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 200000)
  {
    $("#scomp_logo_alert").html("File Size is big"); 
  }
  else   {    $("#scomp_logo_alert").html("");  }
 });
}); 


				$(document).ready(function(){
 $(document).on('change', '#comp_h_pic', function(){
  var name = document.getElementById("comp_h_pic").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase(); 
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
    $("#comp_h_pic_alert").html("Invalid Image format File"); 
  } else   {    $("#comp_h_pic_alert").html("");  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("comp_h_pic").files[0]);
  var f = document.getElementById("comp_h_pic").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 200000)
  {
    $("#scomp_h_pic_alert").html("File Size is big"); 
  }
  else   {    $("#scomp_h_pic_alert").html("");  }
 });
}); 
$(document).ready(function(){ 
    $("#submit").click(function(){ 
      var comp_addr    = validatetext('comp_addr','comp_addr_alert');
      var comp_name     = validatetext('comp_name','comp_name_alert');
      var comp_h_name     = validatetext('comp_h_name','comp_h_name_alert');
      var comp_h_email    = validemailid('comp_h_email','comp_h_email_alert');
      var comp_h_phone    =validmobile('comp_h_phone','comp_h_phone_alert');
      var comp_cont_pers_name   = validatetext('comp_cont_pers_name','comp_cont_pers_name_alert');
      var compy_cont_per_email    = validemailid('compy_cont_per_email','compy_cont_per_email_alert');
      var compy_cont_person_phone    =validmobile('compy_cont_person_phone','compy_cont_person_phone_alert');
      //var path     = validatetext('path','path_alert');
      var comp_logo   = validate_fileupload('comp_logo','comp_logo_alert'); 
        var comp_h_pic   = validate_fileupload('comp_h_pic','comp_h_pic_alert'); 

      if(comp_addr == 0 || comp_name == 0 || comp_h_name == 0|| comp_h_email == 0 || comp_h_phone == 0 || comp_cont_pers_name == 0 ||compy_cont_per_email == 0 || compy_cont_person_phone== 0 || comp_logo == 0 ||  comp_h_pic == 0)
      {
        return false;
      }

      });  
  });    




</script>
<?php $this->load->view('Home/Footerm'); ?>
 

			
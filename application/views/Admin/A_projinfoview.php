<?php $this->load->view('Client/Headm_links_validations'); ?>
 <style>

  .error{
  display: none;
  margin-left: 10px;
}   

.error_show{
  color: red;
  margin-left: 10px;
}


input.invalid, textarea.invalid{
  border: 2px solid red; 
}

input.valid, textarea.valid{
  border: 2px solid green;
}
 </style>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

					 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 <?php
						 /*var_dump($A_view_proj);exit();*/
						?>
						<!-- Task grid -->
						<div class="card">
						  <div class="card-body">
										   <form method="post" action="<?php echo base_url().'Admin/A_view_projinfo_update'; ?>" >
											<fieldset class="mb-12">
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Client location Address</label>
													<div class="col-lg-9">
				                              
				                                    <input type="text" class="form-control" name="address" id="address" value="<?php echo $A_view_proj[0]['clint_loc_adres'];?>" required>
				                                        <span style="color: red" id="address_alert"></span>
				                                        
													</div>
												</div>
												
				                                
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Client location head</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="client_head" name="client_head" value="<?php echo $A_view_proj[0]['clint_loc_head'];?>" required>
														<span style="color: red" id="client_head_alert"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3">client location head mail</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="client_loc_head_mail" name="client_loc_head_mail" value="<?php echo $A_view_proj[0]['clint_loc_head_mail'];?>" required>
														<span style="color: red" id="client_loc_head_mail_alert"></span>
													</div>
												</div>
												 <div class="form-group row">
													<label class="col-form-label col-lg-3">client location Contact Number</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="contact" id="contact" value="<?php echo $A_view_proj[0]['clint_loc_head_ph'];?>" required>
														<span style="color: red" id="contact_alert"></span>
														<input type="hidden" name="project_id" value="<?php echo $A_view_proj[0]['project_id'];?>">
														
													</div>
												</div>
									
				                      
											
				    					    </fieldset>
				                           
				                            

											<div class="text-right">
												<button type="submit" name="profile_submit" id="profile_submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
											</div>
<!-- value="client_update" -->
										</form>
                             </div>	
                         </div>
                         <div class="card">
						  <div class="card-body">
										   <form method="post" action="<?php echo base_url().'Admin/A_view_projcpinfo_update'; ?>" >
											<fieldset class="mb-12">
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Company url link</label>
													<div class="col-lg-9">
				                                
				                                    <input type="text" class="form-control" name="comp_url_link" id="comp_url_link" value="<?php echo $A_view_proj_cpinfo[0]['c_url_link'];?>" required>
				                                        <span style="color: red" id="clint_id_alert"></span>
				                                        
													</div>
												</div>
												
				                                
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">url config file user-id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="url_config_file_uid" name="url_config_file_uid" value="<?php echo $A_view_proj_cpinfo[0]['url_config_file_uid'];?>" required>
														<span style="color: red" id="url_config_file_uid_alert"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3">url config file password</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="url_config_file_pwd" name="url_config_file_pwd" value="<?php echo $A_view_proj_cpinfo[0]['url_config_file_pwd'];?>" required><input type="checkbox" onclick="url_pwdshowhide()">Show Password
														<span style="color: red" id="url_config_file_pwd_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">url config file database user-id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="url_config_db_uid" name="url_config_db_uid" value="<?php echo $A_view_proj_cpinfo[0]['url_config_db_uid'];?>" required>
														<span style="color: red" id="url_config_db_uid_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">url config file database password</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="url_config_db_pwd" name="url_config_db_pwd" value="<?php echo $A_view_proj_cpinfo[0]['url_config_db_pwd'];?>" required>
														<input type="checkbox" onclick="url_db_pwdshowhide()">Show Password
														<span style="color: red" id="clint_id_alert"></span>
														<input type="hidden" name="project_id" value="<?php echo $A_view_proj_cpinfo[0]['project_id'];?>">
														
													</div>
												</div>
												 
				                      
											
				    					    </fieldset>
				                           
				                            

											<div class="text-right">
												<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
											</div>
<!-- value="client_update" -->
										</form>
                             </div>	
                         </div>
                         
              
						 

			 

	
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<script type="text/javascript">
	
$(document).ready(function(){  
    $("#address").keyup(function(){
      return validatetext('address','address_alert'); 
});   });
$(document).ready(function(){  
    $("#client_head").keyup(function(){
      return validatetext('client_head','client_head_alert'); 
});   });
$(document).ready(function(){  
    $("#client_loc_head_mail").keyup(function(){
      return validemailid('client_loc_head_mail','client_loc_head_mail_alert'); 
});   });
$(document).ready(function(){  
    $("#contact").keyup(function(){
      return validphone('contact','contact_alert'); 
});   });

 function url_pwdshowhide() {
  var x = document.getElementById("url_config_file_pwd");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
function url_db_pwdshowhide() {
  var x = document.getElementById("url_config_db_pwd");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}


$(document).ready(function(){  
    $("#url_config_file_uid").keyup(function(){
      return validatetext('url_config_file_uid','url_config_file_uid_alert'); 
});   });
$(document).ready(function(){  
    $("#url_config_file_pwd").keyup(function(){
      return validatetext('url_config_file_pwd','url_config_file_pwd_alert'); 
});   });
$(document).ready(function(){  
    $("#url_config_db_uid").keyup(function(){
      return validatetext('url_config_db_uid','url_config_db_uid_alert'); 
});   });
$(document).ready(function(){  
    $("#url_config_db_pwd").keyup(function(){
      return validatetext('url_config_db_pwd','url_config_db_pwd_alert'); 
});   });
 

$(document).ready(function(){ 
    $("#profile_submit").click(function(){ 
      var  address    = validatetext('address','address_alert');
      var client_head     = validatetext('client_head','client_head_alert');
      var client_loc_head_mail   = validemailid('client_loc_head_mail','client_loc_head_mail_alert');
      var contact    =validphone('contact','contact_alert');
     

      if(address == 0 || client_head == 0 || client_loc_head_mail == 0||  contact==0 || comp_url_link==0)
      {
        return false;
      }

      });  
  });    
 $(document).ready(function(){ 
    $("#submit").click(function(){ 
      var url_config_file_pwd     = validatetext('url_config_file_pwd','url_config_file_pwd_alert');
       var url_config_file_uid     = validatetext('url_config_file_uid','url_config_file_uid_alert');
        var url_config_db_pwd     = validatetext('url_config_db_pwd','url_config_db_pwd_alert');
        var url_config_db_uid     = validatetext('url_config_db_uid','url_config_db_uid_alert'); 

      if(url_config_file_pwd == 0 || url_config_file_uid == 0 || comp_url_link == 0|| url_config_db_uid==0 || url_config_db_pwd==0 )
      {
        return false;
      }

      });  
  });    
 

</script>			
<?php $this->load->view('Home/Footerm'); ?>			
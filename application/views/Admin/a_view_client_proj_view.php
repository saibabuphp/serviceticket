<?php $this->load->view('Home/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div> 

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						
				<!-- Rounded thumbs -->
				 
				<div class="row">
					 
 				<?php //var_dump($client_proj_det) ;exit();
                        if(!empty($client_proj_det)){ foreach($client_proj_det as $det){ ?> 
			 
                    	<div class="col-xl-4 col-sm-6">
						<div class="card">
							<div class="card-img-actions mx-1 mt-1">
								<img class="card-img img-fluid" src="<?php echo base_url().'assets/images/client/logo/'.$det['clint_company_logo'];?>"  alt="">
								<!-- src="< ?php echo $det['clint_company_logo']; ?>" -->
								<div class="card-img-actions-overlay card-img"> 
									<a href="<?php echo base_url().'Admin/A_view_projinfo/'.$det['project_id']; ?>" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round ml-2">
										<i class="icon-link"></i>
									</a>
								</div>
							</div>

					    	<div class="card-body text-center">
					    		<h6 class="font-weight-semibold mb-0"><?php echo $det['clint_company_name']; ?></h6>
					    		<h6 class="font-weight-semibold mb-0"><?php echo $det['clint_company_head_name']; ?></h6>
					    		<span class="d-block text-muted"><?php echo $det['c_url_link']; ?></span>

								<ul class="list-inline list-inline-condensed mt-3 mb-0">
									<li class="list-inline-item"><a href="<?php echo base_url().'Admin/admin_proj_profile_deactivate/'.$det['project_id']; ?>" class="btn btn-outline bg-info btn-icon text-info border-info border-2 rounded-round">
										<i class="icon-eraser2"></i> Deactivate</a>
									</li>
								</ul>

					    	</div>
				    	</div>
					</div>    	
					 
				<?php }}
				?>









				</div>
				


				
				<!-- /rounded thumbs-->
						 



			 

	
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
		</div></div>
			<!-- /content area -->
<?php $this->load->view('Home/Footerm'); ?>
 

			
 

			
 <?php $this->load->view('Client/Headm_links_validations'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper"> 
 
			<!-- Page header --> 
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div> 

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
	               <div class="card">
							<div class="card-header header-elements-md-inline">
								<h3 class="card-title text-primary">Client Name</h3><br>
                                
								<!--<div class="header-elements">
									<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right">Check in <b><i class="icon-alarm-check"></i></b></a>
			                	</div>-->
							</div>

							<div class="card-body"> 
								<div class="row container-fluid"> 
									<div class="col-md-6">
										<dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Project Name</dt>
				                            <dd><?php echo $assignsingleticketdet['c_url_link']; ?></dd>				                               
				                        </dl>
										<div class="card">
											<div class="card-header bg-transparent header-elements-inline">
												<span class="card-title font-weight-semibold">Control panel Details</span>
												<div class="header-elements">
													<div class="list-icons">
								                		<a class="list-icons-item" data-action="collapse"></a>
							                		</div>
						                		</div>
											</div>
											<div class="card-body">
												<form action="#">
													<div class="form-group-feedback form-group-feedback-right">
														<dl>
				                                	<dt class="font-size-sm text-primary text-uppercase">File Manager Details</dt>	
				                                	<dd><?php echo $assignsingleticketdet['url_config_file_uid']; ?></dd>
				                                	<dd><?php echo $assignsingleticketdet['url_config_file_pwd']; ?></dd>
				                                
				                                	<dt class="font-size-sm text-primary text-uppercase">Database Details</dt>	
				                                	<dd><?php echo $assignsingleticketdet['url_config_db_uid']; ?></dd>
				                                	<dd><?php echo $assignsingleticketdet['url_config_db_pwd']; ?></dd>
				                                
				                            		</dl>
													</div>
												</form>
											</div>
										</div> 
									</div> 
									<div class="col-md-6"> 
											<dl>
				                                <dt class="font-size-sm text-primary text-uppercase">Image Information</dt>
				                                <dd><img src="<?php echo base_url(); ?>assets/images/ticket/<?php echo $assignsingleticketdet['ticket_pics']; ?>" target="_blank" height="300" width="300"></dd>
				                            </dl>
			                        </div> 
			                        <div class="col-md-12"> 
			                        	<h6 class="font-weight-semibold">Subject</h6>
			                        	<p class="card-title text-primary"><?php echo $assignsingleticketdet['ticket_sub']; ?></p>
				                        <h6 class="font-weight-semibold">Descritption</h6>
								<p class="mb-3"><?php echo $assignsingleticketdet['ticket_discription']; ?></p>
								<h6 class="font-weight-semibold">Problem raised Path</h6>
								<p class="mb-4"><?php echo $assignsingleticketdet['url_link']; ?></p>
								<h6 class="font-weight-semibold">Assigned Emp Name</h6>
								<p class="mb-4"><?php echo ucfirst($assignsingleticketdet['emp_name']); ?></p>
								<h6 class="font-weight-semibold">Admin Estimed Time</h6>
								<p class="mb-4"><?php echo $assignsingleticketdet['admin_time']." MIN"; ?></p> 
			                        </div> 
			                    </div>               
	       		</div>

							 
						</div>
	 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
			 
<?php $this->load->view('Home/Footerm'); ?>
 

			
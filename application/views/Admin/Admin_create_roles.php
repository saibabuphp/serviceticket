<?php $this->load->view('Client/Headm_links_validations'); ?>
 <style>

  .error{
  display: none;
  margin-left: 10px;
}   

.error_show{
  color: red;
  margin-left: 10px;
}


input.invalid, textarea.invalid{
  border: 2px solid red; 
}

input.valid, textarea.valid{
  border: 2px solid green;
}
 </style>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
							<div class="card">
					<div class="card-header header-elements-inline">
						<h5 class="card-title">Create User Roles</h5>
					</div>

					<div class="card-body">
						<form method="post" action="<?php echo base_url().'Admin/inst_role'; ?>" enctype="multipart/form-data">
							<fieldset class="mb-3">
                                <div class="form-group row">
									<label class="col-form-label col-lg-2">Role Name</label>
									<div class="col-lg-10">
										<input type="text" name="role_name" class="form-control" id="role_name" value="" placeholder="Role Name" required>
										<span style="color: red" id="role_name_alert"></span>
									</div>
								</div>
                                <div class="form-group row">
									<label class="col-form-label col-lg-2">User Logos</label>
									<div class="col-lg-10">
										<input type="file" name="role_pic" id="role_pic" value="" class="form-control h-auto">
										<span style="color: red" id="role_pic_alert"></span>
										<span style="color: red" id="srole_pic_alert"></span>
									</div>
								</div>
    					    </fieldset>

							<div class="text-right">
								<button type="submit" class="btn btn-primary" id="submit">Submit <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>  
					</div>
					<!-- /left content --> 
					 <!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends--> 
				</div> 
				<!-- /main charts --> 
			</div>
			<!-- /content area -->
			<script type="text/javascript">
	$(document).ready(function(){  
    $("#role_name").keyup(function(){
      return validatetext('role_name','role_name_alert'); 
});   });

	$(document).ready(function(){
 $(document).on('change', '#role_pic', function(){
  var name = document.getElementById("role_pic").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase(); 
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
    $("#role_pic_alert").html("Invalid Image format File"); 
  } else   {    $("#role_pic_alert").html("");  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("role_pic").files[0]);
  var f = document.getElementById("role_pic").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 200000)
  {
    $("#srole_pic_alert").html("File Size is big"); 
  }
  else   {    $("#srole_pic_alert").html("");  }
 });
}); 
	$(document).ready(function(){ 
    $("#submit").click(function(){ 
      var role_name    = validatetext('role_name','role_name_alert'); 
      var role_pic    = validate_fileupload('role_pic','role_pic_alert'); 
      if(role_name == 0 || role_pic == 0 )
      {
        return false;
      }

      });  
  });    

</script>			
<?php $this->load->view('Home/Footerm'); ?>
 

			
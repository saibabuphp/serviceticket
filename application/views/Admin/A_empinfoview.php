<?php $this->load->view('Client/Headm_links_validations'); ?>
 
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

					 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 <?php
						 /*var_dump($A_empinfo_view);exit();*/
						?>
						<!-- Task grid -->
						  <div class="card-body">
										   <form method="post" action="<?php echo base_url().'Admin/A_empinfoview_update1'; ?>" enctype="multipart/form-data">
											<fieldset class="mb-12">
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="name" id="name" value="<?php echo $A_empinfo_view[0]["emp_name"]; ?>" required>
														<span style="color: red" id="name_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Employee picture</label>
													<div class="col-lg-9">
														<img height="60" src="<?php echo base_url('assets/images/emp/').$A_empinfo_view[0]['emp_pic']; ?>" id="comp_logo" name="comp_logo" value="" /> 
														<input type="file" class="form-control h-auto" name="update_logo" id="update_logo" value="">
														 <input type="hidden" name="emp_img" value="<?php echo $A_empinfo_view[0]['emp_id'];?>">
														 <span style="color: red" id="comp_logo_alert"></span>
														 
													</div>
												</div>
				                                
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Email_id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="email" name="email" value="<?php echo $A_empinfo_view[0]['emp_mail'];?>" required>
														<span style="color: red" id="email_alert"></span>
														
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3">user-role-id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="user_role_id" name="user_role_id" value="<?php echo $A_empinfo_view[0]['user_role_id'];?>" required>
														<span style="color: red" id="user_role_id_alert"></span>
													</div>
												</div>
												 <div class="form-group row">
													<label class="col-form-label col-lg-3">Contact Number</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="contact" id="contact" value="<?php echo $A_empinfo_view[0]['phone_no'];?>" required>
														<span style="color: red" id="contact_alert"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Date of joining</label>
													<div class="col-lg-9">
														<input type="date" class="form-control" id="doj" name="doj" value="<?php echo $A_empinfo_view[0]['doj'];?>" required>
														<span style="color: red" id="doj_alert"></span>
													</div>
												</div>
				                                 <div class="form-group row">
													<label class="col-form-label col-lg-3">Address</label>
													<div class="col-lg-9">
				                                 <!-- <textarea style="background-color:red;" rows="3" cols="3" class="form-control" name="addr"  id="addr" value="<?php echo $topay_company_bank[0]['address'];?>" required> 
				                                    </textarea> -->
				                                    <input type="text" class="form-control" name="address" id="address" value="<?php echo $A_empinfo_view[0]['address'];?>" required>
				                                        <span style="color: red" id="address_alert"></span>
				                                        <input type="hidden" name="emp_id" value="<?php echo $A_empinfo_view[0]['emp_id'];?>">
													</div>
												</div>
				                                
				                      
											
				    					    </fieldset>
				                           
				                            

											<div class="text-right">
												<button type="submit" name="submit" id="client_update_submit"  class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
											</div>
<!-- value="client_update" -->
										</form>
                             </div>	 
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts --> 
			</div>
			<!-- /content area -->
<script> 

$(document).ready(function(){  
    $("#name").keyup(function(){
      return validatetext('name','name_alert'); 
});   });
$(document).ready(function(){  
    $("#email").keyup(function(){
      return validemailid('email','email_alert'); 
});   });
$(document).ready(function(){  
    $("#contact").keyup(function(){
      return validmobile('contact','contact_alert'); 
});   });

$(document).ready(function(){  
    $("#user_role_id").keyup(function(){
      return validatetext('user_role_id','user_role_id_alert'); 
});   });
 
$(document).ready(function(){  
    $("#doj").keyup(function(){
      return validatetext('doj','doj_alert'); 
});   });
 
$(document).ready(function(){  
    $("#address").keyup(function(){
      return validatetext('address','address_alert'); 
});   }); 

$(document).ready(function(){ 
    $("#submit").click(function(){

      var name    = validatetext('name','name_alert');
      var user_role_id    = validatetext('user_role_id','user_role_id_alert');
      var email    = validemailid('email','email_alert');
      var contact    =validmobile('contact','contact_alert');
      var emp_gender   = validatetext('emp_gender','emp_gender_alert');
      var address = validatetext('address','address_alert');
      //var path     = validatetext('path','path_alert');
      var emp_pic    = validate_fileupload('emp_pic','emp_pic_alert'); 

      if(name == 0 || user_role_id == 0 || email == 0|| contact == 0 || doj == 0 || comp_logo == 0 || address == 0 )
      {
        return false;
      }

      });  
  });    

</script>

<?php $this->load->view('Home/Footerm'); ?>
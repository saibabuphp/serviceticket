<?php $this->load->view('Client/Headm_links_validations'); ?>
 
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div> 
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

					 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 <?php
						 //var_dump($A_clientinfo_view);exit();
						?>
						<!-- Task grid -->
						  <div class="card-body">
										   <form method="post" action="<?php echo base_url().'Admin/A_clientinfoview_update1'; ?>" enctype="multipart/form-data">
											<fieldset class="mb-12">
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Company Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="comp_name" id="comp_name" value="<?php echo $A_clientinfo_view[0]["clint_company_name"]; ?>" required>
														<span style="color: red" id="comp_name_alert"></span>
														
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Company Logo</label>
													<div class="col-lg-9">
														<img height="60" src="<?php echo base_url('assets/images/client/logo/').$A_clientinfo_view[0]['clint_company_logo']; ?>" id="comp_logo" name="comp_logo" value=""  /> 
														<input type="file" class="form-control h-auto" name="update_logo" id="update_logo" value="">
														 <input type="hidden" name="clint_uid" value="<?php echo $A_clientinfo_view[0]['clint_uid'];?>">
														<span style="color: red" id="comp_logo_alert"></span>
														 
													</div>
												</div>
				                                
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Client Company Head Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="Client_company_head_name" name="Client_company_head_name" value="<?php echo $A_clientinfo_view[0]['clint_company_head_name'];?>" required>
														<span style="color: red" id="Client_company_head_name_alert"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3">client Company Head Pic</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="clint_comp_head_pic" name="clint_comp_head_pic" value="<?php echo $A_clientinfo_view[0]['clint_company_head_pic'];?>" required>
														<span style="color: red" id="clint_comp_head_pic_alert"></span>
													</div>
												</div>
												 <div class="form-group row">
													<label class="col-form-label col-lg-3">Client Company Head Mail</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="clint_comp_head_mail" id="clint_comp_head_mail" value="<?php echo $A_clientinfo_view[0]['clint_company_head_mail'];?>" required>
													<span style="color: red" id="clint_comp_head_mail_alert"></span>
													</div>
												</div>
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3">client Company Head Phone</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="clint_comp_head_ph" name="clint_comp_head_ph" value="<?php echo $A_clientinfo_view[0]['clint_company_head_ph'];?>" required>
														<span style="color: red" id="clint_comp_head_ph_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">client Company Contact Persion</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="clint_company_contact_persion" name="clint_company_contact_persion" value="<?php echo $A_clientinfo_view[0]['clint_company_contact_persion'];?>" required>
													<span style="color: red" id="clint_company_contact_persion_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">client Company Contact Persion Mail</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="clint_company_contact_persion_mail" name="clint_company_contact_persion_mail" value="<?php echo $A_clientinfo_view[0]['clint_company_contact_persion_mail'];?>" required>
														<span style="color: red" id="clint_company_contact_persion_mail_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">client Company Contact Persion Phone</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="clint_company_contact_persion_phone" name="clint_company_contact_persion_phone" value="<?php echo $A_clientinfo_view[0]['clint_company_contact_persion_phone'];?>" required>
														<span style="color: red" id="clint_company_contact_persion_phone_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">client Status</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="client_status" name="client_status" value="<?php echo $A_clientinfo_view[0]['client_status'];?>" required>
														<span style="color: red" id="client_status_alert"></span>
														
														  <input type="hidden" name="clint_uid" value="<?php echo $A_clientinfo_view[0]['clint_uid'];?>">
													</div>
												</div>
				                                 
				                                
				                      
											
				    					    </fieldset>
				                           
				                            

											<div class="text-right">
												<button type="submit" name="submit" id="submit"  class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
											</div>
<!-- value="client_update" -->
										</form>
                             </div>	
                         
              
						 

			 

	
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>

			<!-- /content area -->

<script type="text/javascript">

	
	
	$(document).ready(function(){  
    $("#comp_name").change(function(){
      return validatetext('comp_name','comp_name_alert'); 
});   });
	$(document).ready(function(){  
    $("#Client_company_head_name").keyup(function(){
      return validatetext('Client_company_head_name','Client_company_head_name_alert'); 
});   });
$(document).ready(function(){  
    $("#clint_comp_head_mail").keyup(function(){
      return validemailid('clint_comp_head_mail','clint_comp_head_mail_alert'); 
});   });
$(document).ready(function(){  
    $("#clint_comp_head_ph").keyup(function(){
      return validmobile ('clint_comp_head_ph','clint_comp_head_ph_alert'); 
});   });

$(document).ready(function(){  
    $("#clint_company_contact_persion").keyup(function(){
      return validatetext('clint_company_contact_persion','clint_company_contact_persion_alert'); 
});   });
$(document).ready(function(){  
    $("#clint_company_contact_persion_mail").keyup(function(){
      return validemailid('clint_company_contact_persion_mail','clint_company_contact_persion_mail_alert'); 
});   });
$(document).ready(function(){  
    $("#clint_company_contact_persion_phone").keyup(function(){
      return validmobile ('clint_company_contact_persion_phone','clint_company_contact_persion_phone_alert'); 
});   });
$(document).ready(function(){  
    $("#client_status").keyup(function(){
      return validatenumber('client_status','client_status_alert'); 
});   });

				
$(document).ready(function(){ 
    $("#submit").click(function(){ 
     
      var comp_name     = validatetext('comp_name','comp_name_alert');
      var Client_company_head_name  = validatetext('Client_company_head_name','Client_company_head_name_alert');
      var clint_comp_head_mail    = validemailid('clint_comp_head_mail','clint_comp_head_mail_alert');
      var clint_comp_head_ph    =validphone('clint_comp_head_ph','clint_comp_head_ph_alert');
      var clint_company_contact_persion   = validatetext('clint_company_contact_persion','clint_company_contact_persion_alert');
      var clint_company_contact_persion_mail    = validemailid('clint_company_contact_persion_mail','clint_company_contact_persion_mail_alert');
      var clint_company_contact_persion_phone    =validphone('clint_company_contact_persion_phone','clint_company_contact_persion_phone_alert');
      var client_status    = validatetext('client_status','client_status_alert');
    

      if( comp_name == 0 || Client_company_head_name == 0|| clint_comp_head_mail == 0 || clint_comp_head_ph == 0 || clint_company_contact_persion == 0 ||compy_cont_per_email == 0 || clint_company_contact_persion_phone== 0 || client_status == 0 || comp_logo == 0 ||  comp_h_pic == 0)
      {
        return false;
      }

      });  
  });    




</script>
<?php $this->load->view('Client/Footerm'); ?>
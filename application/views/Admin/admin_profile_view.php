 <?php $this->load->view('Home/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	<!--  < ?php echo  "<pre>";var_dump($employee_profile);exit();?> -->
	<!-- Page content -->
	<div class="page-content">
	 <?php $this->load->view('Admin/Sidebar_m'); ?>


		
	
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

				 	<?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1"> 
						<!-- Task grid -->
						<div class="row">
                        <div class="card">
							<div class="card-body">
								<div class="mb-8"> 
					 
							<div class="media card-body flex-column flex-md-row m-0">
								 
								 <div class="table-responsive">
													<table class="table table-dark bg-slate-300">
														<thead>
															<tr>
															 
																<th>Name</th>
																
																<th>Mali-id</th>
																<th>Gender</th>
																<th>Employee Role</th>
																<th>Contact Number</th>
																<th>Date Of Birth</th>
																<th>date Of Joining</th>
																<th>Address</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																 
																<td><?php echo $employee_profile[0]['emp_name']; ?></td>
																<td><?php echo $employee_profile[0]['emp_mail']; ?></td>
																<td><?php echo $employee_profile[0]['gender_type']; ?></td>
																<td><?php echo $employee_profile[0]['user_name']; ?></td>
																<td><?php echo $employee_profile[0]['phone_no']; ?></td>
																<td><?php echo $employee_profile[0]['dob']; ?></td>
																<td><?php echo $employee_profile[0]['doj']; ?></td>
																<td><?php echo $employee_profile[0]['address']; ?></td>
																
																
															</tr>
															
														</tbody>
														
													</table>
													
												</div>
							</div>
						 
						<!-- /about author --> 
								</div>

							</div>
						 	<div class="card-body">
									<fieldset class="mb-12">
									<div class="form-group row">
																						<label class="col-form-label col-lg-3"> My Name</label>
																						<div class="col-lg-9">
																							<?php echo $employee_profile[0]["emp_name"]; ?>
										</div>
									</div>
									<div class="form-group row">
																						<label class="col-form-label col-lg-3">Email_id</label>
																						<div class="col-lg-9">
																							<?php echo $employee_profile[0]['emp_mail'];?>
																						</div>
									</div>
									<div class="form-group row">
									<label class="col-form-label col-lg-3"> Gender</label>
									<div class="col-lg-9">
									<?php echo $employee_profile[0]['gender_type'];?>
									</div>
									</div>
									<div class="form-group row">
									<label class="col-form-label col-lg-3"> Emp Role Id</label>
																						<div class="col-lg-9">
																							<?php echo $employee_profile[0]['user_name'];?>
																						</div>
									</div>
									<div class="form-group row">
									<label class="col-form-label col-lg-3"> phone Number</label>
									<div class="col-lg-9">
									<?php echo $employee_profile[0]['phone_no'];?>
									</div>
									</div>
									<div class="form-group row">
									<label class="col-form-label col-lg-3">Date Of Birth</label>
									<div class="col-lg-9">
									<?php echo $employee_profile[0]['dob'];?>
									</div>
									</div>
									<div class="form-group row">
									<label class="col-form-label col-lg-3">Date Of joining</label>
									<div class="col-lg-9">
									<?php echo $employee_profile[0]['doj'];?>
									</div>
									</div>
									<div class="form-group row">
										<label class="col-form-label col-lg-3">Address</label>
																						<div class="col-lg-9">
									<?php echo $employee_profile[0]['address'];?> 
									</div>
									</div>
									<div class="form-group row">
									<label class="col-form-label col-lg-3">Do you want to update </label>
									<div class="col-lg-9">
									<a href="<?php echo base_url().'Admin/admin_profile_update/'.$employee_profile[0]['emp_id'];?>" class="nav-link"><i class=icon-pencil></i> <span> Update</span></a>
									</div>
									</div>
									</fieldset>
									</div>

							<div class="card-body"> 
						 
							<div class="media card-body flex-column flex-md-row m-0">
								 
								<div class="mb-3 text-center"> 
									Employee picture <br/><br/>
									<img width="150" src="<?php echo base_url('assets/images/emp/').$employee_profile[0]['emp_pic']; ?>" />  
									</div>  
							</div> 
							</div>
						</div>
					 
						</div>
				
					</div>
					<!-- /left content -->


					<!-- Right sidebar component -->
					 
					<!-- /right sidebar component -->
<?php $this->load->view('Admin/Admin_right_sidebar'); ?>	
				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<?php $this->load->view('Home/Footerm'); ?>
 

			
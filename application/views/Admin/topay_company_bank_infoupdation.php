<?php $this->load->view('Client/Headm_links_validations'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Admin/Sidebar_m'); ?>
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

					 <?php $this->load->view('Admin/Adm_head_analytics_ticket_count'); ?>
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
						  <div class="card-body">
										   <form method="post" action="<?php echo base_url().'Admin/topay_companybankinfo_update'; ?>" enctype="multipart/form-data">
											<fieldset class="mb-12">
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3"> Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="name" id="name" value="<?php echo $topay_company_bank[0]["name"]; ?>" required>
														<span style="color: red" id="name_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Comapany Logos</label>
													<div class="col-lg-9">
														<img height="60" src="<?php echo base_url('assets/images/client/logo/').$topay_company_bank[0]['company_logo']; ?>" id="comp_logo" name="comp_logo" value="" required /> 
														 
													</div>
												</div>
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">url</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="url" name="url"  value="<?php echo $topay_company_bank[0]['url'];?>" required>
														<span style="color: red" id="url_alert"></span>
													</div>
												</div>
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Email_id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="email" name="email" value="<?php echo $topay_company_bank[0]['mail_id'];?>" required>
														<span style="color: red" id="email_alert"></span>
													</div>
												</div>
												 <div class="form-group row">
													<label class="col-form-label col-lg-3">Contact Number</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="contact" id="contact" value="<?php echo $topay_company_bank[0]['phone_no'];?>" required>
														<span style="color: red" id="contact_alert"></span>
													</div>
												</div>
				                                 <div class="form-group row">
													<label class="col-form-label col-lg-3">Address</label>
													<div class="col-lg-9">
				                                 <!-- <textarea style="background-color:red;" rows="3" cols="3" class="form-control" name="addr"  id="addr" value="<?php echo $topay_company_bank[0]['adsress'];?>" required> 
				                                    </textarea> -->
				                                    <input type="text" class="form-control" name="addr" id="addr" value="<?php echo $topay_company_bank[0]['adsress'];?>" required>
				                                        <span style="color: red" id="addr_alert"></span>
													</div>
												</div>
				                                
				                                
												<div class="form-group row">
													<label class="col-form-label col-lg-3">ifsc_code</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="ifsc" name="ifsc" value="<?php echo $topay_company_bank[0]['ifcs_code'];?>" required>
														<span style="color: red" id="ifsc_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Account Number</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="account" name="account" value="<?php echo $topay_company_bank[0]['account_no'];?>" required>
														<span style="color: red" id="account_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Bank Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="bank" name="bank" value="<?php echo $topay_company_bank[0]['bank_name'];?>" required>
														<span style="color: red" id="bank_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Branch</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="branch" name="branch" value="<?php echo $topay_company_bank[0]['branch'];?>" required>
														<span style="color: red" id="branch_alert"></span>
													</div>
												</div>
				    					    </fieldset>
				                           
				                            

											<div class="text-right">
												<button type="submit" name="submit" id="client_update_submit"  class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
											</div>
<!-- value="client_update" -->
										</form>
                             </div>	
                         
              
						 

			 

	
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Admin/Admin_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->

<script type="text/javascript">

	$(document).ready(function(){  
    $("#addr").keyup(function(){
      return validatetext('addr','addr_alert'); 

});   });
	
	$(document).ready(function(){  
    $("#name").keyup(function(){
      return validatetext('name','name_alert'); 
	});   });
	
	$(document).ready(function(){  
    $("#email").keyup(function(){
      return validemailid('email','email_alert'); 
	});   });

	$(document).ready(function(){  
    $("#contact").keyup(function(){
      return validphone('contact','contact_alert'); 
	});   });

	$(document).ready(function(){
 	$(document).on('change', '#comp_logo', function(){
  	var name = document.getElementById("comp_logo").files[0].name;
  	var form_data = new FormData();
  	var ext = name.split('.').pop().toLowerCase(); 
  	if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  	{
    	$("#comp_logo_alert").html("Invalid Image format File"); 
  	} else   {    $("#comp_logo_alert").html("");  }
  	var oFReader = new FileReader();
  	oFReader.readAsDataURL(document.getElementById("comp_logo").files[0]);
  	var f = document.getElementById("comp_logo").files[0];
  	var fsize = f.size||f.fileSize;
  	if(fsize > 200000)
  	{
    	$("#scomp_logo_alert").html("File Size is big"); 
  	}
 	 else   {    $("#scomp_logo_alert").html("");  }
 	});
	}); 
	$(document).ready(function(){  
    $("#ifsc").keyup(function(){
      return validatetext('ifsc','ifsc_alert'); 
	});   });
	$(document).ready(function(){  
    $("#account").keyup(function(){
      return validatenumber('account','account_alert'); 
	});   });
	$(document).ready(function(){  
    $("#bank").keyup(function(){
      return validatetext('bank','bank_alert'); 
	});   });
	$(document).ready(function(){  
    $("#branch").keyup(function(){
      return validatetext('branch','branch_alert'); 
	});   });

	


				
	$(document).ready(function(){ 
    $("#client_update_submit").click(function(){ 
      var addr    = validatetext('addr','addr_alert');
      var name     = validatetext('name','name_alert');
      var email    = validemailid('email','email_alert');
      var contact    =validphone('contact','contact_alert');
      var ifsc   = validatetext('ifsc','ifsc_alert');
      var bank   = validatetext('bank','bank_alert');
      var branch   = validatetext('branch','branch_alert');
      var account    =validatenumber('account','account_alert');
      //var path     = validatetext('path','path_alert');
      var comp_logo   = validate_fileupload('comp_logo','comp_logo_alert'); 
      
      if(addr == 0 ||name == 0 ||  email == 0|| contact == 0 || ifsc == 0 || bank == 0 ||branch == 0 || account== 0 || comp_logo == 0 )
      {
        return false;
      }

      });  
  	});    
</script>
<?php $this->load->view('Client/Footerm'); ?>
 

			
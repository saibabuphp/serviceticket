 <?php $this->load->view('Client/Headm_links_validations');  ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Client/Sidebar_m'); ?> 
		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div> 

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div> 
			</div> 
			<!-- /page header --> 
			<!-- Content area -->
			<div class="content"> 

				 <?php $this->load->view('Client/Clnt_head_analytics_ticket_count');?> 
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
						<div class="card">
							<div class="card-header header-elements-md-inline">
                                
								<h3 class="card-title text-primary"><?php  echo $tick_status[0]['clint_company_name']; ?></h3><br>
                                
								<!--<div class="header-elements">
									<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right">Check in <b><i class="icon-alarm-check"></i></b></a>
			                	</div>-->
							</div>

							<div class="card-body"> 
								<div class="row container-fluid"> 
									<div class="col-md-6">
										<dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Project Name</dt>
				                            <dd><?php echo $tick_status[0]['c_url_link']; ?></dd>	
				                        </dl>
                                        <dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Priority</dt>
				                            <dd><?php echo $tick_status[0]['priority_type']; ?></dd>	
				                        </dl>
                                        <dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Request Type</dt>
				                            <dd><?php echo $tick_status[0]['reqirement_type']; ?></dd>	
				                        </dl>
                                        <?php if(isset($tick_status[0]['emp_name'])) {?>
                                        <dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Assigned To Employee</dt>
				                            <dd><?php echo $tick_status[0]['emp_name']; ?></dd>	
				                        </dl>
                                        <dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Assigned Date</dt>
				                            <dd><?php echo date("d M, Y",strtotime($tick_status[0]['admin_assigned_toemp'])); ?></dd>	
				                        </dl>
										<?php }?>
									</div> 
									<div class="col-md-6"> 
											<dl>
				                                <dt class="font-size-sm text-primary text-uppercase">Image Information</dt>
				                                <dd><img src="http://localhost/serviceticket/assets/images/ticket/<?php echo $tick_status[0]['ticket_pics']; ?>" target="_blank" height="300" width="300"></dd>
				                            </dl>
			                        </div> 
			                        <div class="col-md-12"> 
                                        <h3 style="padding:0; margin:0px;">Subject</h3>
			                        	<h4 class="card-title text-primary"><?php echo $tick_status[0]['ticket_sub']; ?></h4>
				                        <h6 class="font-weight-semibold">Descritption</h6>
								<p class="mb-3"><?php echo $tick_status[0]['ticket_discription']; ?></p>
								<h6 class="font-weight-semibold">Problem raised Path</h6>
								<p class="mb-4"><?php echo $tick_status[0]['url_link']; ?></p> 
			                        </div> 
			                    </div>
			                    <?php if($view_form == 1) {?>
			                              <div class="card"> 
									<div class="card-body"> 
										<form action="<?php echo base_url().'Client/clint_close_tic' ?>" method="post">
											<fieldset class="mb-3">
											   <legend class="text-uppercase font-size-sm font-weight-bold">Resolved</legend>
				                                <input type="hidden" class="form-control" name="tic_id" id="clickable-label" value="<?php echo $tick_status[0]['ticket_id']; ?>" required="">
				                                
									<div class="form-group row">
		                        		<label class="col-form-label col-lg-4">Close Ticket</label>
		                        		<div class="col-lg-8">
			                            <select class="form-control" name="emp_id" id="close_ticket" required="">
			                            	<option value="Select Close Ticket">Select Close Ticket</option>
			                                <option value="5">Close</option>
                                        	<option value="5">Reassign</option>			 
                                        </select>
			                            <span style="color: red" id="close_ticket_alert"></span>
		                           		 </div>
		                        	</div>
		                        	<div class="form-group row">
										<label class="col-form-label col-lg-4">Message</label>
										<div class="col-lg-8">
											<textarea rows="3" cols="3" name="msg" id="msg" class="form-control" placeholder="Message"></textarea>
											<span style="color: red" id="msg_alert"></span>
										</div>
									</div>
				                                 
				                                
											
											</fieldset>
											<!-- id="a_assign_ticket_to_emp_submit" -->
				                            <div class="text-right">
												<button type="submit"  id ="submit" name="submit" class="btn btn-primary">Close <i class="icon-paperplane ml-2"></i></button>
											</div>
										</form>
									</div>
								</div>
								<?php	} ?> 
                                
	       		</div> 
							<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
								<span class="d-flex align-items-center">
									<span class="badge badge-mark border-blue mr-2"></span>
									Ticket Rised Date : <?php echo date("d M, Y",strtotime($tick_status[0]['created'])); ?>
								 
								</span>
 
							</div>
						</div> 

					</div>
					<!-- /left content --> 
					<!-- Right sidebar component starts -->
					<?php $this->load->view('Client/client_right_sidebar'); ?>
					<!-- Right sidebar component ends--> 
				</div>
				
				<!-- /main charts -->  
			</div>
			<!-- /content area -->
			<script type="text/javascript">
	$(document).ready(function(){ 

    $("#close_ticket").change(function(){
    var select = $("#close_ticket option:selected").val();  
      validclose_ticket();  
});   });  
	
	function validclose_ticket() 
	{
 	var close_ticket = $("#close_ticket");   
       if(close_ticket.val() == "" ||close_ticket.val() == null || close_ticket.val() == "Select Close Ticket")
       {    
       	  close_ticket.focus();
          $("#close_ticket_alert").html("Close ticket field reqired");
          return false; 
     	}
     	else { $("#close_ticket_alert").html(""); }
	}
	
	
	$(document).ready(function(){  
    $("#msg").keyup(function(){
    return validatetext('msg','msg_alert'); 
	});   });
	
	$(document).ready(function(){ 
    $("#submit").click(function(){  
      var close_ticket    = validatetext('close_ticket','close_ticket_alert');
      var msg   = validatetext('msg','msg_alert'); 
      if( close_ticket == 0 || msg == 0 )
      {
        return false;
      }

      });  
  });    
</script>			
<?php $this->load->view('Client/Footerm'); ?>
 

			
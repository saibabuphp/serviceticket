<!-- Main sidebar -->
		<div class="sidebar sidebar-dark sidebar-main sidebar-expand-md">

			<!-- Sidebar mobile toggler -->
			<div class="sidebar-mobile-toggler text-center">
				<a href="#" class="sidebar-mobile-main-toggle">
					<i class="icon-arrow-left8"></i>
				</a>
				Navigation
				<a href="#" class="sidebar-mobile-expand">
					<i class="icon-screen-full"></i>
					<i class="icon-screen-normal"></i>
				</a>
			</div>
			<!-- /sidebar mobile toggler -->


			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-user">
					<div class="card-body">
						<div class="media">
							<?php $p_view_menu=$this->Clientmodel->clnt_profile_view_menu();?>
							<div class="mr-3">
								<a href="#"><img src="<?php echo base_url().'assets/images/client/logo/'.$p_view_menu[0]['clint_company_logo'];?>" width="38" height="38" class="rounded-circle" alt=""></a>
							</div>

							<div class="media-body">
								<div class="media-title font-weight-semibold"><?php echo $p_view_menu[0]['clint_company_name']; ?></div>
								<div class="font-size-xs opacity-50">
									<i class="icon-pin font-size-sm"></i> &nbsp;<?php echo $p_view_menu[0]['clint_company_adres']; ?>
								</div>
							</div> 
						</div>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="card card-sidebar-mobile">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
						<!--<li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>-->
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Client_dashboard';?>" class="nav-link active">
								<i class="icon-home4"></i>
								<span>
									Dashboard 
								</span>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Client_newtickets';?>" class="nav-link"><i class="icon-reading"></i> <span>Raised Tickets</span></a>

						</li>
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Client_onprograss';?>" class="nav-link"><i class="icon-task"></i> <span>Approved</span></a>

						</li>
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Client_onhold';?>" class="nav-link"><i class="icon-watch2"></i> <span>Onhold</span></a>
                        </li>
						<li class="nav-item ">
							<a href="<?php echo base_url().'Client/Client_resolved';?>" class="nav-link"><i class="icon-copy"></i> <span>Resolved</span></a>
                        </li>
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Client_closed';?>" class="nav-link"><i class="icon-file-check"></i> <span>Closed</span></a>
						</li>
                        
						<!-- <li class="nav-item ">
							<a href="<?php echo base_url().'Client/Client_history';?>" class="nav-link"><i class="icon-history"></i> <span>History</span></a>
                        </li> -->
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Client_profile';?>" class="nav-link"><i class="icon-user-plus"></i> <span>My Profile</span></a>
                        </li>
						
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Bank_details';?>" class="nav-link"><i class="icon-cash3"></i> <span>Bank details</span></a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url().'Client/Client_FAQS';?>" class="nav-link"><i class="icon-question6"></i> <span>FAQS</span></a>

						</li>
						<!-- /main -->

						 

					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
		<!-- /main sidebar -->

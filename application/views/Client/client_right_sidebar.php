<!-- Right sidebar component -->
					<div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md  col-sm-12">

						<!-- Sidebar content -->
						<div class="sidebar-content "> 

							<!-- Search -->
							<!-- <div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">Search ticket</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div> 

								<div class="card-body">
									<form action="#">
										<div class="form-group-feedback form-group-feedback-right">
											<input type="search" class="form-control" placeholder="type and hit Enter">
											<div class="form-control-feedback">
												<i class="icon-search4 font-size-base text-muted"></i>
											</div>
										</div>
									</form>
								</div>
							</div> -->
							 
						<div class="card   col-sm-12">
								<div class="card-header bg-transparent header-elements-inline">
								<span class="text-uppercase font-size-sm font-weight-semibold">Quick Links </span>
								<div class="header-elements">
									<div class="list-icons">
								         <a class="list-icons-item" data-action="collapse"></a>
								    </div>
								</div>
								</div>

								<div class="card-body p-0">
									<div class="nav nav-sidebar mb-2">  
									<li class="nav-item">
										<a href="<?php echo base_url().'Client/Client_home';?>" class="nav-link"><i class="icon-plus-circle2"></i>  Raise ticket </a>
									</li> 
									<li class="nav-item">
										<a href="<?php echo base_url().'Client/Aview_employee';?>" class="nav-link"><i class="icon-user-tie"></i> Employee
											<span class="badge badge-primary badge-pill ml-auto">21</span>
										</a>
									</li>
									</div>
								</div>
							</div>
							<!-- /search -->
							<!-- calcilations for display prograss chat in rightside start -->
							<?php
							 /*echo $this->session->userdata('c_assigned_tickets_count'); 
							 echo $this->session->userdata('c_shedule_tickets_count'); */
							 //prograss bar for prograss tickets
							 $c_pb_Progress=round((100*$this->session->userdata('c_shedule_tickets_count'))/$this->session->userdata('c_assigned_tickets_count'));
							?>
							<!-- calcilations for display prograss chat ends -->
							<!-- Completeness stats -->
					 	<div class="card  col-sm-12">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="text-uppercase font-size-sm font-weight-semibold">Completeness stats</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body">
									<ul class="list-unstyled mb-0">
							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Client/Client_onprograss';?>"  >
							                <div class="d-flex align-items-center mb-1">Progress <span class="text-muted ml-auto"><?php echo $c_pb_Progress?>%</span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-grey-400" style="width: <?php echo $c_pb_Progress?>%">
													<span class="sr-only"><?php echo $c_pb_Progress?>% Complete</span>
												</div>
											</div>
										</a>
							            </li>

							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Client/Client_resolved';?>"  >
							                <div class="d-flex align-items-center mb-1">My resolved tickets <span class="text-muted ml-auto">50%</span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-warning-400" style="width: 50%">
													<span class="sr-only">50% Complete</span>
												</div>
											</div>
										</a>
							            </li>

							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Client/Client_closed';?>"  >
							                <div class="d-flex align-items-center mb-1">Closed  tickets<span class="text-muted ml-auto">70%</span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-success-400" style="width: 70%">
													<span class="sr-only">70% Complete</span>
												</div>
											</div>
										</a>
							            </li>

							            
							        </ul>
								</div>
							</div> 
							<!-- /completeness stats -->

 

						</div>
						<!-- /sidebar content -->

					</div>
					<!-- /right sidebar component -->
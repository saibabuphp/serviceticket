
<?php $this->load->view('Client/Headm_links'); ?>
<body onload=display_ct();>
	<?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Client/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper"> 

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>
 
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
					<div class="header-elements d-none">
						<div class="breadcrumb justify-content-center">
							 

							<div class="breadcrumb-elements-item dropdown p-0">
							 
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				<!-- Main charts --> 
				
				<?php $this->load->view('Client/Clnt_head_analytics_ticket_count');?>
				<!-- /main charts -->


				<!-- Dashboard content -->
				<div class="row">
					<div class="col-xl-8 col-md-8 col-lg-8 col-sm-12">

						<!-- Marketing campaigns -->
						<div class="card">
							<div class="card-header header-elements-sm-inline">
								<div class="card-title ">New Ticket</div>  
								<?php
								if($this->session->flashdata('item')) {
								$message = $this->session->flashdata('item');?>
								<div class="<?php echo $message['class'] ?> "><?php echo $message['message']; ?> 
								</div>
								<?php }	?>
								<div class="header-elements  "> 
									<div class="list-icons ml-3">
				                		<div class="d-flex align-items-center justify-content-center mb-2">
											<a href="<?php echo base_url().'Client/Client_home/'; ?>" class="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
												<i class="icon-plus3"></i>
											</a>
											<div>
												<div class="font-weight-semibold">Rise Ticket</div> 
											</div>
										</div>
				                	</div>
			                	</div>
							</div> 
						</div>
						 
						 <div class="card">
								<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th style="width: 50px">Date</th>
											<th style="width: 300px;">Project </th>
											<th>Description</th> 
											<th>Actions</th> 
										</tr>
									</thead>
									<tbody> 

										<tr class="table-active table-border-double">
											<td colspan="3">Resolved tickets</td>
											<td class="text-right">
												<span class="badge bg-success badge-pill"><?php echo $this->session->userdata('c_resolved_count');?></span>
											</td>
										</tr>
                                        <?php foreach($resolved as $row)
                                                    {?>
										<tr>
											<td class="text-center">
												<?php echo date("d M Y",strtotime($row['created'])); ?>
											</td>
											<td>
												<div class="d-flex align-items-center">
													
													<div>
														<a href="#" class="text-default font-weight-semibold letter-icon-title"><?php  echo parse_url($row['c_url_link'], PHP_URL_HOST); ?></a>
														<div class="text-muted font-size-sm"><span class="badge badge-mark border-success mr-1"></span> Resolved</div>
													</div>
												</div>
											</td>
											<td>
												<a href="#" class="text-default">
													<div><?php echo $row['ticket_sub']; ?></div>
													<span class="text-muted"><?php echo $row['ticket_discription']; ?></span>
												</a>
											</td>
											<td class="text-center"> 
															<a href="<?php echo base_url('Client/resolved_ticket_status/').$row['ticket_id']; ?>" class="dropdown-item"><i class="icon-cross2 text-danger"></i> Close Ticket</a> 
											</td>
										</tr>
                                        <?php }?> 
									</tbody>
								</table>
							</div>
						</div>
						<div class="card"> 
							<div class="table-responsive">
								<table class="table text-nowrap">
									<thead>
										<tr>
											<th style="width: 50px">Date</th>
											<th style="width: 300px;">Project </th>
											<th style="width: 300px;">Description</th> 
										</tr>
									</thead>
									<tbody>
										<tr class="table-active table-border-double">
                                            
											<td colspan="3">Active tickets</td>
											<td class="text-right">
												<span class="badge bg-blue badge-pill"><?php echo $this->session->userdata('c_shedule_tickets_count');?></span>
											</td>
										</tr>
                                        <?php foreach($active as $row)
                                                    {?>
										<tr>
											<td class="text-center">
												<h6 class="mb-0"><?php echo date("d M Y",strtotime($row['created'])); ?></h6>
												 
                                            </td>
											<td>
												<div class="d-flex align-items-center">
												 	<div class="mr-3">
														 
													</div> 
													<div>
														 <?php
                                                     echo parse_url($row['c_url_link'], PHP_URL_HOST); ?> 
														<div class="text-muted font-size-sm"><span class="badge badge-mark border-blue mr-1"></span> Active</div>
													</div>
												</div>
											</td>
											<td>
												 
												<div class="font-weight-semibold"><?php echo $row['ticket_sub']; ?></div>
												<span class="text-muted"><?php echo $row['ticket_discription']; ?></span> 
											</td> 
											<td></td>
										</tr>
                                        <?php       }  ?> 
                                         
										<tr class="table-active table-border-double">
											<td colspan="3">Closed tickets</td>
											<td class="text-right">
												<span class="badge bg-danger badge-pill"><?php echo $this->session->userdata('c_closed_tickets_count');?></span>
											</td>
										</tr>
                                        <?php foreach($closed as $row)  {?> 
									<tr>
											<td class="text-center">
												<?php echo date("d M Y",strtotime($row['created'])); ?>
											</td>
											<td>
												<div class="d-flex align-items-center">
													 
													<div>
														 <?php
                                                     echo parse_url($row['c_url_link'], PHP_URL_HOST); ?> 
														<div class="text-muted font-size-sm"><span class="badge badge-mark border-danger mr-1"></span> Closed</div>
													</div>
												</div>
											</td>
											<td>
												 
													<div><?php echo $row['ticket_sub']; ?></div>
													<span class="text-muted"><?php echo substr($row['ticket_discription'],0,25)."..."; ?></span> 
											</td>
											<td></td>
											 
										</tr>
                                        <?php }?>
									</tbody>
								</table>
							</div>
						</div> 
					 
						<!-- /support tickets --> 
					</div>

					<div class="col-xl-4 col-md-4 col-lg-4 col-sm-12"> 

						<!-- My messages -->
						<div class="card">
							<div class="card-header header-elements-inline">
								<h6 class="card-title">Tickets Analysis</h6>
								<div class="header-elements">
									<span><i class="icon-history text-warning mr-2"></i>  <?php echo  date("M");?>, </span>
									<span id='ct' ></span>
									<span class="badge bg-success align-self-start ml-2">Online</span>
								</div>
							</div>

							<!-- Numbers -->
							<div class="card-body py-0">
								<div class="row text-center">
									<div class="col-6">
										<div class="mb-3">
											<h5 class="font-weight-semibold mb-0"><?php echo $last_day_count; ?></h5>
											<span class="text-muted font-size-sm">last day</span>
										</div>
									</div>

									<div class="col-6">
										<div class="mb-3">
											<h5 class="font-weight-semibold mb-0"><?php echo $last_week_count; ?></h5>
											<span class="text-muted font-size-sm">this week</span>
										</div>
									</div>

									 
								</div>
							</div>
							<!-- /numbers -->


							<!-- Area chart -->
							<div id="messages-stats"></div>
							<!-- /area chart -->


							<!-- Tabs -->
		                	<ul class="nav nav-tabs nav-tabs-solid nav-justified bg-indigo-400 border-x-0 border-bottom-0 border-top-indigo-300 mb-0">
								<li class="nav-item">
									<a href="#messages-tue" class="nav-link font-size-sm text-uppercase active" data-toggle="tab">
										Last Day
									</a>
								</li>

								<li class="nav-item">
									<a href="#messages-mon" class="nav-link font-size-sm text-uppercase" data-toggle="tab">
										this week
									</a>
								</li>

								 
							</ul>
							<!-- /tabs -->


							<!-- Tabs content -->
							<div class="tab-content card-body">
								<div class="tab-pane active fade show" id="messages-tue">
									<ul class="media-list">
										<?php  foreach($last_day_info as $row) {
											if(strlen($row['ticket_sub'])>15){
												$sub=substr($row['ticket_sub'], 0,15)."...";
												
											}
											else{
												$sub=$row['ticket_sub'];
											}
											if(strlen($row['ticket_discription'])>35){
												$desc=substr($row['ticket_discription'],0,25)."...";
												
											}
											else{
												$desc=$row['ticket_discription'];
											} 

											?>
										<li class="media">
											<div class="media-body">
												<div class="d-flex justify-content-between">
													<p style="color: green;"><?php echo $sub; ?></p>
													<span class="font-size-sm text-muted"><?php echo date("H:i",strtotime($row['updated'])); ?></span>
												</div>
												<div class="d-flex justify-content-between">
												<?php echo $desc; ?><br/>
												<?php /*echo ucfirst($row['status']);*/ ?>
												 <p> <?php echo ucfirst($row['status']); ?></p> 
												</div>
										</li>

										<?php } ?>
									</ul>
								</div>

								<div class="tab-pane fade" id="messages-mon">
									<ul class="media-list">
										<?php  foreach($week_info as $row) {
											if(strlen($row['ticket_sub'])>15){
												$sub=substr($row['ticket_sub'], 0,15)."...";
												
											}
											else{
												$sub=$row['ticket_sub'];
											}
											if(strlen($row['ticket_discription'])>35){
												$desc=substr($row['ticket_discription'],0,25)."...";
												
											}
											else{
												$desc=$row['ticket_discription'];
											} 

											?>
										<li class="media">
											<div class="media-body">
												<div class="d-flex justify-content-between">
													<p style="color: green;"><?php echo $sub; ?></p>
													<span class="font-size-sm text-muted"><?php echo date("d , M",strtotime($row['updated'])); ?></span>
												</div>
												<div class="d-flex justify-content-between">
												<?php echo $desc; ?>
												<p><!-- <span class="badge badge-mark border-blue mr-1"> --><?php echo ucfirst($row['status']); ?> 
												<!-- </span> --></p></div>
											</div>
										</li>

										<?php } ?>

									</ul>
								</div>

								 
							</div>

							<!-- /tabs content -->

						</div>
						<!-- /my messages -->

						<!-- Right sidebar component starts -->
					<?php $this->load->view('Client/client_right_sidebar_u');?>
					<!-- Right sidebar component ends-->
					 

					</div>
				</div>
				<!-- /dashboard content -->

			</div>
			<!-- /content area -->
			<script type="text/javascript"> 
function display_c(){
var refresh=1000; // Refresh rate in milli seconds
mytime=setTimeout('display_ct()',refresh)
}

function display_ct() {
var x = new Date()
var x1=x.getDate() ; 
x1 = x1 + " - " +  x.getHours( )+ ":" +  x.getMinutes()+ ":" +  x.getSeconds();
document.getElementById('ct').innerHTML = x1;
display_c();
 }
</script>
<?php $this->load->view('Client/Footerm'); ?>
 

			
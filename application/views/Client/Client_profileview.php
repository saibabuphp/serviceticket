 <?php $this->load->view('Client/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Client/Sidebar_m'); ?> 
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div> 
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

				 <?php $this->load->view('Client/Clnt_head_analytics_ticket_count');?> 
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
						<div class="row">
                          						<div class="card">
							<div class="card-body">
								<div class="mb-4">
									<div class="mb-3 text-center">
										<a href="#" class="d-inline-block">
											<img src="../../../../global_assets/images/placeholders/cover.jpg" class="img-fluid" alt="">
										</a>
									</div> 
									<h4 class="font-weight-semibold mb-1">
										<a href="#" class="text-default"><?php echo $Client_companydetails[0]['clint_company_name']; ?></a>
									</h4> 
									<ul class="list-inline list-inline-dotted text-muted mb-3">
										<li class="list-inline-item">By <a href="#" class="text-muted"><?php echo strtoupper($Client_companydetails[0]['clint_company_head_name']); ?></a></li>
										<li class="list-inline-item"></li> 
									</ul>
									<div class="row">
										<div class="col-lg-6">
											<ul class="list list-unstyled">
												<li>
													<i class="icon-checkmark3 text-success mr-2"></i>
													<?php echo $Client_companydetails[0]['clint_company_adres']; ?>
												</li> 
											</ul>
										</div>
										<div class="col-lg-6">
										</div>
									</div>
						<!-- About author -->
						<div class="card">
							<div class="card-header header-elements-inline">
								<h6 class="card-title">About the Founder</h6> 
							</div> 
							<div class="media card-body flex-column flex-md-row m-0">
								<div class="mr-md-3 mb-2 mb-md-0">
									<a href="#">
										<img src="<?php echo $Client_companydetails[0]['clint_company_head_pic']; ?>" class="rounded-circle" width="64" height="64" alt="">
									</a>
								</div> 
								<div class="media-body">
									<h6 class="media-title font-weight-semibold"><?php echo strtoupper($Client_companydetails[0]['clint_company_head_name']); ?></h6>
									<p>So slit more darn hey well wore submissive savage this shark aardvark fumed thoughtfully much drank when angelfish so outgrew some  </p> 
									<ul class="list-inline list-inline-dotted mb-0">
										 
										<li class="list-inline-item"><a href="#"><?php echo $Client_companydetails[0]['clint_company_head_mail']; ?></a></li>
										<li class="list-inline-item"><a href="#"><?php echo $Client_companydetails[0]['clint_company_head_ph']; ?></a></li>
										<li class="list-inline-item"><a href="<?php echo base_url().'Client/client_profile_update/'.$Client_companydetails[0]['clint_uid'];?>"  ><i class=icon-pencil4></i> <span>Edit</span></a></li>
									</ul>
								</div>
							</div>
						</div>
						<!-- /about author --> 
								</div>  
							</div>
						</div> 
              
						</div>
						<div class="row">
							 
								<div class="col-xl-12 card-header header-elements-inline">
									<h6 class="card-title">Projects Details</h6> 
								</div>
							 
					<?php //var_dump($Client_profil_det) ;exit();
                        if(!empty($Client_profil_det)){ foreach($Client_profil_det as $det){ ?> 
                 			<div class="col-xl-6">
								<div class="card border-left-3 border-left-success-400 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                                           
											<div>
												<h6><a href="#"><?php 
													if (strlen($det['c_url_link']) < 25) { $sub =  $det['c_url_link'];
														} else {    $sub = substr($det['c_url_link'], 0, 25). ' &nbsp;&nbsp;<a href="#">...</a>';
														} 
														echo parse_url($sub, PHP_URL_HOST); ?></a></h6> 
													</br> 
														<p>Controll paneel details</p>
                                                <span class="text-muted">url_config_file_uid:<?php echo $det['url_config_file_uid']; ?></span></br>
                                                 
                                                 <span class="text-muted">url_config_file_pwd:<?php echo $det['url_config_file_pwd']; ?></span></br>
                                                 <span class="text-muted">url_config_db_uid:<?php echo $det['url_config_db_uid']; ?></span> </br>
                                                  <span class="text-muted">url_config_db_pwd:<?php echo $det['url_config_db_pwd']; ?></span>  
											</div> 
											 
										</div>
									</div>
									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>Project Stated date: <span class="font-weight-semibold"><?php echo $new_date_format = date('d F, Y', strtotime($det['created']));	 ?></span></span> 
									</div> 
								</div>
							</div>
						<?php } }?> 

						</div>  
					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Client/client_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<?php $this->load->view('Client/Footerm'); ?>
 

			
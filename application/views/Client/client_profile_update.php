<?php $this->load->view('Client/Headm_links_validations'); ?>
 <style>

  .error{
  display: none;
  margin-left: 10px;
}   

.error_show{
  color: red;
  margin-left: 10px;
}


input.invalid, textarea.invalid{
  border: 2px solid red; 
}

input.valid, textarea.valid{
  border: 2px solid green;
}
 </style>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Client/Sidebar_m'); ?>
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

					 <?php $this->load->view('Client/Clnt_head_analytics_ticket_count');?>
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
						<div class="card">
						  <div class="card-body">
										   <form method="post" action="<?php echo base_url().'Client/client_company_profile_info_update'; ?>" enctype="multipart/form-data">
											<fieldset class="mb-12">
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3"> Company Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="name" id="name" value="<?php echo $client_profile_details[0]["clint_company_name"]; ?>" required>
														<span style="color: red" id="name_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Company Address</label>
													<div class="col-lg-9">
				                                 <!-- <textarea style="background-color:red;" rows="3" cols="3" class="form-control" name="addr"  id="addr" value="<?php echo $topay_company_bank[0]['adsress'];?>" required> 
				                                    </textarea> -->
				                                    <input type="text" class="form-control" name="addr" id="addr" value="<?php echo $client_profile_details[0]['clint_company_adres'];?>" required>
				                                        <span style="color: red" id="addr_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3"> Company Head Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="head_name" id="head_name" value="<?php echo $client_profile_details[0]["clint_company_head_name"]; ?>" required>
														<span style="color: red" id="head_name_alert"></span>
													</div>
												</div>
												
												
												 
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Company Head Email_id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="head_email" name="head_email" value="<?php echo $client_profile_details[0]['clint_company_head_mail'];?>" required>
														<span style="color: red" id="head_email_alert"></span>
													</div>
												</div>
												 <div class="form-group row">
													<label class="col-form-label col-lg-3">Company Head phone Number</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="head_phone" id="head_phone" value="<?php echo $client_profile_details[0]['clint_company_head_ph'];?>" required>
														<span style="color: red" id="head_phone_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3"> Company Contact Person Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="contact_name" id="contact_name" value="<?php echo $client_profile_details[0]["clint_company_contact_persion"]; ?>" required>
														<span style="color: red" id="contact_name_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Company Contact Person Email_id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="contact_email" name="contact_email" value="<?php echo $client_profile_details[0]['clint_company_contact_persion_mail'];?>" required>
														<span style="color: red" id="contact_email_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Company Contact Person phone Number</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="contact_phone" id="contact_phone" value="<?php echo $client_profile_details[0]['clint_company_contact_persion_phone'];?>" required>
														<span style="color: red" id="contact_phone_alert"></span>
													</div>
												</div>
				                                 
				                                
				                                
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Client status</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="client_sts" name="client_sts" value="<?php echo $client_profile_details[0]['client_status'];?>" required>
														<span style="color: red" id="client_sts_alert"></span>
														<input type="hidden" name="clint_uid" value="<?php echo $client_profile_details[0]['clint_uid'];?>">
													</div>
												</div>
												
				    					    </fieldset>
				                           
				                            

											<div class="text-right">
												<button type="submit" name="submit" id="client_update_submit"  class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
											</div>
<!-- value="client_update" -->
										</form>
                             </div>	
                         
              
						 </div>

			 

	
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Client/client_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->
</div>

		
			</div>
			<!-- /content area -->
	</div>
</div>
<script type="text/javascript">

	$(document).ready(function(){  
    $("#name").keyup(function(){
      return validatetext('name','name_alert'); 
});   });
	
	$(document).ready(function(){  
    $("#addr").keyup(function(){
      return validatetext('addr','addr_alert'); 
});   });
	$(document).ready(function(){  
    $("#head_name").keyup(function(){
      return validatetext('head_name','head_name_alert'); 
});   });
$(document).ready(function(){  
    $("#head_email").keyup(function(){
      return validemailid('head_email','head_email_alert'); 
});   });
$(document).ready(function(){  
    $("#head_phone").keyup(function(){
      return validphone('head_phone','head_phone_alert'); 
});   });

$(document).ready(function(){  
    $("#contact_name").keyup(function(){
      return validatetext('contact_name','contact_name_alert'); 
});   });
$(document).ready(function(){  
    $("#contact_email").keyup(function(){
      return validemailid('contact_email','contact_email_alert'); 
});   });
$(document).ready(function(){  
    $("#contact_phone").keyup(function(){
      return validphone('contact_phone','contact_phone_alert'); 
});   });
$(document).ready(function(){  
    $("#client_sts").keyup(function(){
      return validatenumber('client_sts','client_sts_alert'); 
});   });

				
$(document).ready(function(){ 
    $("#submit").click(function(){ 
      var name    = validatetext('name','name_alert');
      var addr     = validatetext('addr','addr_alert');
      var head_name     = validatetext('head_name','head_name_alert');
      var head_email    = validemailid('head_email','head_email_alert');
      var head_phone    =validphone('head_phone','head_phone_alert');
      var contact_name   = validatetext('contact_name','contact_name_alert');
      var contact_email    = validemailid('contact_email','contact_email_alert');
      var contact_phone    =validphone('contact_phone','contact_phone_alert');
       var client_sts    = validatenumber('client_sts','client_sts_alert');
      //var path     = validatetext('path','path_alert');
      

      if(name == 0 || addr == 0 || head_name == 0|| head_email == 0 || head_phone == 0 || contact_name == 0 ||contact_email == 0 || contact_phone== 0 || client_sts== 0 )
      {
        return false;
      }

      });  
  });    




</script>

<?php $this->load->view('Client/Footerm'); ?>
 

			
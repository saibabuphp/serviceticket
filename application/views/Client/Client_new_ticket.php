 <?php $this->load->view('Client/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Client/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

				 <?php $this->load->view('Client/Clnt_head_analytics_ticket_count');?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
						<div class="row">
                             
                        <?php //var_dump($Client_new_ticket) ;exit();
                        if(!empty($Client_new_ticket)){ foreach($Client_new_ticket as $det){ ?> 
                 			<div class="col-xl-6">
								<div class="card border-left-3 border-left-success-400 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                                           
											<div>
												<h6><a href="<?php echo base_url().'Client/c_rise_ticket_status/'.$det['ticket_id'];?>"><?php 
													if (strlen($det['ticket_sub']) < 25) { $sub =  $det['ticket_sub'];
														} else {    $sub = substr($det['ticket_sub'], 0, 25). ' &nbsp;&nbsp;<a href="#">...</a>';
														} 
														echo $sub; ?></a></h6>
												<p class="mb-3"><?php echo substr($det['ticket_discription'], 0, 70). '&nbsp;   more...'; ?></p> 
                                                
                                                
                                                 
                                                 <!--<span class="text-muted">Employee status:<?php echo $det['emp_tk_status']; ?></span> -->
											</div> 
											<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
												<li> Client Priority: &nbsp;<?php    
												echo $det['ticket_priority']; ?>  </li> 
												  <li> Request type : &nbsp;<?php    
												echo $det['request_type']; ?>  </li> 
												<li><span class="text-muted"><?php  
												echo $new_date_format = date('d F, Y', strtotime($det['created']));	 
												  ?></span></li>
                                                 
											</ul>
										</div>
									</div>

									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>Ticket raised date: <span class="font-weight-semibold"><?php echo $new_date_format = date('d F, Y', strtotime($det['created']));	 ?></span></span>

									 
									</div>
								</div>
							</div>
						<?php } }?>
              
						</div>

			 
					<?= $this->pagination->create_links() ?>
	
						<!-- Pagination -->
						<div class="d-flex justify-content-center mt-3 mb-3">
							<ul class="pagination">
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
							</ul>
						</div>
						<!-- /pagination -->

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Client/client_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<?php $this->load->view('Client/Footerm'); ?>
 

			
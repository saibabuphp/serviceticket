 <?php $this->load->view('Client/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Client/Sidebar_m'); ?> 
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

				 <?php $this->load->view('Client/Clnt_head_analytics_ticket_count');?> 
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
				 
						<div class="row">
							 
								<div class="col-xl-12 card-header header-elements-inline">
									<h6 class="card-title">Webgrid Details</h6> 
								</div>
							 
					<?php //var_dump($webgrid_details) ;exit();
                        if(!empty($webgrid_details)){ foreach($webgrid_details as $det){ ?> 
                 			<div class="col-xl-12">
								<div class="card border-left-3 border-left-success-400 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
                                           
											<div>
												<h6><a href="#"><?php  echo $det['url'];
													if (strlen($det['url']) < 25) { $sub =  $det['url'];
														} else {    $sub = substr($det['url'], 0, 25). ' &nbsp;&nbsp;<a href="#">...</a>';
														} 
														echo parse_url($sub, PHP_URL_HOST); ?></a></h6> 
													</br> 
														 
                                                <span class="text-muted">name:<?php echo $det['name']; ?></span></br>
                                                 
                                                 <span class="text-muted">mail_id:<?php echo $det['mail_id']; ?></span></br>
                                                 <span class="text-muted">phone_no:<?php echo $det['phone_no']; ?></span> </br>
                                                  <span class="text-muted">address:<?php echo $det['adsress']; ?></span>  
											</div> 
											 
										</div>
									</div>
									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>IFSC Code: <span class="font-weight-semibold"> :<?php echo  $det['ifcs_code'] ;?></span></span>
										<span>Account No: <span class="font-weight-semibold"> :<?php echo  $det['account_no'] ;?></span></span> 
										<span>Bank Name: <span class="font-weight-semibold"> :<?php echo  $det['bank_name'] ;?></span></span> 
										<span>Branch Name: <span class="font-weight-semibold">branch name :<?php echo  $det['ifcs_code'] ;?></span></span> 
										
									</div> 
								</div>
							</div>
						<?php } }?> 

						</div>  
					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Client/client_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<?php $this->load->view('Client/Footerm'); ?>
 

			
 
 <script src="<?php echo base_url().'assets/js/validations.js';?>"></script>
 <?php $this->load->view('Client/Headm_links'); ?>
 <style>

  .error{
  display: none;
  margin-left: 10px;
}   

.error_show{
  color: red;
  margin-left: 10px;
}


input.invalid, textarea.invalid{
  border: 2px solid red; 
}

input.valid, textarea.valid{
  border: 2px solid green;
}
 </style>
 
<body>
   
    <?php $this->load->view('Home/Headm_navbar'); ?>
  <!-- Page content -->
  <div class="page-content">

     <?php $this->load->view('Client/Sidebar_m'); ?>


    <!-- Main content -->
    <div class="content-wrapper">

      <!-- Page header -->
  <div class="card">
          <div class="card-header header-elements-inline">
            <h5 class="card-title">Client</h5>
            <div class="header-elements">
               
                    </div>
          </div>

          <div class="card-body">
            
            <!-- <form method="post" enctype="multipart/form-data" name="formdatafields" id="formdatafields"> -->
              <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
              <fieldset class="mb-3">
              
                                   <div class="form-group row">
                              <label class="col-form-label col-lg-2">Project Name</label>
                              <div class="col-lg-10">
                                  <select class="form-control" name="proj_name" id="proj_name" required="">
                                      <option>Select Project Name</option>
                                            <?php  
                                            foreach($porj_id->result() as $row) { 
                                                     $c_url_link=parse_url($row->c_url_link, PHP_URL_HOST);
                                        echo    "<option value=' $row->project_id  '> $c_url_link  </option>";    
                                             }?>
                                  </select>
                                  <span style="color: red" id="proj_name_alert"></span>
                                </div>
                            </div>
                                 <div class="form-group row">
                              <label class="col-form-label col-lg-2">Priority</label>
                              <div class="col-lg-10">
                                  <select class="form-control" name="priority"  id="priority" required="">
                                      <option>Select Priority</option>                                   
                                      <?php foreach($priority->result() as $row) {

                                        echo    "<option value='$row->id'>".ucfirst($row->priority_type)."  </option>";    
                                             }?>
                                  </select>
                                  <span style="color: red" id="priority_alert"></span>
                                </div>
                            </div>
                                 <div class="form-group row">
                              <label class="col-form-label col-lg-2">Request Type</label>
                              <div class="col-lg-10">
                                  <select class="form-control" name="req_type" id="req_type" required="">
                                      <option>Select Request Type</option>
                                      <?php foreach($request->result() as $row) {
                                        echo    "<option value='$row->id'>".ucfirst($row->reqirement_type)."  </option>";    
                                             }?>
                                  </select>
                                  <span style="color: red" id="req_type_alert"></span>
                                </div>
                            </div>
                            
                                <div class="form-group row">  
                                  <label class="col-form-label col-lg-2">Subject</label> 
                  <div class="col-lg-10">
                    <input type="text" name="subject" id="subject" placeholder="Sunbject Name *" class="form-control"> 
                    <span style="color: red" id="subject_alert"></span>
                  </div>
                </div>
                                <div class="form-group row">
                  <label class="col-form-label col-lg-2">Description</label>
                  <div class="col-lg-10">
                    <textarea rows="3" cols="3" name="desc" id="desc" class="form-control" placeholder="Description"></textarea>
                    <span style="color: red" id="desc_alert"></span>
                  </div>
                </div>
                                <div class="form-group row">
                  <label class="col-form-label col-lg-2">URL Path</label>
                  <div class="col-lg-10">
                    <input type="text" name="path" id="path"   class="form-control">
                    <span style="color: red" id="path_alert"></span>
                  </div>
                </div> 
                                <div class="form-group row">
                  <label class="col-form-label col-lg-2">phot1</label>
                  <div class="col-lg-10">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" name="phot1" id="phot1">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                      <span style="color: red" id="phot1_alert"></span>
                      <span style="color: red" id="sphoto_alert"></span>
                    </div>
                  </div>
                </div>
                 
                <div id="successMsg" class="alert alert-success" style="display: none;">successfully Raised ticket</div> 
            
              </fieldset>
                            <div class="text-right">
                              <input type="submit" value="Submit" id="btnSubmit"/>
  <!-- <button type="button" name="raiseticketsubmit" id="raiseticketsubmit" class="btn btn-primary" onclick="raise_newticket()">Submit <i class="icon-paperplane ml-2"></i></button> -->
              </div>
    
          </form>
          </div>
        </div>  <!-- /page header --> 
<script> 
$(document).ready(function () {

    $("#btnSubmit").click(function (event) {
 /*     var m = 1;
    var n = 1;
    var l = 1;
    var k = 1;
    var o = 1;
    var p = 1;


  var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    if($("#subject").val() == "" || $("#subject").val() == null)
        {
            $("#subject").focus();
            $("#subject_alert").html("Please enter Sunbject Name...");
            l++;
        }else{
            $("#subject_alert").html("");
        }
        if($("#desc").val() == "" || $("#desc").val() == null)
        {
            $("#desc").focus();
            $("#desc_alert").html("Please enter Description...");
            m++;
        }else{
            $("#desc_alert").html("");
        }
      if($("#path").val() == "" || $("#path").val()==null)
        {   
            $("#path").focus();
            $("#path_alert").html("Please enter proper URL...");
            n++;
        }
        else if(!regExp.test($("#path").val())){
            $("#path").focus();
            $("#path_alert").html("Please enter proper URL...");
            k++;
        }else{
            $("#path_alert").html("");
        } 
       var phot1_alertxt = $("#phot1").val();
        var assignext = phot1_alertxt.substring(phot1_alertxt.lastIndexOf('.') + 1);

        if($("#phot1").val() == "" || $("#phot1").val() == null)
        {
            $("#phot1").focus();
            $("#phot1_alert").html("Please upload image file...");
            o++;
        }else{

        if(assignext=="jpeg" || assignext=="jpg" || assignext=="png" || assignext=="gif" || assignext=="JPG" || assignext=="PNG"){
             $("#phot1_alert").html("");
        }else{ 
            $("#phot1").focus();
            $("#phot1_alert").html("Please upload image file...");
            o++;
        }
        if(priority.val() == "" || priority.val() == null || priority.val() == "Select Priority")
       {    
       priority.focus();
          $("#priority_alert").html("select priority.. reqired");
          p++;
          return false; 
       } else { $("#priority_alert").html(""); }
    }
if( l == 1 && m == 1 && n  == 1 &&  o  == 1 && k == 1 && p == 1  )
    {*/
        //stop submit the form, we will post it manually.
        event.preventDefault();
        // Get form
        var form = $('#fileUploadForm')[0];
    // Create an FormData object 
        var data = new FormData(form);
    // If you want to add an extra field for the FormData
        data.append("CustomField", "This is some extra data, testing");

    // disabled the submit button
        $("#btnSubmit").prop("disabled", true);
    var baseurl= 'http://localhost/serviceticket/';
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: baseurl+'Client/Rise_ticket',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
              document.getElementById("successMsg").style.display="block",
               window.location.href =  baseurl+'Client/Client_dashboard',
                $("#result").text(data),
                console.log("SUCCESS : ", data);               
                
                //$("#btnSubmit").prop("disabled", false);
            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });
        /* }else{
        alert("Please Enter form data");
    }*/

    });

}); 
$(document).ready(function(){
  $("#subject").keyup(function(){
    return validatetext('subject','subject_alert'); 
  });
});
$(document).ready(function(){
  $("#desc").keyup(function(){
    return validatetext('desc','desc_alert'); 
  });
}); 
$('#path').on('input', function() {
  var input=$(this);
  if (input.val().substring(0,4)=='www.'){input.val('http://www.'+input.val().substring(4));}
  var re = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&:\/~+#-]*[\w@?^=%&\/~+#-])?/;
  var is_url=re.test(input.val());
  if(is_url){   $("#path_alert").html(""); 
  }
  else{   $("#path_alert").html("Enter Corect URl Path "); 
  }
});
$(document).ready(function(){
    $("#priority").change(function(){
    var select = $("#priority option:selected").val();  
      validpriority();  
});   });  
function validpriority() 
{
 var priority = $("#priority");   
       if(priority.val() == "" || priority.val() == null || priority.val() == "Select Priority")
       {    priority.focus();
          $("#priority_alert").html("priority field reqired");
          return false; 
     }
     else { $("#priority_alert").html(""); }
}
$(document).ready(function(){
    $("#proj_name").change(function(){
    var select = $("#proj_name option:selected").val();  
      validproj_name(); 
});   });  
function validproj_name() 
{
 var proj_name = $("#proj_name");   
       if(proj_name.val() == "" || proj_name.val() == null || proj_name.val() == "Select Project Name")
       {    proj_name.focus();
          $("#proj_name_alert").html("Select Project Name field reqired");
          return false; 
     }
     else { $("#proj_name_alert").html(""); }
}

$(document).ready(function(){
    $("#req_type").change(function(){
    var select = $("#req_type option:selected").val();  
      validreq_type();  
});   });  
function validreq_type() 
{
 var req_type = $("#req_type");   
       if(req_type.val() == "" || req_type.val() == null || req_type.val() == "Select Request Type")
       {    req_type.focus();
          $("#req_type_alert").html("Select Project Name field reqired");
          return false; 
     }
     else { $("#req_type_alert").html(""); }
} 
$(document).ready(function(){
 $(document).on('change', '#phot1', function(){
  var name = document.getElementById("phot1").files[0].name;
  var form_data = new FormData();
  var ext = name.split('.').pop().toLowerCase(); 
  if(jQuery.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
  {
    $("#phot1_alert").html("Invalid Image format File"); 
  } else   {    $("#phot1_alert").html("");  }
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("phot1").files[0]);
  var f = document.getElementById("phot1").files[0];
  var fsize = f.size||f.fileSize;
  if(fsize > 200000)
  {
    $("#sphoto_alert").html("File Size is big"); 
  }
  else   {    $("#sphoto_alert").html("");  }
 });
}); 



function raise_newticket(){
  var m = 1;
    var n = 1;
    var l = 1;
    var k = 1;
    var o = 1;

  var re = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    if($("#subject").val() == "" || $("#subject").val() == null)
        {
            $("#subject").focus();
            $("#subject_alert").html("Please enter Sunbject Name...");
            l++;
        }else{
            $("#subject_alert").html("");
        }
        if($("#desc").val() == "" || $("#desc").val() == null)
        {
            $("#desc").focus();
            $("#desc_alert").html("Please enter Description...");
            m++;
        }else{
            $("#desc_alert").html("");
        }
      if($("#path").val() == "" || $("#path").val()==null)
        {   
            $("#path").focus();
            $("#path_alert").html("Please enter proper URL...");
            n++;
        }
        else if(!regExp.test($("#path").val())){
            $("#path").focus();
            $("#path_alert").html("Please enter proper URL...");
            k++;
        }else{
            $("#path_alert").html("");
        } 
    var phot1_alertxt = $("#phot1").val();
        var assignext = phot1_alertxt.substring(phot1_alertxt.lastIndexOf('.') + 1);

        if($("#phot1").val() == "" || $("#phot1").val() == null)
        {
            $("#phot1").focus();
            $("#phot1_alert").html("Please upload image file...");
            o++;
        }else{

        if(assignext=="jpeg" || assignext=="jpg" || assignext=="png" || assignext=="gif" || assignext=="JPG" || assignext=="PNG"){
             $("#phot1_alert").html("");
        }else{ 
            $("#phot1").focus();
            $("#phot1_alert").html("Please upload image file...");
            o++;
        }
    }
  if( 1==1 )
    {  
    //var form = $('#formdatafields')[0]; 
     
    // var baseurl= 'http://localhost/serviceticket/';
    // var form = $('#formdatafields')[0];
    // var data = new FormData(form);
    // $.ajax({
    // type: "POST",
    // url:baseurl+'Client/Rise_ticket',                                    
    // data:data,
    //     enctype: 'multipart/form-data',
    //     processData: false,  // Important!
    //     contentType: false,
    //     cache: false,
    // success: function (data) { 
    // //$("#result").text(data);
    //  //console.log("SUCCESS : ", data);      
    //   alert(data);
    //   //$("#raiseticketsubmit").prop("disabled", false);
    //  //$("#newticketdetails").html(response);     
    // },
    // error: function(e){ 
    //   alert("asas");
    //  // $("#result").text(e.responseText);
    //             console.log("ERROR : ", e);
    // alert('Error: ' + e.responseText); 
    // return false; 
    //   }

    // });
    $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: baseurl+'Client/Rise_ticket',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            success: function (data) {
                alert(data);
                $("#result").text(data);
                console.log("SUCCESS : ", data);
                $("#btnSubmit").prop("disabled", false);

            },
            error: function (e) {

                $("#result").text(e.responseText);
                console.log("ERROR : ", e);
                $("#btnSubmit").prop("disabled", false);

            }
        });

            
    }else{
        alert("Please Enter form data");
    }
}
</script>
      <!-- Content area -->
      <div class="content">

        <!-- Main charts -->
      <!-- /main charts -->


        <!-- Dashboard content -->
        <div class="row">
    
          <div class="col-xl-4">

            <!-- Progress counters -->
            <!-- /progress counters -->


            <!-- Daily sales -->
            <!-- /daily sales -->


            <!-- My messages -->
            <!-- /my messages -->


            <!-- Daily financials -->
          <!-- /daily financials -->

          </div>
        </div>
        <!-- /dashboard content -->

      </div>
      <!-- /content area -->
<?php $this->load->view('Client/Footerm'); ?>
 

      


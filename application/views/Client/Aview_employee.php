 <?php $this->load->view('Client/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Client/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

				 <?php $this->load->view('Client/Clnt_head_analytics_ticket_count');?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
				<div class="row">

					<?php  if(!empty($emp_info)){ foreach ($emp_info as $row) { ?>
					 <div class="col-xl-3 col-sm-6">
								<div class="card">
									<div class="card-body text-center">
										<div class="card-img-actions d-inline-block mb-3">
											<img class="img-fluid rounded-circle" src="1905sanpweb.jpg" width="170" height="170" alt="">
											<div class="card-img-actions-overlay card-img rounded-circle">
												<!-- <a href="#" class="btn btn-outline bg-white text-white border-white border-2 btn-icon rounded-round">
													<i class="icon-plus3"></i>
												</a> -->
												
													<i class="icon-link"></i>
												
											</div>
										</div>

							    		<h6 class="font-weight-semibold mb-0"><?php echo ucfirst($row['emp_name']); ?></h6>
							    		<span class="d-block text-muted"><?php echo $row['user_name']; ?> </span> 
							    	</div>
						    	</div>
							</div>
							<?php	} } ?>
					</div>

			 

	
						<!-- Pagination -->
						<!-- <div class="d-flex justify-content-center mt-3 mb-3">
							<ul class="pagination">
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
							</ul>
						</div> -->
						<!-- /pagination -->

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Client/client_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<?php $this->load->view('Client/Footerm'); ?>
 

			
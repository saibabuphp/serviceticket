<?php $this->load->view('Client/Headm_links_validations'); ?>
 <style>

  .error{
  display: none;
  margin-left: 10px;
}    

.error_show{
  color: red;
  margin-left: 10px; 
}
 

input.invalid, textarea.invalid{
  border: 2px solid red; 
}

input.valid, textarea.valid{
  border: 2px solid green;
}
 </style>
<body>
<?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		  <?php $this->load->view('Emp/Sidebar_m'); ?>
		<!-- Main content -->
		<div class="content-wrapper"> 
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
					 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content"> 

					
				 	<?php /*$this->load->view('Emp/employee_personal_profile_info_update');*/ ?>
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						<!-- Task grid -->
						<div class="card">
						  <div class="card-body">
										   <form method="post" action="<?php echo base_url().'Emp/employee_personal_profile_info_update'; ?>" enctype="multipart/form-data">
											<fieldset class="mb-12">
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3"> Employee Name</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="emp_name" id="emp_name" value="<?php echo $employee_profile_details[0]["emp_name"]; ?>" required>
														<span style="color: red" id="emp_name_alert"></span>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Employee Pic</label>
													<div class="col-lg-9">
														<img height="60" src="<?php echo base_url('assets/images/emp/').$employee_profile_details[0]['emp_pic']; ?>" id="emp_img" name="emp_img" value=""  /> 
														<input type="file" class="form-control h-auto" name="update_img" id="update_img" value="">
														 <input type="hidden" name="emp_pic" value="<?php echo $employee_profile_details[0]['emp_id'];?>">
														<span style="color: red" id="emp_name_alert"></span>
													</div>
												</div>
												
												 
				                                <div class="form-group row">
													<label class="col-form-label col-lg-3">Email_id</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="emp_email" name="emp_email" value="<?php echo $employee_profile_details[0]['emp_mail'];?>" required>
														<span style="color: red" id="emp_email_alert"></span>
													</div>
												</div>
												 <div class="form-group row">
													<label class="col-form-label col-lg-3">Employee Gender</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="gender" id="gender" value="<?php echo $employee_profile_details[0]['emp_gender'];?>" required>
														<span style="color: red" id="gender_alert"></span>
													</div>
												</div>
												 <!-- <div class="form-group row">
		                        	<label class="col-form-label col-lg-2">Gender</label>
		                        	<div class="col-lg-10">
			                            <select class="form-control" name="gender" id="gender" required>
			                                <option value="< ?php echo $employee_profile_details[0]['emp_gender'];?>"></option>
			                                <option value="1">Male</option>
			                                <option value="2">Female</option>
			                                <option value="3">Transgender</option>
			                            </select>
			                            <span style="color: red" id="emp_gender_alert"></span>
		                            </div>
		                        </div> -->
												
												<div class="form-group row">
													<label class="col-form-label col-lg-3"> phone Number</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" name="contact_phone" id="contact_phone" value="<?php echo $employee_profile_details[0]['phone_no'];?>" required>
														<span style="color: red" id="contact_phone_alert"></span>
													</div>
												</div>
				                  
				                                
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Date Of Birth</label>
													<div class="col-lg-9">
														<input type="date" class="form-control" id="d_o_b" name="d_o_b" value="<?php echo $employee_profile_details[0]['dob'];?>" required>
														<span style="color: red" id="d_o_b_alert"></span>
													 
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Date Of Birth</label>
													<div class="col-lg-9">
														<input type="date" class="form-control" id="doj" name="doj" value="<?php echo $employee_profile_details[0]['doj'];?>" required>
														<span style="color: red" id="doj_alert"></span>
														 
													</div>
												</div>
												<div class="form-group row">
													<label class="col-form-label col-lg-3">Address</label>
													<div class="col-lg-9">
														<input type="text" class="form-control" id="addr" name="addr" value="<?php echo $employee_profile_details[0]['address'];?>" required>
														<span style="color: red" id="addr_alert"></span>
														<input type="hidden" name="emp_uid" value="<?php echo $employee_profile_details[0]['emp_id'];?>">
													</div>
												</div>
												
				    					    </fieldset>
				                           
				                            

											<div class="text-right">
												<button type="submit" name="submit" id="emp_update_submit"  class="btn btn-primary">Submit <i class="icon-paperplane ml-2"></i></button>
											</div>
<!-- value="client_update" -->
										</form>
                             </div>	
                         
              
						 </div>

			 

	
						 

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Emp/emp_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
			<script>
 
 

$(document).ready(function(){  
    $("#emp_name").keyup(function(){
      return validatetext('emp_name','emp_name_alert'); 
});   });
$(document).ready(function(){  
    $("#emp_email").keyup(function(){
      return validemailid('emp_email','emp_email_alert'); 
});   });
$(document).ready(function(){  
    $("#contact_phone").keyup(function(){
      return validmobile('contact_phone','contact_phone_alert'); 
});   });

 

$(document).ready(function(){  
    $("#d_o_b").change(function(){
      return validatetext('d_o_b','d_o_b_alert'); 
});   });
$(document).ready(function(){  
    $("#doj").change(function(){
      return validatetext('doj','doj_alert'); 
});   });
$(document).ready(function(){  
    $("#addr").change(function(){
      return validatetext('addr','addr_alert'); 
});   });
$(document).ready(function(){  
    $("#gender").change(function(){
      return validatetext('gender','gender_alert'); 
});   });




$(document).ready(function(){ 
    $("#submit").click(function(){

      var emp_name    = validatetext('emp_name','emp_name_alert');
      var emp_email     = validemailid('emp_email','emp_email_alert');
      var contact_phone    = validmobile('contact_phone','contact_phone_alert');
      var emp_phone    =validatetext('emp_phone','emp_phone_alert');
      var d_o_b   = validatetext('d_o_b','d_o_b_alert');
      var doj   = validatetext('doj','doj_alert');
      var addr = validatetext('addr','addr_alert');
      //var path     = validatetext('path','path_alert');
      

      if(emp_name == 0 ||  emp_email == 0|| contact_phone == 0 || gender == 0 || emp_addr == 0 || d_o_b ==0 || doj ==0 || addr == 0)
      {
        return false;
      }

      });  
  });    

</script>


<?php $this->load->view('Client/Footerm'); ?>
 

			
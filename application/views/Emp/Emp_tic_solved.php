<?php $this->load->view('Client/Headm_links_validations'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Emp/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
			 
				</div>

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
			 
				</div> 
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				<!-- Main charts -->
			<?php $this->load->view('Emp/Emp_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						
                            <div class="card">
							<div class="card-header header-elements-md-inline">
								<h3 class="card-title text-primary">Assigned Ticket</h3><br>
                                
								<!--<div class="header-elements">
									<a href="#" class="btn bg-teal-400 btn-sm btn-labeled btn-labeled-right">Check in <b><i class="icon-alarm-check"></i></b></a>
			                	</div>-->
							</div>

							<div class="card-body"> 
								<div class="row container-fluid"> 
									<div class="col-md-6">
										<dl> 
				                            <dt class="font-size-sm text-primary text-uppercase">Project Name</dt>
				                            <dd><?php echo $tic_info[0]['c_url_link']; ?></dd>				                               
				                        </dl>
										<div class="card">
											<div class="card-header bg-transparent header-elements-inline">
												<span class="card-title font-weight-semibold">Control panel Details</span>
												<div class="header-elements">
													<div class="list-icons">
								                		<a class="list-icons-item" data-action="collapse"></a>
							                		</div>
						                		</div>
											</div>
											<div class="card-body" style="">
												<form action="#">
													<div class="form-group-feedback form-group-feedback-right">
														<dl>
				                                	<dt class="font-size-sm text-primary text-uppercase">File Manager Details</dt>	
				                                	<dd><?php echo $tic_info[0]['url_config_file_uid']; ?></dd>
				                                	<dd><?php echo $tic_info[0]['url_config_file_pwd']; ?></dd>
				                                
				                                	<dt class="font-size-sm text-primary text-uppercase">Database Details</dt>	
				                                	<dd><?php echo $tic_info[0]['url_config_db_uid']; ?></dd>
				                                	<dd><?php echo $tic_info[0]['url_config_db_pwd']; ?></dd>
				                                
				                            		</dl>
													</div>
												</form>
											</div>
										</div> 
									</div> 
									<div class="col-md-6"> 
											<dl>
				                                <dt class="font-size-sm text-primary text-uppercase">Image Information</dt>
				                                <dd><img src="<?php echo base_url('images/tickets/').$tic_info[0]['ticket_pics']; ?>" target="_blank" height="300" width="300"></dd>
				                            </dl>
			                        </div> 
			                        <!-- <div class="col-md-12"> 
			                        	<h3 class="card-title text-primary"><?php echo $tic_info[0]['ticket_sub']; ?></h3>
				                        <h6 class="font-weight-semibold">Descritption</h6>
								<p class="mb-3"><?php echo $tic_info[0]['ticket_discription']; ?></p>
								<h6 class="font-weight-semibold">Problem raised Path</h6>
								<p class="mb-4"><?php echo $tic_info[0]['url_link']; ?></p> 
			                        </div>  -->
			                            <div class="col-md-6"> 
			                        	<h3 class="card-title text-primary"><?php echo $tic_info[0]['ticket_sub']; ?></h3> 
								 
										<h6 class="font-weight-semibold">Admin Assigned date: <?php echo $tic_info[0]['admin_assigned_toemp']; ?></h6>  
										<h6 class="font-weight-semibold">Need to Compleate Time: <?php echo $tic_info[0]['admin_time']; ?> Minits</h6>
										<h6 class="font-weight-semibold">My Estimated  Time: <?php echo $tic_info[0]['emp_estimate_time']; ?> Minits</h6>
										 
			                        </div> 
			                        <div class="col-md-6"> 
			                        	 
				                        <h6 class="font-weight-semibold">Descritption</h6>
										<p class="mb-3"><?php echo $tic_info[0]['ticket_discription']; ?></p>
										<h6 class="font-weight-semibold">Problem raised Path</h6>
										<p class="mb-4"><?php echo $tic_info[0]['url_link']; ?></p> 
			                        </div> 
			                    </div>

                                
                <div class="card"> 
					<div class="card-body"> 
						<form action="<?php echo base_url().'Emp/Emp_tic_solve_update'; ?>" method="post">
							<fieldset class="mb-3">
							   <legend class="text-uppercase font-size-sm font-weight-bold">Completed Time</legend>
                                <input type="hidden" class="form-control" name="tic_id" id="clickable-label" value="<?php echo $tic_info[0]['ticket_id']; ?>" required="">
                                
								<div class="form-group row">
									<label class="col-form-label col-lg-4 cursor-pointer" for="clickable-label">Estimated Time (Min)</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" name="cmp_time" id="cmp_time" value="" placeholder="Time In Minutes Only" required="">
										<span style="color: red" id="cmp_time_alert"></span>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-form-label col-lg-4">Message</label>
									<div class="col-lg-8">
									<textarea rows="3" cols="3" name=" emp_msg_solved" class="form-control" placeholder="Message"></textarea>
									</div>
								</div>
 
							
							</fieldset>
                            <div class="text-right">
								<button type="submit" id="a_assign_ticket_to_emp_submit" name="a_assign_ticket_to_emp_submit" class="btn btn-primary">Solved <i class="icon-paperplane ml-2"></i></button>
							</div>
						</form>
					</div>
				</div>
                                
	       		</div>

							 
						</div>

		 
						<!-- Pagination -->
						<div class="d-flex justify-content-center mt-3 mb-3">
							<ul class="pagination">
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
							</ul>
						</div>
						<!-- /pagination -->

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Emp/emp_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<?php $this->load->view('Home/Footerm'); ?>
 
<script type="text/javascript">  
	$(document).ready(function(){  
    $("#cmp_time").change(function(){
      return validatenumber('cmp_time','cmp_time_alert'); 
});   });
	 
 
$(document).ready(function(){ 
    $("#a_assign_ticket_to_emp_submit").click(function(){ 
      var cmp_time    = validatetext('cmp_time','cmp_time_alert'); 
      if(cmp_time == 0 )
      {
        return false;
      } 
      });  
  }); 

</script>
			
 <?php $this->load->view('Home/Headm_links'); ?>
<body>
	 <?php $this->load->view('Home/Headm_navbar'); ?>
	 
	<!-- Page content -->
	<div class="page-content">

		 <?php $this->load->view('Emp/Sidebar_m'); ?>


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="page-header-content header-elements-md-inline">
				 
				</div> 

				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					 
				</div>
			</div>
			<!-- /page header -->


			<!-- Content area -->
			<div class="content">

				<!-- Main charts -->
			<?php $this->load->view('Emp/Emp_head_analytics_ticket_count'); ?>
                
                
                	<div class="d-flex align-items-start flex-column flex-md-row">

					<!-- Left content -->
					<div class="w-100 overflow-auto order-2 order-md-1">

						 
						
                            <div class="row">
                             
                        <?php //var_dump($newtickets) ;exit();
                        if(!empty($progress)){ foreach($progress as $det){ ?> 
                                <div class="col-xl-6">
                                        <div class="card border-left-3 border-left-success-400 rounded-left-0">
                                        <div class="card-body">
                                        <div class="d-sm-flex align-item-sm-center flex-sm-nowrap">

                                        <div>
                                        <?php 
													if (strlen($det['ticket_sub']) < 25) { $sub =  $det['ticket_sub'];
														} else {    $sub = substr($det['ticket_sub'], 0, 25). ' &nbsp;&nbsp;<a href="#">...</a>';
														} ?>
												<h6><a href="<?php echo base_url().'Emp/Emp_tic_solved/'.$det['ticket_id'];?>">
														<?php echo $sub; ?></a></h6>
												<p class="mb-3"><?php echo substr($det['ticket_discription'], 0, 70). '&nbsp; <a href="#">  more..</a>'; ?></p>
                                                                                      
                                        </div>

                                        <ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
                                        <li> Admin Priority: &nbsp;<?php    
                                        echo $det['priority_type']; ?>  </li>
                                        <li><span class="text-muted">Estimated Time (MIN) : <?php  
                                        echo $det['emp_estimate_time'];	
                                        //echo $new_date_format;
                                         ?></span></li> 

                                        </ul>
                                        </div>
                                        </div>

                                        <div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
                                        <span>Accepted: <span class="font-weight-semibold"><?php echo $new_date_format = date('d F, Y', strtotime($det['emp_accept_time']));	?></span></span>


                                        </div>
                                        </div>
                                        </div>
                                        <?php } }?>    
                                    </div>

			<!--			<div class="row">
							<div class="col-xl-6">
								<div class="card border-left-3 border-left-grey-300 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
											<div>
												<h6><a href="task_manager_detailed.html">#22. Create ad campaign banners set</a></h6>
												<p class="mb-3">That he had recently cut out of an illustrated magazine..</p>

							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#" class="btn btn-icon bg-transparent border-slate-300 text-slate rounded-round border-dashed"><i class="icon-plus22"></i></a>
											</div>

											<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
												<li><span class="text-muted">10 January, 2015</span></li>
												<li class="dropdown">
							                		Priority: &nbsp; 
													<a href="#" class="badge bg-grey-300 align-top dropdown-toggle" data-toggle="dropdown">Low</a>
													<div class="dropdown-menu dropdown-menu-right">
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-danger"></span> Blocker</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-warning-400"></span> High priority</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-success"></span> Normal priority</a>
														<a href="#" class="dropdown-item active"><span class="badge badge-mark mr-2 border-grey-300"></span> Low priority</a>
													</div>
												</li>
												<li><a href="#">Singular website</a></li>
											</ul>
										</div>
									</div>

									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>Due: <span class="font-weight-semibold">22 hours</span></span>

										<ul class="list-inline mb-0 mt-2 mt-sm-0">
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">Resolved</a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item">Open</a>
													<a href="#" class="dropdown-item">On hold</a>
													<a href="#" class="dropdown-item active">Resolved</a>
													<a href="#" class="dropdown-item">Closed</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item">Dublicate</a>
													<a href="#" class="dropdown-item">Invalid</a>
													<a href="#" class="dropdown-item">Wontfix</a>
												</div>
											</li>
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item"><i class="icon-alarm-add"></i> Check in</a>
													<a href="#" class="dropdown-item"><i class="icon-attachment"></i> Attach screenshot</a>
													<a href="#" class="dropdown-item"><i class="icon-rotate-ccw2"></i> Reassign</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item"><i class="icon-pencil7"></i> Edit task</a>
													<a href="#" class="dropdown-item"><i class="icon-cross2"></i> Remove</a>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="col-xl-6">
								<div class="card border-left-3 border-left-success-400 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
											<div>
												<h6><a href="task_manager_detailed.html">#21. Edit the draft for the icons</a></h6>
												<p class="mb-3">You've got to get enough sleep. Other travelling salesmen..</p>

							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#" class="btn btn-icon bg-transparent border-slate-300 text-slate rounded-round border-dashed"><i class="icon-plus22"></i></a>
											</div>

											<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
												<li><span class="text-muted">4 January, 2015</span></li>
												<li class="dropdown">
							                		Priority: &nbsp; 
													<a href="#" class="badge bg-success-400 align-top dropdown-toggle" data-toggle="dropdown">Normal</a>
													<div class="dropdown-menu dropdown-menu-right">
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-danger"></span> Blocker</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-warning-400"></span> High priority</a>
														<a href="#" class="dropdown-item active"><span class="badge badge-mark mr-2 border-success"></span> Normal priority</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-grey-300"></span> Low priority</a>
													</div>
												</li>
												<li><a href="#">Corelius app</a></li>
											</ul>
										</div>
									</div>

									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>Due: <span class="font-weight-semibold">27 hours</span></span>

										<ul class="list-inline mb-0 mt-2 mt-sm-0">
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">Invalid</a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item">Open</a>
													<a href="#" class="dropdown-item">On hold</a>
													<a href="#" class="dropdown-item">Resolved</a>
													<a href="#" class="dropdown-item">Closed</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item">Dublicate</a>
													<a href="#" class="dropdown-item active">Invalid</a>
													<a href="#" class="dropdown-item">Wontfix</a>
												</div>
											</li>
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item"><i class="icon-alarm-add"></i> Check in</a>
													<a href="#" class="dropdown-item"><i class="icon-attachment"></i> Attach screenshot</a>
													<a href="#" class="dropdown-item"><i class="icon-rotate-ccw2"></i> Reassign</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item"><i class="icon-pencil7"></i> Edit task</a>
													<a href="#" class="dropdown-item"><i class="icon-cross2"></i> Remove</a>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>-->

			<!--			<div class="row">
							<div class="col-xl-6">
								<div class="card border-left-3 border-left-warning-400 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
											<div>
												<h6><a href="task_manager_detailed.html">#20. Fix validation issues and commit</a></h6>
												<p class="mb-3">But who knows, maybe that would be the best thing for me..</p>

							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#" class="btn btn-icon bg-transparent border-slate-300 text-slate rounded-round border-dashed"><i class="icon-plus22"></i></a>
											</div>

											<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
												<li><span class="text-muted">28 December, 2014</span></li>
												<li class="dropdown">
							                		Priority: &nbsp; 
													<a href="#" class="badge bg-warning-400 align-top dropdown-toggle" data-toggle="dropdown">High</a>
													<div class="dropdown-menu dropdown-menu-right">
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-danger"></span> Blocker</a>
														<a href="#" class="dropdown-item active"><span class="badge badge-mark mr-2 border-warning-400"></span> High priority</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-success"></span> Normal priority</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-grey-300"></span> Low priority</a>
													</div>
												</li>
												<li><a href="#">Singular app</a></li>
											</ul>
										</div>
									</div>

									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>Due: <span class="font-weight-semibold">18 hours</span></span>

										<ul class="list-inline mb-0 mt-2 mt-sm-0">
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">Resolved</a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item">Open</a>
													<a href="#" class="dropdown-item">On hold</a>
													<a href="#" class="dropdown-item active">Resolved</a>
													<a href="#" class="dropdown-item">Closed</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item">Dublicate</a>
													<a href="#" class="dropdown-item">Invalid</a>
													<a href="#" class="dropdown-item">Wontfix</a>
												</div>
											</li>
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item"><i class="icon-alarm-add"></i> Check in</a>
													<a href="#" class="dropdown-item"><i class="icon-attachment"></i> Attach screenshot</a>
													<a href="#" class="dropdown-item"><i class="icon-rotate-ccw2"></i> Reassign</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item"><i class="icon-pencil7"></i> Edit task</a>
													<a href="#" class="dropdown-item"><i class="icon-cross2"></i> Remove</a>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div class="col-xl-6">
								<div class="card border-left-3 border-left-grey-300 rounded-left-0">
									<div class="card-body">
										<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
											<div>
												<h6><a href="task_manager_detailed.html">#19. Support tickets list doesn't support commas</a></h6>
												<p class="mb-3">I'd have gone up to the boss and told him just what i think..</p>

							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#">
							                		<img src="../../../../global_assets/images/placeholders/placeholder.jpg" class="rounded-circle" width="36" height="36" alt="">
						                		</a>
							                	<a href="#" class="btn btn-icon bg-transparent border-slate-300 text-slate rounded-round border-dashed"><i class="icon-plus22"></i></a>
											</div>

											<ul class="list list-unstyled mb-0 mt-3 mt-sm-0 ml-auto">
												<li><span class="text-muted">20 November, 2014</span></li>
												<li class="dropdown">
							                		Priority: &nbsp; 
													<a href="#" class="badge bg-grey-300 align-top dropdown-toggle" data-toggle="dropdown">Low</a>
													<div class="dropdown-menu dropdown-menu-right">
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-danger"></span> Blocker</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-warning-400"></span> High priority</a>
														<a href="#" class="dropdown-item"><span class="badge badge-mark mr-2 border-success"></span> Normal priority</a>
														<a href="#" class="dropdown-item active"><span class="badge badge-mark mr-2 border-grey-300"></span> Low priority</a>
													</div>
												</li>
												<li><a href="#">Singular app</a></li>
											</ul>
										</div>
									</div>

									<div class="card-footer d-sm-flex justify-content-sm-between align-items-sm-center">
										<span>Due: <span class="font-weight-semibold">30 hours</span></span>

										<ul class="list-inline mb-0 mt-2 mt-sm-0">
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown">Closed</a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item">Open</a>
													<a href="#" class="dropdown-item">On hold</a>
													<a href="#" class="dropdown-item">Resolved</a>
													<a href="#" class="dropdown-item active">Closed</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item">Dublicate</a>
													<a href="#" class="dropdown-item">Invalid</a>
													<a href="#" class="dropdown-item">Wontfix</a>
												</div>
											</li>
											<li class="list-inline-item dropdown">
												<a href="#" class="text-default dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>

												<div class="dropdown-menu dropdown-menu-right">
													<a href="#" class="dropdown-item"><i class="icon-alarm-add"></i> Check in</a>
													<a href="#" class="dropdown-item"><i class="icon-attachment"></i> Attach screenshot</a>
													<a href="#" class="dropdown-item"><i class="icon-rotate-ccw2"></i> Reassign</a>
													<div class="dropdown-divider"></div>
													<a href="#" class="dropdown-item"><i class="icon-pencil7"></i> Edit task</a>
													<a href="#" class="dropdown-item"><i class="icon-cross2"></i> Remove</a>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>-->

	
						<!-- Pagination -->
						<div class="d-flex justify-content-center mt-3 mb-3">
							<ul class="pagination">
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-right"></i></a></li>
								<li class="page-item active"><a href="#" class="page-link">1</a></li>
								<li class="page-item"><a href="#" class="page-link">2</a></li>
								<li class="page-item"><a href="#" class="page-link">3</a></li>
								<li class="page-item"><a href="#" class="page-link">4</a></li>
								<li class="page-item"><a href="#" class="page-link">5</a></li>
								<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-small-left"></i></a></li>
							</ul>
						</div>
						<!-- /pagination -->

					</div>
					<!-- /left content -->


					<!-- Right sidebar component starts -->
					<?php $this->load->view('Emp/emp_right_sidebar'); ?>
					<!-- Right sidebar component ends-->

				</div>
				
				<!-- /main charts -->


				
			</div>
			<!-- /content area -->
<?php $this->load->view('Home/Footerm'); ?>
 

			
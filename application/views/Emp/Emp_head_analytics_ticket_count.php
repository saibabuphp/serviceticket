<!-- Main charts -->
				<?php
		/*		$newtickets_count=count($this->Empmodel->get_emp_newtickets());
		$shedule_tickets_count=count($this->Empmodel->get_emp_inprogress());
		 $resolved_count=count($this->Empmodel->get_emp_resolved());
		$closed_tickets_count=count($this->Empmodel->get_emp_closed_tickets());
		class="col-lg-2 col-md-2 col-sm-6 "
		*/
		?>
				<div class="row">
					<div class="col-xl-12"> 

						<!-- Traffic sources -->
						<div class="card" class="col-lg-2 col-md-2 col-sm-6 ">
							<div class="card-header header-elements-inline">
								<h6 class="card-title">Tickets</h6>
								<div class="header-elements">
									<div class="form-check form-check-right form-check-switchery form-check-switchery-sm">
										<label class="form-check-label">
											Live update:
											<input type="checkbox" class="form-input-switchery" checked data-fouc>
										</label>
									</div>
								</div>
							</div>

							<div class="card-body py-0">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-6">
										<div class="d-flex align-items-center   mb-2">
											<a href="#" class="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
												<i class="icon-plus3"></i>
											</a>
											<div>
												<div class="font-weight-semibold">New Tickets</div>
												<span class="text-muted"><span class="badge badge-mark border-success mr-2"></span><?php echo $this->session->userdata('e_newtickets_count');?></span>
											</div>
										</div>
										<div class="w-75 mx-auto mb-3" id="new-visitors"></div>
									</div>

									

									<div class="col-lg-3 col-md-3 col-sm-6  ">
										<div class="d-flex align-items-center   mb-2">
											<a href="#" class="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon mr-3">
												<i class="icon-make-group"></i>
											</a>
											<div>
												<div class="font-weight-semibold">Progressed Tickets</div>
												<span class="text-muted"><span class="badge badge-mark border-success mr-2"></span> <?php echo $this->session->userdata('e_shedule_tickets_count');?></span>
											</div>
										</div>
										<div class="w-75 mx-auto mb-3" id="total-online"></div>
									</div>
									 
									<div class="col-lg-3 col-md-3 col-sm-6  ">
										<div class="d-flex align-items-center   mb-2">
											<a href="#" class="btn bg-transparent border-indigo-400 text-indigo-400 rounded-round border-2 btn-icon mr-3">
												<i class="icon-copy"></i>
											</a>
											<div>
												<div class="font-weight-semibold">Resolved Tickets</div>
												<span class="text-muted"><span class="badge badge-mark border-success mr-2"></span> <?php echo $this->session->userdata('e_resolved_count');?></span>
											</div>
										</div>
										<div class="w-75 mx-auto mb-3" id="total-online"></div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6  ">
										<div class="d-flex align-items-center   mb-2">
											<a href="#" class="btn bg-transparent border-teal text-teal rounded-round border-2 btn-icon mr-3">
												<i class="icon-file-check"></i>
											</a>
											<div>
												<div class="font-weight-semibold">Closed Tickets</div>
												<span class="text-muted"><span class="badge badge-mark border-success mr-2"></span> <?php echo $this->session->userdata('e_closed_tickets_count');?></span>
											</div>
										</div>
										<div class="w-75 mx-auto mb-3" id="total-online"></div>
									</div>
								</div>
							</div>

							<div class="chart position-relative" id="traffic-sources"></div>
						</div>
						<!-- /traffic sources -->

					</div>
                </div>
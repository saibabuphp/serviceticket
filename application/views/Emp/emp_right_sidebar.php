			<!-- Right sidebar component -->
					<div class="sidebar sidebar-light bg-transparent sidebar-component sidebar-component-right border-0 shadow-0 order-1 order-md-2 sidebar-expand-md">

						<!-- Sidebar content -->
						<div class="sidebar-content">

							<!-- Search -->
						<!-- 	 <div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">Search ticket</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body"> 
									<form action="#">
										<div class="form-group-feedback form-group-feedback-right">
											<input type="search" class="form-control" placeholder="type and hit Enter">
											<div class="form-control-feedback">
												<i class="icon-search4 font-size-base text-muted"></i>
											</div>
										</div>
									</form>
								</div>
							</div>   -->
						 
						<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
								<span class="text-uppercase font-size-sm font-weight-semibold">Basic Details</span>
								<div class="header-elements">
									<div class="list-icons">
								         <a class="list-icons-item" data-action="collapse"></a>
								    </div>
								</div>
								</div>

								<div class="card-body p-0">
									<div class="nav nav-sidebar mb-2"> 
									 
									<!-- <li class="nav-item">
										<a href="<?php echo base_url().'Emp/Emp_History';?>" class="nav-link"> <i class="icon-history"></i>All tickets
										<span class="badge badge-primary badge-pill ml-auto">26</span> </a>
									</li> -->
									<li class="nav-item">
										<a href="<?php echo base_url().'Emp/Aview_projects';?>" class="nav-link"><i class="icon-portfolio"></i> Projects
										<span class="badge badge-primary badge-pill ml-auto">3</span> </a>
									</li> 
									</div>
								</div>
							</div>
							<!-- /search --> 

							<!-- My Actions  --> 
				 			<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">Actions</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body">
									<div class="row row-tile no-gutters">
										<div class="col-6">
											<a href="<?php echo base_url().'Emp/Emp_home';?>">
											<button type="button"  class="btn btn-light btn-block btn-float m-0">
												<i class="icon-reading"></i>
												<span>Assigned to me</span>
											</button></a>
										</div>
										 
										 


										<div class="col-6">		
										<a href="<?php echo base_url().'Emp/Emp_closed';?>">
											<button type="button" class="btn btn-light btn-block btn-float m-0">
												<i class="icon-file-check text-success-400 "></i>
												<span>My tickets closed</span>
											</button></a>
										</div>
									</div>
								</div>
							</div> 
							<!-- /actions -->
							<!-- My Actions  -->
				 			<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">My Actions</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body">
									<div class="row row-tile no-gutters"> 
										<div class="col-6">
											<a href="<?php echo base_url().'Emp/Emp_onprograss';?>"><button type="button" class="btn btn-light btn-block btn-float m-0">
												<i class="icon-make-group text-pink-400 icon-2x"></i>
												<span>progress</span>
											</button></a>
										</div>
										<div class="col-6">
											<a href="<?php echo base_url().'Emp/Emp_resolved';?>">
											<button type="button" class="btn btn-light btn-block btn-float m-0">
												<i class="icon-copy text-success-400"></i>
												<span>Resolved by me</span>
											</button></a>
										</div>
									</div>
								</div>
							</div> 
							<!-- /actions -->


							<!-- Navigation -->
					 		<!-- <div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="card-title font-weight-semibold">Tasks</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body p-0">
									<div class="nav nav-sidebar mb-2"> -->
										<!-- <li class="nav-item-header">Actions</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-googleplus5"></i>
												Create task
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-portfolio"></i>
												Create project
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-compose"></i>
												Edit task list
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-user-plus"></i>
												Assign users
												<span class="badge bg-success ml-auto">94 online</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-collaboration"></i>
												Create team
											</a>
										</li> 
										<li class="nav-item-header">Tasks</li>-->
										<!-- <li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-files-empty"></i>
												All tasks
											</a>
										</li> -->
										<!-- <li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-file-plus"></i>
												Active tasks
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-file-check"></i>
												Closed tasks
											</a>
										</li> -->
										<!-- <li class="nav-item-divider"></li> -->
										<!-- <li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-reading"></i>
												Assigned to me
												<span class="badge bg-info badge-pill ml-auto">86</span>
											</a>
										</li>
										<li class="nav-item">
											<a href="#" class="nav-link">
												<i class="icon-make-group"></i>
												Assigned to my team
												<span class="badge bg-info badge-pill ml-auto">47</span>
											</a>
										</li> -->
										 
									<!-- </div>
								</div>
							</div>  -->
							<!-- /navigation -->


						 <!-- calcilations for display prograss chat in rightside start -->
							<?php
							  /*echo $this->session->userdata('e_assigned_tickets_count'); 
							 echo $this->session->userdata('e_shedule_tickets_count'); */ 
							 //prograss bar for prograss tickets
							 $e_pb_Progress=round((100*$this->session->userdata('e_shedule_tickets_count'))/$this->session->userdata('e_assigned_tickets_count'));
							?>
							<!-- calcilations for display prograss chat ends -->

 

							<!-- Completeness stats -->
					 	<div class="card">
								<div class="card-header bg-transparent header-elements-inline">
									<span class="text-uppercase font-size-sm font-weight-semibold">Completeness stats</span>
									<div class="header-elements">
										<div class="list-icons">
					                		<a class="list-icons-item" data-action="collapse"></a>
				                		</div>
			                		</div>
								</div>

								<div class="card-body">
									<ul class="list-unstyled mb-0">
							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Emp/Emp_onprograss';?>">
							                <div class="d-flex align-items-center mb-1">Progress <span class="text-muted ml-auto"><?php echo $e_pb_Progress?>%</span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-grey-400" style="width: <?php echo $e_pb_Progress?>%">
													<span class="sr-only"><?php echo $e_pb_Progress?>% Complete</span>
												</div>
											</div></a>
							            </li>

							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Emp/Emp_resolved';?>" >
							                <div class="d-flex align-items-center mb-1">Resolved by me <span class="text-muted ml-auto">50%</span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-warning-400" style="width: 50%">
													<span class="sr-only">50% Complete</span>
												</div>
											</div></a>
							            </li>

							            <li class="mb-3">
							            	<a href="<?php echo base_url().'Emp/Emp_closed';?>" >
							                <div class="d-flex align-items-center mb-1">Closed tickets<span class="text-muted ml-auto">70%</span></div>
											<div class="progress" style="height: 0.125rem;">
												<div class="progress-bar bg-success-400" style="width: 70%">
													<span class="sr-only">70% Complete</span>
												</div>
											</div></a>
							            </li>

							            
							        </ul>
								</div>
							</div> 
							<!-- /completeness stats -->

						</div>
						<!-- /sidebar content -->

					</div>
					<!-- /right sidebar component -->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emp extends CI_Controller {

	/**
	 * Index Page for this controller.
	 * 
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome 
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name> 
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() 
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('form','url','html'));
    $this->load->helper('security');
    $this->load->database(); 
    $this->load->model('Empmodel');
    //$this->load->model('Adminmodel');	
    $this->load->model('Authmodel');	
  }
	public function Emp_home() 
	 
	{    //echo "<pre>";print_r($this->session->all_userdata());exit();
		//$data['e_view_menu']=$this->Empmodel->emp_profile_view_menu();
		$data['newtickets']=$this->Empmodel->get_emp_newtickets();
 
		$this->session->set_userdata('e_newtickets_count',count($this->Empmodel->get_emp_newtickets()));
		$this->session->set_userdata('e_shedule_tickets_count',count($this->Empmodel->get_emp_inprogress()));
		$this->session->set_userdata('e_resolved_count',count($this->Empmodel->get_emp_resolved()));
		$this->session->set_userdata('e_closed_tickets_count',count($this->Empmodel->get_emp_closed_tickets()));

		$this->session->set_userdata('e_assigned_tickets_count',count($this->Empmodel->get_e_assigned_tickets_count()));
		
/*echo "string".$this->session->userdata('e_assigned_tickets_count');
echo "string".$this->session->userdata('e_shedule_tickets_count');
exit(); */

		$this->load->view('Emp/Emp_dashboard',$data);
	}
	/*public function Emp_dashboard()
    {echo "Emp string";exit();
         
        $this->load->view('Emp/Homeview',$data); 
    }*/
	public function Emp_onprograss()
	{   
		//$data['e_view_menu']=$this->Empmodel->emp_profile_view_menu();
		$data['progress']=$this->Empmodel->get_emp_inprogress();
		$this->load->view('Emp/Emp_porgress',$data);
	}
	public function eassignsingleticket()
	{    
		$refval=$this->uri->segment(3,0); 
		//$data['e_view_menu']=$this->Empmodel->emp_profile_view_menu();
		echo "string".$refval;exit();
		$this->load->view('Emp/assignsingleticket',$data);
	}
	public function Emp_resolved()
	{  
         $data['resolved']=$this->Empmodel->get_emp_resolved();
        $this->load->view('Emp/Emp_resolved',$data);
	}
	public function Emp_closed()
	{       
		//$data['e_view_menu']=$this->Empmodel->emp_profile_view_menu();
		$data['emp_closed_tickets']=$this->Empmodel->get_emp_closed_tickets();
		//echo "<pre>";var_dump($data['emp_closed_tickets']);exit();
        $this->load->view('Emp/Emp_closed',$data);
	}
	public function Emp_History()
	{   
		//$data['e_view_menu']=$this->Empmodel->emp_profile_view_menu();
	    echo "Emp Emp_history";exit();
        $this->load->view('Emp/Emp_history',$data);
	}
	public function Emp_new_tic_info()
	{    
		$refval=$this->uri->segment(3,0); 
		$data['tic_info']=$this->Empmodel->get_tic_info($refval);
		$this->load->view('Emp/Emp_new_tick_info',$data);
	}
		public function Accept_emp_tic()
	{    
		//$refval=$this->uri->segment(3,0);
		date_default_timezone_set("Asia/Calcutta");
		$data['emp_estimate_time']=$this->input->post('est_time');
		$refval=$this->input->post('tic_id');
		$data['emp_accept_time']=date("Y-m-d H:m:s");
		$result=$this->Empmodel->emp_accept($refval,$data);
		$user_id=$this->session->userdata('user_id');
		$user_type=$this->session->userdata('user_type');
       	$data_msg=array(
                'ticket_id' => $refval,
                'user_id'=>$user_id,
                'user_role_id'=>$user_type,
                'message'=>$this->input->post('emp_msg'),
                'created'=>$data['emp_accept_time']
            );
       	$res=$this->Empmodel->emp_tick_msg($data_msg);

		$data1=array('ticket_id' => $refval,'status_id'=>'2','created'=>$data['emp_accept_time'],'updatedby'=>$user_id );
		$result2=$this->Empmodel->emp_solved_history($data1);
		if($result)
			$this->Emp_home();
		else
			$this->Emp_new_tic_info();
	}

	public function Emp_tic_solved()
	{    
		$refval=$this->uri->segment(3,0); 
		$data['tic_info']=$this->Empmodel->get_progress_info($refval);
		$this->load->view('Emp/Emp_tic_solved',$data);
	}
	public function Emp_tic_solve_update()
	{	date_default_timezone_set("Asia/Calcutta");
		$date=date("Y-m-d H:m:s");
		$emp_cmp_time=$this->input->post('cmp_time');
		$refval=$this->input->post('tic_id');
		$result1=$this->Empmodel->emp_solved($refval,$emp_cmp_time);		
		$data['emp_accept_time']=date("Y-m-d H:m:s");
		$user_id=$this->session->userdata('user_id');
		$user_type=$this->session->userdata('user_type');
       	$data_msg=array(
                'ticket_id' => $refval,
                'user_id'=>$user_id,
                'user_role_id'=>$user_type,
                'message'=>$this->input->post('emp_msg_solved'),
                'created'=>$date
            );
       	$res=$this->Empmodel->emp_tick_msg($data_msg);
       	$res=$this->Empmodel->emp_ticks_emp_accept_2($refval);

		$data=array('ticket_id' => $refval,'status_id'=>'4','created'=>$date,'updatedby'=>$user_id );
		$result2=$this->Empmodel->emp_solved_history($data);
		if($result1)
			$this->Emp_home();
		else
			$this->Emp_tic_solve_update();
	}

	public function Emp_profile()
    {    
        $data['employee_profile']=$this->Empmodel->get_Employee_profiledetails();
        
       // echo  "<pre>";var_dump($data['employee_profile']);exit();  
        $this->load->view('Emp/Employee_profileview',$data);
    }

    public function employee_profile_update()
    {      
    	$emp_id=$this->uri->segment(3,0);
        $data['employee_profile_details']=$this->Empmodel->get_employee_profile_update_detailspre($emp_id);
         $this->load->view('Emp/emp_profile_update',$data);
    }
    /* public function  employee_personal_profile_info_update()
    {
        //echo "<pre>";var_dump($_POST);        var_dump($_FILES); exit();
        $data = array(
        'emp_name' => $this->input->post('emp_name'),
        'emp_mail' => $this->input->post('emp_email'),
        'emp_gender' => $this->input->post('gender'),
        'phone_no' => $this->input->post('contact_phone'),
        'dob' => $this->input->post('d_o_b'),
        'doj' => $this->input->post('doj'),
        'address' => $this->input->post('addr')
        );
        $this->Empmodel->employeeprofileinfo_update_details($data,$this->input->post('emp_uid')); 
        redirect('Emp/Emp_profile','refresh');
    }*/
    public function  employee_personal_profile_info_update()
    {
        //echo "<pre>";var_dump($_POST);        var_dump($_FILES); exit();
         $emp_id=$this->input->post('emp_img'); 
        $path = $_FILES['update_img']['name'];
        if(!empty($path)){
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $emp_imge = $emp_id.'.'.$ext;
        $output_dir1 = "assets/images/emp/";
        move_uploaded_file($_FILES["update_img"]["tmp_name"],$output_dir1.$emp_imge);
        }
        else{
            $path= $this->input->post('update_img');
           
        }
        $data = array(
        'emp_name' => $this->input->post('emp_name'),
         'emp_pic' =>$emp_imge,
        'emp_mail' => $this->input->post('emp_email'),
        'emp_gender' => $this->input->post('gender'),
        'phone_no' => $this->input->post('contact_phone'),
        'dob' => $this->input->post('d_o_b'),
        'doj' => $this->input->post('doj'),
        'address' => $this->input->post('addr')
        );
        $this->Empmodel->employeeprofileinfo_update_details($data,$this->input->post('emp_uid')); 
        redirect('Emp/Emp_profile','refresh');
    }

    public function Aview_projects()
    {
        $data['emp_wrok_info']=$this->Empmodel->get_work_clint_proj();
        $this->load->view('Emp/Emp_work_on_clint',$data);
    }
    public function eresolvedsingleticket()
	{    
		$refval=$this->uri->segment(3,0); 
		
		$data['tic_info']=$this->Empmodel->get_s_tic_solved_info($refval);
		 
		$this->load->view('Emp/Emp_solved_singleticket',$data);
	}
	public function eclosedsingleticket()
	{    
		$refval=$this->uri->segment(3,0);

		$data['tic_info']=$this->Empmodel->get_s_tic_closed_info($refval);
		$this->load->view('Emp/Emp_closed_singleticket',$data);
	}



	 
}

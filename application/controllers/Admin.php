<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()    
     {
        parent::__construct(); 
        $this->load->library('session');
        $this->load->helper(array('form','url','html'));
        $this->load->helper('security');
        $this->load->database(); 
        $this->load->model('Adminmodel');
        $this->load->model('Empmodel');
        $this->load->model('Authmodel');
     }
	public function Admin_home()
	{	
		$data['priority_list']=$this->Adminmodel->get_priority_list();
		$data['emp']=$this->Adminmodel->get_emp();
		$data['newtickets']=$this->Adminmodel->get_newtickets();
		$data['newtickets']=$this->Adminmodel->get_newtickets();      
        $this->session->set_userdata('a_newtickets_count',count($this->Adminmodel->get_newtickets()));
        $this->session->set_userdata('a_shedule_tickets_count',count($this->Adminmodel->get_shedule_tickets()));
        $this->session->set_userdata('a_onhold_count',count($this->Adminmodel->get_onhold_tickets()));
        $this->session->set_userdata('a_resolved_count',count($this->Adminmodel->get_resolved_tickets()));
        $this->session->set_userdata('a_closed_tickets_count',count($this->Adminmodel->get_closed_tickets())); 
        $this->session->set_userdata('a_emps_count',$this->Adminmodel->get_a_emps_count()); 
        $this->session->set_userdata('a_clients_count',$this->Adminmodel->get_a_clients_count()); 
        $this->session->set_userdata('a_projectcs_count',$this->Adminmodel->get_a_projectcs_count()); 
        $this->session->set_userdata('a_userrole_count',$this->Adminmodel->get_a_userrole_count()); 
        $this->session->set_userdata('a_priority_count',$this->Adminmodel->get_a_priority_count()); 
        $this->session->set_userdata('a_compbank_info_count',$this->Adminmodel->get_a_compbank_info_count()); 
        $this->session->set_userdata('a_total_tickets_count',$this->Adminmodel->get_a_total_tickets_count());
		$this->load->view('Admin/Admin_dashboard',$data);
	}
    public function scheduled()
	{	
        $data['shedule_tickets']=$this->Adminmodel->get_shedule_tickets();
        $this->load->view('Admin/Admin_scheduled',$data);
	}
	public function assignsingleticket() 	 
	{	
		$refval=$this->uri->segment(3,0); 
        $data['priority_list']=$this->Adminmodel->get_priority_list();
        $data['assignsingleticketdet']=$this->Adminmodel->get_assignsingleticketdet($refval);
        $data['emp']=$this->Adminmodel->get_techemp();
        $this->load->view('Admin/Admin_assign_ticket',$data);
	}
    public function onhold()
	{	
        $data['onhold']=$this->Adminmodel->get_onhold_tickets();
		$data['emp']=$this->Adminmodel->get_emp();
		$this->load->view('Admin/Admin_onhold',$data);
	}
    public function resolved()
	{	
		$data['resolved']=$this->Adminmodel->get_resolved_tickets();
		$this->load->view('Admin/Admin_resolved',$data);
	}
    public function closed()
	{	
		$data['closed_tickets']=$this->Adminmodel->get_closed_tickets();
		$this->load->view('Admin/Admin_closed',$data);
	}
    public function history()
	{	
		$data['emp']=$this->Adminmodel->get_emp();
		$this->load->view('Admin/Admin_history',$data);
	}
    public function assigningtoemp()     
    {   
        date_default_timezone_set("Asia/Calcutta");
        $date=date("Y-m-d h:i:s");
        $emp_id = $this->input->post('emp_id');
            if($emp_id == 1){
                $tick_ststus=1;
            }
            else{
                $tick_ststus=2;
            }
       $data = array(
      'ticket_id' => $this->input->post('tic_id'),
                'emp_id' => $emp_id,
                'emp_roll_id' => 1,
                'assigned_by' => $this->session->userdata('user_id'),
                'admin_priority' => $this->input->post('admin_priority'),
                'admin_assigned_toemp' => $date,
                'admin_time' => $this->input->post('est_time'),
                );
       $this->Adminmodel->admin_tic_assigned_flag_change($this->input->post('tic_id'),$tick_ststus);
       $this->Adminmodel->admin_tic_assigned_emp($data); 
       $user_id=$this->session->userdata('user_id');
       $user_type=$this->session->userdata('user_type');
       $data_msg=array(
                'ticket_id' => $this->input->post('tic_id'),
                'user_id'=>$user_id,
                'user_role_id'=>$user_type,
                'message'=>$this->input->post('admin_msg'),
                'created'=>$date
                 );
       $res=$this->Adminmodel->ins_tick_msg($data_msg);
       $data1=array('ticket_id' => $this->input->post('tic_id'),'status_id'=>$tick_ststus,'created'=>$date,'updatedby'=>$user_id );
       $result2=$this->Empmodel->emp_solved_history($data1);
        redirect('Admin/Admin_home', 'refresh');
    }
	public function create_roles()
	{	
	   $this->load->view('Admin/Admin_create_roles');
	}
    public function inst_role()
	{	
        $icon = $this->input->post('role_pic');  
        $path = $_FILES['role_pic']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $num=rand(1000,9999);
        $role_img = $num.'.'.$ext;
		$output_dir1 = "assets/images/roles/";
		move_uploaded_file($_FILES["role_pic"]["tmp_name"],$output_dir1.$role_img);
        $data=array(
             'user_name'=>$this->input->post('role_name'),
             'user_logo'=>$role_img,
             'created'=>date('Y-m-d')
             );
        $result=$this->Adminmodel->inst_role_tbl($data);
        if($result)
            $this->Admin_home();
        else
            $this->create_roles();
	}
    public function create_client()
	{	
		$this->load->view('Admin/Admin_create_client');
	}
    public function inst_client()
	{	
        $compy_name=$this->input->post('comp_name');
        $clint_id=date("ym").substr($compy_name,0,4).rand(100,999);
        $path = $_FILES['comp_logo']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $compy_img = $clint_id.'.'.$ext;
		$output_dir1 = "assets/images/client/logo/";
		move_uploaded_file($_FILES["comp_logo"]["tmp_name"],$output_dir1.$compy_img);
        $output_dir2 = "assets/images/client/comp_head_img/";
		move_uploaded_file($_FILES["comp_h_pic"]["tmp_name"],$output_dir2.$compy_img);
        $data=array(
             'clint_uid'=>$clint_id,
             'clint_company_name'=>$compy_name,
             'clint_company_logo'=>$compy_img,
             'clint_company_adres'=>$this->input->post('comp_addr'),
             'clint_company_head_name'=>$this->input->post('comp_h_name'),
             'clint_company_head_pic'=>$compy_img,
             'clint_company_head_mail'=>$this->input->post('comp_h_email'),
             'clint_company_head_ph'=>$this->input->post('comp_h_phone'),
             'clint_company_contact_persion'=>$this->input->post('comp_cont_pers_name'),
             'clint_company_contact_persion_mail'=>$this->input->post('compy_cont_per_email'),
             'clint_company_contact_persion_phone'=>$this->input->post('compy_cont_person_phone'),
             'client_status'=>1,
             'created'=>date('Y-m-d')
             );
        $result=$this->Adminmodel->inst_clint_tbl($data);
        $data2=array(
             'user_id'=>$clint_id,
             'user_pwd'=>$clint_id, 
             'user_type'=>7 
             );
        $result2=$this->Adminmodel->inst_userlogin_for_emp($data2);
        if($result)
            $this->Admin_home();
        else
            $this->create_client();   
	}
    public function create_project()
	{	
        $data['clint_list']=$this->Adminmodel->get_clint_name();
		$this->load->view('Admin/Admin_create_project',$data);
	}
    public function inst_project()
	{	
        $project_id="p".$this->input->post('clint_id');
        $data_proj_details=array(
             'project_id'=>$project_id,
             'clint_id'=>$this->input->post('clint_id'),
             'clint_loc_adres'=>$this->input->post('lh_addr'),
             'clint_loc_head'=>$this->input->post('lh_name'),
             'clint_loc_head_mail'=>$this->input->post('lh_email'),
             'clint_loc_head_ph'=>$this->input->post('lh_phone'),
             'created'=>date('Y-m-d')
             );
        $data_url_info=array(
            'project_id'=>$project_id,
            'clint_id'=>$this->input->post('clint_id'),
            'c_url_link'=>$this->input->post('proj_url'),
            'url_config_file_uid'=>$this->input->post('file_id'),
            'url_config_file_pwd'=>$this->input->post('file_pass'),
            'url_config_db_uid'=>$this->input->post('db_name'),
            'url_config_db_pwd'=>$this->input->post('db_pass'),
            'created'=>date('Y-m-d')
            );
        $result=$this->Adminmodel->inst_project_tbl($data_url_info,$data_proj_details);
        if($result)
            $this->Admin_home();
        else
            $this->create_project();
	}
    public function create_employee()
	{	
        $data['roles_list']=$this->Adminmodel->get_roles_for_create_emp();
        $this->load->view('Admin/Admin_create_emp',$data);
	}
    public function inst_emp()
	{	
        $icon = $this->input->post('emp_pic');  
        $path = $_FILES['emp_pic']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $emp_name=$this->input->post('emp_name');
        $emp_id=date("ym").$this->input->post('emp_role').substr($emp_name,0,3).rand(100,999);
        $emp_img = $emp_id.'.'.$ext;
		$output_dir1 = "assets/images/emp/";
		move_uploaded_file($_FILES["emp_pic"]["tmp_name"],$output_dir1.$emp_img);
        $data=array(
             'emp_id'=>$emp_id,
             'emp_name'=>$emp_name,
             'emp_pic'=>$emp_img,
             'emp_mail'=>$this->input->post('emp_email'),
             'emp_gender'=>$this->input->post('emp_gender'),
             'user_role_id'=>$this->input->post('emp_role'),
             'phone_no'=>$this->input->post('emp_phone'),
             'doj'=>date('Y-m-d'),
             'address'=>$this->input->post('emp_addr'),
             'created'=>date('Y-m-d')
             );
        $result=$this->Adminmodel->inst_emp_tbl($data);
        $data2=array(
             'user_id'=>$emp_id,
             'user_pwd'=>$emp_id, 
             'user_type'=>$this->input->post('emp_role') 
             );
        $result2=$this->Adminmodel->inst_userlogin_for_emp($data2);
        if($result)
            $this->Admin_home();
        else
            $this->create_employee();
	}
    public function Aview_roles()
    {   
        $data['roledetails']=$this->Adminmodel->get_view_roledetails();
        $this->load->view('Admin/a_view_roledetails',$data);
    }
    public function Aview_prority()
    {   
        $data['propritylist']=$this->Adminmodel->get_view_proritylist();
        $this->load->view('Admin/a_view_proritylist',$data);
    }
    public function Aview_clients()
    {   
        $data['Clientdetails']=$this->Adminmodel->get_Clientdetails();
        $this->load->view('Admin/a_view_clientdetails',$data);
    }
    public function Aview_employee()
    {   
        $data['employeedetails']=$this->Adminmodel->get_view_employeedetails();
        $this->load->view('Admin/a_view_employeedetails',$data);
    }
    public function Aview_projects()
    {    
        $data['client_proj_det']=$this->Adminmodel->get_client_proj_view_for_a_view();
        $this->load->view('Admin/a_view_client_proj_view',$data);
    }
    public function Admin_bankprofile()
    {      
        $data['company_bank']=$this->Adminmodel->get_topay_company_bank_update_detailspre();
        $this->load->view('Admin/company_bankinfo',$data);
    }
    public function topay_company_bank_info_update()
    {      
        $data['topay_company_bank']=$this->Adminmodel->get_topay_company_bank_update_detailspre();
        $this->load->view('Admin/topay_company_bank_infoupdation',$data);
    }

    public function  topay_companybankinfo_update()
    {
        $data = array(
        'name' => $this->input->post('name'),
        'url' => $this->input->post('url'),
        'mail_id' => $this->input->post('email'),
        'phone_no' => $this->input->post('contact'),
        'adsress' => $this->input->post('addr'),
        'ifcs_code' => $this->input->post('ifsc'),
        'account_no' => $this->input->post('account'),
        'bank_name' => $this->input->post('bank'),
        'branch' => $this->input->post('branch')
        );
        $this->Adminmodel->companybankinfo_update_details($data); 
        redirect('Admin/Admin_bankprofile','refresh');
    }
    public function A_clientinfoview()     
    {   
        $refvalue=$this->uri->segment(3,0); 
        $data['A_clientinfo_view']=$this->Adminmodel->get_clientinfoview_updatepre($refvalue);
        $this->load->view('Admin/A_clientinfoview',$data);
    }
    /*public function A_clientinfoview_update1()
    {
        $data = array(
        'clint_company_name'=> $this->input->post('comp_name'),
        'clint_company_head_name' => $this->input->post('Client_company_head_name'),
        'clint_company_head_pic' => $this->input->post('clint_comp_head_pic'),
        'clint_company_head_mail' => $this->input->post('clint_comp_head_mail'),
        'clint_company_head_ph' => $this->input->post('clint_comp_head_ph'),
        'clint_company_contact_persion' => $this->input->post('clint_company_contact_persion'),
        'clint_company_contact_persion_mail' => $this->input->post('clint_company_contact_persion_mail'),
        'clint_company_contact_persion_phone' => $this->input->post('clint_company_contact_persion_phone'),
        'client_status' => $this->input->post('client_status')
        );
        $this->Adminmodel->A_clientinfoview_update_details($data,$this->input->post('clint_uid')); 
        redirect('Admin/Aview_clients','refresh');
    }*/
    public function A_clientinfoview_update1()
    {
        /*echo "<pre>";var_dump($_POST); 
        var_dump($_FILES); exit();*/
       // $this->input->post('update_logo');
        $clint_id=$this->input->post('clint_uid'); 
        $path = $_FILES['update_logo']['name'];
        if(!empty($path)){
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $clint_img = $clint_id.'.'.$ext;
        $output_dir1 = "assets/images/client/logo/";
        move_uploaded_file($_FILES["update_logo"]["tmp_name"],$output_dir1.$clint_img);
        }
        else{
            echo $this->input->post('comp_logo');
            $path= "logo.png";
        }

        
        $data = array(
        'clint_company_name'=> $this->input->post('comp_name'),
        
        'clint_company_head_name' => $this->input->post('Client_company_head_name'),
         'clint_company_logo' =>$clint_img,

        'clint_company_head_pic' => $this->input->post('clint_comp_head_pic'),
        'clint_company_head_mail' => $this->input->post('clint_comp_head_mail'),
        
        
        'clint_company_head_ph' => $this->input->post('clint_comp_head_ph'),
        'clint_company_contact_persion' => $this->input->post('clint_company_contact_persion'),
        'clint_company_contact_persion_mail' => $this->input->post('clint_company_contact_persion_mail'),
        'clint_company_contact_persion_phone' => $this->input->post('clint_company_contact_persion_phone'),
        'client_status' => $this->input->post('client_status')
        );
        /*echo "<pre>";var_dump($_POST); 
        var_dump($_FILES); exit();*/
        $this->Adminmodel->A_clientinfoview_update_details($data,$clint_id); 
        
        redirect('Admin/Aview_clients','refresh');
    }

    public function A_empinfoview()     
    {   
        $refval=$this->uri->segment(3,0);  
         $data['A_empinfo_view']=$this->Adminmodel->get_A_empinfoview_updatepre($refval);
        $this->load->view('Admin/A_empinfoview',$data);
    }

    /*public function A_empinfoview_update1()
    {
        $data = array(
        'emp_name' => $this->input->post('name'),
        'emp_mail' => $this->input->post('email'),
        'doj' => $this->input->post('doj'),
        
        'user_role_id' => $this->input->post('user_role_id'),
        'phone_no' => $this->input->post('contact'),
        'address' => $this->input->post('address')
        );
        $this->Adminmodel->A_empinfoview_update_details($data,$this->input->post('emp_id')); 
        redirect('Admin/Aview_employee','refresh');
    }*/
     public function A_empinfoview_update1()
    {
        /*echo "<pre>";var_dump($_POST); 
        var_dump($_FILES); exit();*/
         $emp_id=$this->input->post('emp_img'); 
        $path = $_FILES['update_logo']['name'];
        if(!empty($path)){
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $clint_img = $emp_id.'.'.$ext;
        $output_dir1 = "assets/images/emp/";
        move_uploaded_file($_FILES["update_logo"]["tmp_name"],$output_dir1.$clint_img);
        }
        else{
             $path= $this->input->post('update_logo');
           
        }
        $data = array(
        'emp_name' => $this->input->post('name'),
        'emp_pic' =>$clint_img,
        'emp_mail' => $this->input->post('email'),
        'doj' => $this->input->post('doj'),
        
        'user_role_id' => $this->input->post('user_role_id'),
        'phone_no' => $this->input->post('contact'),
        'address' => $this->input->post('address')
        );
        /*echo "<pre>";var_dump($_POST); 
        var_dump($_FILES); exit();*/
        $this->Adminmodel->A_empinfoview_update_details($data,$this->input->post('emp_id')); 
        redirect('Admin/Aview_employee','refresh');
    }

    public function A_view_projinfo()     
    {   
        $refval=$this->uri->segment(3,0); 
        $data['A_view_proj']=$this->Adminmodel->get_A_view_projinfo_details($refval);
        $data['A_view_proj_cpinfo']=$this->Adminmodel->get_A_view_projcpinfo_details($refval);
        $this->load->view('Admin/A_projinfoview',$data);
    }
    public function A_view_projinfo_update()
    {
        $data = array(
        'clint_loc_adres' => $this->input->post('address'),
        'clint_loc_head' => $this->input->post('client_head'),
        'clint_loc_head_mail' => $this->input->post('client_loc_head_mail'),   
        'clint_loc_head_ph' => $this->input->post('contact'),
        );
        $this->Adminmodel->A_view_projinfo_update_details($data,$this->input->post('project_id')); 
        redirect('Admin/Aview_projects','refresh');
    }
    public function A_view_projcpinfo_update()
    {
        $data = array(
        'c_url_link' => $this->input->post('comp_url_link'),   
        'url_config_file_uid' => $this->input->post('url_config_file_uid'),
        'url_config_file_pwd' => $this->input->post('url_config_file_pwd'),
        'url_config_db_uid' => $this->input->post('url_config_db_uid'),
        'url_config_db_pwd' => $this->input->post('url_config_db_pwd'),
        );
        $this->Adminmodel->A_view_projcpinfo_update_details($data,$this->input->post('project_id')); 
        redirect('Admin/Aview_projects','refresh');
    }

    public function Admin_profile()
    {    
        $data['employee_profile']=$this->Adminmodel->get_Admin_profiledetails();  
        $this->load->view('Admin/admin_profile_view',$data);
    }

     public function admin_profile_update()
    {      
        $emp_id=$this->uri->segment(3,0);
        $data['employee_profile_details']=$this->Adminmodel->get_admin_profile_update_detailspre($emp_id);
         $this->load->view('Admin/admin_profile_update',$data);
    }
     public function  admin_personal_profile_info_update()
    {
        $data = array(
        'emp_name' => $this->input->post('emp_name'),
        'emp_mail' => $this->input->post('emp_email'),
        'emp_gender' => $this->input->post('gender'),
        'phone_no' => $this->input->post('contact_phone'),
        'dob' => $this->input->post('d_o_b'),
        'doj' => $this->input->post('doj'),
        'address' => $this->input->post('addr')
        );
        $this->Adminmodel->adminprofileinfo_update_details($data,$this->input->post('emp_uid')); 
        redirect('Admin/Admin_profile','refresh');
    }
    function onprog_assignsingleticket()
    {
        $refval=$this->uri->segment(3,0); 
        $data['priority_list']=$this->Adminmodel->get_priority_list();
        $data['assignsingleticketdet']=$this->Adminmodel->get_onprog_single_ticket($refval);
        $data['emp']=$this->Adminmodel->get_techemp();
        $this->load->view('Admin/Admin_onprg_ticket_info',$data);
    }
    function resolved_assignsingleticket()
    {
        $refval=$this->uri->segment(3,0); 
        $data['priority_list']=$this->Adminmodel->get_priority_list();
        $data['assignsingleticketdet']=$this->Adminmodel->get_resolved_single_ticket($refval);
        $data['emp']=$this->Adminmodel->get_techemp();
        $this->load->view('Admin/Admin_resolved_ticket_info',$data);
    }
    function closed_assignsingleticket()
    {
        $refval=$this->uri->segment(3,0); 
        $data['priority_list']=$this->Adminmodel->get_priority_list();
        $data['assignsingleticketdet']=$this->Adminmodel->get_closed_single_ticket($refval);
        $data['emp']=$this->Adminmodel->get_techemp();
        $this->load->view('Admin/Admin_closed_ticket_info',$data);
    }
    function onhold_assignsingleticket()
    {
        $refval=$this->uri->segment(3,0); 
        $data['priority_list']=$this->Adminmodel->get_priority_list();
        $data['assignsingleticketdet']=$this->Adminmodel->get_onhold_single_ticket($refval);
        $data['emp']=$this->Adminmodel->get_techemp();
        $this->load->view('Admin/Admin_onhold_ticket_info',$data);
    }
    function onhold_to_emp()
    {
        date_default_timezone_set("Asia/Calcutta");
        $date=date("Y-m-d h:i:s");
        $ticket_id=$this->input->post('tic_id');
        $data = array(
                'emp_id' => $this->input->post('emp_id'),
                'emp_roll_id' => 1,
                'assigned_by' => $this->session->userdata('user_id'),
                'admin_priority' => $this->input->post('admin_priority'),
                'admin_assigned_toemp' => $date,
                'admin_time' => $this->input->post('est_time'),
                );
        $result=$this->Adminmodel->onhold_tic_update($data,$ticket_id);
        $this->Adminmodel->onhold_tick_ststus_update($ticket_id);
        $data_history=array(
                      'ticket_id' =>$ticket_id,
                      'status_id' =>'2',
                      'updatedby' =>$this->input->post('emp_id'),
                      'created'   =>$date
                       );
        $this->Adminmodel->onhold_tick_history_update($data_history);
        $user_id=$this->session->userdata('user_id');
        $user_type=$this->session->userdata('user_type');
        $data_msg=array(
                'ticket_id' => $ticket_id,
                'user_id'=>$user_id,
                'user_role_id'=>$user_type,
                'message'=>$this->input->post('admin_msg'),
                'created'=>$date
            );
       $res=$this->Adminmodel->ins_tick_msg($data_msg);
       if($result)
            redirect('Admin/Admin_home','refresh');
        else
            redirect('Admin/onhold','refresh');
    }	
    public function admin_emp_prfile_details_view_deactivate()
    {
        $refval=$this->uri->segment(3,0);  
         $this->Adminmodel->a_emp_deactivate_employee_profile($refval); 
        $this->Adminmodel->a_emp_deactivate_userlogin($refval);
        redirect('Admin/Aview_employee','refresh');    
    }
    public function admin_proj_profile_deactivate()
    {
        $refval=$this->uri->segment(3,0); 
        $this->Adminmodel->a_proj_deactivate_proj_url($refval); 
        $this->Adminmodel->a_proj_deactivate_clients_project_details($refval);
        redirect('Admin/Aview_projects','refresh');  
    }
    public function admin_client_profile_deactivate()
    {
       $refval=$this->uri->segment(3,0); 
       $this->Adminmodel->a_client_deactivate_clients_profile($refval); 
       $this->Adminmodel->a_client_deactivate_userlogin($refval);
       redirect('Admin/Aview_clients','refresh');    
    }


 
}

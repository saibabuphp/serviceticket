<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {
	function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('form','url','html'));
    $this->load->helper('security');
    $this->load->database(); 
    $this->load->model('Homemodel');
       

  }
	public function index()
	{
		$this->load->view('Home/Homeview');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or - 
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html 
	 */
	function __construct() 
    {
    parent::__construct();
        $this->load->library('session');
        $this->load->helper(array('form','url','html'));
        $this->load->helper('security');
        $this->load->database(); 
        $this->load->model('Clientmodel');
        $this->load->model('Authmodel');
        $this->load->model('Empmodel');
    }
    public function Client_dashboard()
    {
    	$data['week_info']=$this->Clientmodel->this_week();
        $data['last_day_info']=$this->Clientmodel->last_day();

        $data['active']=$this->Clientmodel->active_tick();
        $data['resolved']=$this->Clientmodel->resolved_tick();
        $data['closed']=$this->Clientmodel->closed_tick();
        $data['act_count']=$this->Clientmodel->act_count_tick();
        $data['last_day_count']=$this->Clientmodel->last_day_tick_count();
        $data['last_week_count']=$this->Clientmodel->last_week_tick_count();
        $data['resolved_count']=$this->Clientmodel->resolved_count_tick();
        $data['closed_count']=$this->Clientmodel->closed_count_tick(); 
		$this->session->set_userdata('c_newtickets_count',count($this->Clientmodel->get_clnt_new_ticket()));
		$this->session->set_userdata('c_shedule_tickets_count',count($this->Clientmodel->get_clnt_scheduled_ticket()));
		$this->session->set_userdata('c_onhold_count',count($this->Clientmodel->get_clnt_onhold_ticket()));
		$this->session->set_userdata('c_resolved_count',count($this->Clientmodel->get_clnt_resolved_ticket()));
		$this->session->set_userdata('c_closed_tickets_count',count($this->Clientmodel->get_clnt_closed_ticket())); 
		$this->session->set_userdata('c_assigned_tickets_count',count($this->Clientmodel->get_c_assigned_tickets_count()));
		
/*echo "string".$this->session->userdata('c_assigned_tickets_count');
echo "string".$this->session->userdata('c_shedule_tickets_count');
exit(); */
		

        $this->load->view('Client/Homeview',$data);  
    }
	public function Client_home()
	{
       $data['porj_id']=$this->Clientmodel->get_project_id();
       $data['priority']=$this->Clientmodel->get_priority_list();
       $data['request']=$this->Clientmodel->get_request_list();
		//var_dump($data);
        $this->load->view('Client/Client_ticket',$data); 
	}
    public function Rise_ticket()//with out ajax
	{
		//echo "<pre>"; print_r($_FILES);  echo "fsdfsdafasdf"; print_r($_POST);exit;
		 $this->load->helper(array('form', 'url'));
		  
        date_default_timezone_set('Asia/Calcutta');
		$date=date('Y-m-d');  
	    $currentmonth = date('m');
	    $currentdate  = date('d'); 
	    $currentyear  = date('y'); 
 		$key2='';
	     for($i=0;$i<2;$i++)
	     {
	        $key2.=rand(0,9);
	     }
	      $key='';
	     for($i=0;$i<1;$i++)
	     {
	         $key.=rand(0,9);
	     } 
		$ticket_id= $currentyear.$currentmonth.$currentdate.'tick'.$key2.$key;
		$this->load->helper(array('form', 'url'));
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('priority', 'priority', 'trim|required|xss_clean');
        $this->form_validation->set_rules('req_type', 'req_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('subject', 'subject', 'trim|required|xss_clean');
        $this->form_validation->set_rules('desc', 'desc', 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE)
		{	 
			$this->load->view('Client/Client_ticket');
		}
        else{

        	 $icon = $this->input->post('phot1');  
        	 $path = $_FILES['phot1']['name'];
		     $ext = pathinfo($path, PATHINFO_EXTENSION); 
		     $tickname = $ticket_id.'.'.$ext;
		     $output_dir1 = "assets/images/ticket/";
		     move_uploaded_file($_FILES["phot1"]["tmp_name"],$output_dir1.$tickname);

		     $data=array(
                    'ticket_id'=>$ticket_id,
                    'project_id'=>$this->input->post('proj_name'),
                    'request_type'=>$this->input->post('req_type'),
                    'url_link'=>$this->input->post('path'),
                    'ticket_sub'=>$this->input->post('subject'),
                    'ticket_discription'=>$this->input->post('desc'),
                    'ticket_pics'=>$tickname,
                    'ticket_priority'=>$this->input->post('priority'),
                    'created'=>date('Y-m-d')
                  ); 
            $insert_res = $this->Clientmodel->ticket_insert($data);	
            //$data['raisedticket'] = " <spam style='background-color:#E1E1E1;'><b>Successfully raised ticket</b></spam>";
            $messge = array('message' => 'Successfully raised ticket on '.$this->input->post('subject'),'class' => 'alert alert-success');
			$this->session->set_flashdata('item', $messge);
			$this->session->keep_flashdata('item',$messge);
            redirect('Client/Client_dashboard', 'refresh');
            //$this->load->view('Client/Client_dashboard',$data);
            //echo $raisedticket;  
             
            
        }
	}


  /*public function Rise_ticket()//with ajax
	{
		// echo "<pre>"; print_r($_FILES);  echo "fsdfsdafasdf"; print_r($_POST);exit;
		 $this->load->helper(array('form', 'url'));
		  
        date_default_timezone_set('Asia/Calcutta');
		$date=date('Y-m-d');  
	    $currentmonth = date('m');
	    $currentdate  = date('d'); 
	    $currentyear  = date('y'); 
 		$key2='';
	     for($i=0;$i<2;$i++)
	     {
	        $key2.=rand(0,9);
	     }
	      $key='';
	     for($i=0;$i<1;$i++)
	     {
	         $key.=rand(0,9);
	     } 
		$ticket_id= $currentyear.$currentmonth.$currentdate.'tick'.$key2.$key;
		$this->load->helper(array('form', 'url'));
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('priority', 'priority', 'trim|required|xss_clean');
        $this->form_validation->set_rules('req_type', 'req_type', 'trim|required|xss_clean');
        $this->form_validation->set_rules('subject', 'subject', 'trim|required|xss_clean');
        $this->form_validation->set_rules('desc', 'desc', 'trim|required|xss_clean');
        
        if ($this->form_validation->run() == FALSE)
		{	 
			$this->load->view('Client/Client_ticket');
		}
        else{

        	 $icon = $this->input->post('phot1');  
        	 $path = $_FILES['phot1']['name'];
		     $ext = pathinfo($path, PATHINFO_EXTENSION); 
		     $tickname = $ticket_id.'.'.$ext;
		     $output_dir1 = "assets/images/ticket/";
		     move_uploaded_file($_FILES["phot1"]["tmp_name"],$output_dir1.$tickname);

		     $data=array(
                    'ticket_id'=>$ticket_id,
                    'project_id'=>$this->input->post('proj_name'),
                    'request_type'=>$this->input->post('req_type'),
                    'url_link'=>$this->input->post('path'),
                    'ticket_sub'=>$this->input->post('subject'),
                    'ticket_discription'=>$this->input->post('desc'),
                    'ticket_pics'=>$tickname,
                    'ticket_priority'=>$this->input->post('priority'),
                    'created'=>date('Y-m-d')
                  ); 
            $insert_res = $this->Clientmodel->ticket_insert($data);	
            $raisedticket = " <spam style='background-color:#E1E1E1;'><b>Successfully raised ticket</b></spam>";
            echo $raisedticket;  
             
            
        }
	}*/

	
	 public function Client_newtickets()
	{    
		//$data['Client_new_ticket']=$this->Clientmodel->get_clnt_new_ticket();




		$this->load->library('pagination');
        $config = [
            'base_url' => base_url('Client/Client_newtickets'),
            'per_page' => 10,
            'total_rows' => $this->Clientmodel->get_clnt_new_ticket_num_rows(),
        ];

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tagl_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tagl_close'] = '</li>';
        $config['first_tag_open'] = '<li class="page-item disabled">';
        $config['first_tagl_close'] = '</li>';
        $config['last_tag_open'] = '<li class="page-item">';
        $config['last_tagl_close'] = '</a></li>';
        $config['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config); // model function
        $data['Client_new_ticket'] = $this->Clientmodel->get_clnt_new_ticket_pn($config['per_page'], $this->uri->segment(3));












        //echo "<pre>";var_dump($data['Client_new_ticket']);exit();
		$this->load->view('Client/Client_new_ticket',$data);
	}
	public function Client_onprograss()
	{    
		$data['Client_scheduled_ticket']=$this->Clientmodel->get_clnt_scheduled_ticket(); 
		//echo "<pre>";var_dump($data['Client_scheduled_ticket']);exit();
		$this->load->view('Client/Client_scheduled_ticket',$data);
	}
	public function Client_onhold()
	{    
		$data['Client_onhold_ticket']=$this->Clientmodel->get_clnt_onhold_ticket();
		$this->load->view('Client/Client_onhold_ticket',$data);
	}
	public function Client_resolved()
	{        
		$data['Client_resolved_ticket']=$this->Clientmodel->get_clnt_resolved_ticket();
		//echo "<pre>"; var_dump($data['Client_resolved_ticket']);exit();
        $this->load->view('Client/Client_resolved',$data);
	}
	public function Client_closed()
	{    
		$data['Client_closed_ticket']=$this->Clientmodel->get_clnt_closed_ticket();
		//echo "<pre>"; var_dump($data['Client_closed_ticket']);exit();  
        $this->load->view('Client/Client_closed',$data);
	}
	public function Client_history()
	{     
	echo "Client_history";exit();   
        $this->load->view('Client/Client_history',$data);
	}
	public function Client_profile()
	{    
	  	$data['Client_companydetails']=$this->Clientmodel->get_Client_companydetails();
	  	$data['Client_profil_det']=$this->Clientmodel->get_Client_profileview();
		//echo "<pre>";var_dump($data['Client_companydetails']); var_dump($data['Client_profil_det']);exit();  
        $this->load->view('Client/Client_profileview',$data);
	}
	public function Bank_details()
	{      
			$data['webgrid_details']=$this->Clientmodel->get_webgrid_details();
		//echo "<pre>";var_dump($data['webgrid_details']);  
        $this->load->view('Client/Bankdetails',$data);
	}
	public function Client_FAQS()
	{      
			//echo "Client_FAQS";exit();  
        $this->load->view('Client/Client_FAQS');
	}
	    public function c_rise_ticket_status()
	{      
		$data['view_form']=0;
        $tic_id=$this->uri->segment(3,0);
        $data['tick_status']=$this->Clientmodel->clnt_rise_tic_menu($tic_id);
        $this->load->view('Client/Clint_tic_status_view',$data);
	}
    public function approved_ticket_status()
	{      
		$data['view_form']=0;
        $tic_id=$this->uri->segment(3,0);
        $data['tick_status']=$this->Clientmodel->approved_tic_info($tic_id);
        $this->load->view('Client/Clint_tic_status_view',$data);
	}
    public function onhold_ticket_status()
	{      
		$data['view_form']=0;
        $tic_id=$this->uri->segment(3,0);
        $data['tick_status']=$this->Clientmodel->onhold_tic_info($tic_id);
        $this->load->view('Client/Clint_tic_status_view',$data);
	}
    public function resolved_ticket_status()
	{      
		$data['view_form']=1;
        $tic_id=$this->uri->segment(3,0);
        $data['tick_status']=$this->Clientmodel->resolved_tic_info($tic_id);
        $this->load->view('Client/Clint_tic_status_view',$data);
	}
    public function closed_ticket_status()
	{      
		$data['view_form']=0;
        $tic_id=$this->uri->segment(3,0);
        $data['tick_status']=$this->Clientmodel->closed_tic_info($tic_id);
        $this->load->view('Client/Clint_tic_status_view',$data);
	}
	public function client_profile_update()
    {      
    	$cli_id=$this->uri->segment(3,0);
        $data['client_profile_details']=$this->Clientmodel->get_client_profile_update_detailspre($cli_id);
         $this->load->view('Client/client_profile_update',$data);
    }

	     public function  client_company_profile_info_update()
    {
        //echo "<pre>";var_dump($_POST);        var_dump($_FILES); exit();
        $data = array(
        'clint_company_name' => $this->input->post('name'),
        'clint_company_adres' => $this->input->post('addr'),
        'clint_company_head_name' => $this->input->post('head_name'),
        'clint_company_head_mail' => $this->input->post('head_email'),
        'clint_company_head_ph' => $this->input->post('head_phone'),
        'clint_company_contact_persion' => $this->input->post('contact_name'),
        'clint_company_contact_persion_mail' => $this->input->post('contact_email'),
        'clint_company_contact_persion_phone' => $this->input->post('contact_phone'),
        'client_status' => $this->input->post('client_sts')
        );
        $this->Clientmodel->companyprofileinfo_update_details($data,$this->input->post('clint_uid')); 
        redirect('Client/Client_profile','refresh');
    }
    public function clint_close_tic()
	{	date_default_timezone_set("Asia/Calcutta");
		$date=date("Y-m-d H:m:s");
		$refval=$this->input->post('tic_id');
		$user_type=$this->session->userdata('user_type');
		$user_id=$this->session->userdata('user_id');
		$data = array(
				'ticket_id' => $refval,
				'user_id' => $user_id,
				'user_role_id' => $user_type,
				'message' => $this->input->post('msg'), 
				'created' => $date 
				);
		$result=$this->Clientmodel->close_tick($data);
		$data2=array('ticket_id' => $refval,'status_id'=>'5','created'=>$date,'updatedby'=>$user_id );
		 $this->Clientmodel->c_closeing_tic_ststus_to5($refval);
        $result2=$this->Empmodel->emp_solved_history($data2);
        if($result2)
        	redirect('Client/Client_dashboard', 'refresh');
        else
        	redirect('Client/Client_closed', 'refresh');
	}
	public function Aview_employee()
	{      
		$data['emp_info']=$this->Clientmodel->clint_on_emp();
        $this->load->view('Client/Aview_employee',$data);
	}



 





	 
}

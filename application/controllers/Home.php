<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->helper(array('form','url','html'));
    $this->load->helper('security');
    $this->load->database(); 
    $this->load->model('Homemodel');       
    $this->load->model('Authmodel');       

  }
	public function Homevieww()
	{
		$data['no_clients']=$this->Homemodel->get_all_no_clients();
		$data['no_scheduled']=$this->Homemodel->get_all_no_scheduled();
		$data['no_closed']=$this->Homemodel->get_all_closed();
		$data['no_tickets']=$this->Homemodel->get_all_tickets();
		$data['no_projects']=$this->Homemodel->get_all_projects();

		//var_dump($data);exit();
		$this->load->view('Home/Homeview',$data);
	}
	 
}

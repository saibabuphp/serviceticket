



<?php
class Adminmodel extends CI_Model
{    
    function __construct() {
        parent::__construct();
    }

    function get_emp() 
    {
        $res=$this->db->select('employee_profile.emp_id,employee_profile.emp_name')    
            ->from('employee_profile')
            ->where('user_role_id!=',4)->get()->result_array();
            return($res);  
    }
    function get_priority_list() 
    {
        $res=$this->db->select('priority_list.id,priority_list.priority_type')    
            ->from('priority_list')->get()->result_array();
            return($res);  
    }
	function get_newtickets() 
    {
              $res = $this->db->select('ticket_id,request_type.reqirement_type as request_type,ticket_sub,ticket_discription,priority_list.priority_type as ticket_priority,created')
                ->from('tickets')
                ->join('priority_list','priority_list.id=tickets.ticket_priority')
                ->join('request_type','request_type.id=tickets.request_type')
                
                ->where('assigned_flag',0)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array();
        return $res;  
    }
	function get_shedule_tickets() 
	    {
	        $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,ticket_assigned.*')
                  ->from('tickets')
                  ->join('priority_list','priority_list.id=tickets.ticket_priority')
                  ->join('request_type','request_type.id=tickets.request_type')
	                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                  ->where('tickets.tick_ststus',2)
                  ->group_by('tickets.ticket_id')
                  ->order_by("tickets.id","desc")->get()->result_array();
	        return $res;  
	    }
	function get_onhold_tickets() 
		    { 
		                $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,ticket_assigned.*')
                    ->from('tickets')
                    ->join('priority_list','priority_list.id=tickets.ticket_priority')
                    ->join('request_type','request_type.id=tickets.request_type')
                    ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                    ->where('ticket_assigned.emp_id',1)
                    ->group_by('tickets.ticket_id')
                    ->order_by("tickets.id","desc")->get()->result_array(); 
            return $res;  
 
		    }
	function get_resolved_tickets() 
			    {	 
			                $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,ticket_assigned.*')
			                ->from('tickets')
                      ->join('priority_list','priority_list.id=tickets.ticket_priority')
                      ->join('request_type','request_type.id=tickets.request_type')

			                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
			                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
				                ->where('tickets.tick_ststus',4)
			                ->where('ticket_assigned.emp_cmp_time!=',0)
                      ->group_by('tickets.ticket_id')
                      ->order_by("tickets.id","desc")->get()->result_array(); 
			        return $res;  
			    }
	function get_closed_tickets() 
				    {	 
				               $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,ticket_assigned.*')
				                ->from('tickets')
                        ->join('priority_list','priority_list.id=tickets.ticket_priority')
                        ->join('request_type','request_type.id=tickets.request_type')

				                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
				                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
				                ->where('tickets.tick_ststus',5)
				                ->where('ticket_assigned.emp_cmp_time!=',0)
                        ->group_by('tickets.ticket_id')
                        ->order_by("tickets.id","desc")->get()->result_array(); 
				        return $res;  
				    }
 

	function get_assignsingleticketdet($refval)
	    {
	    	 $res = $this->db->select('tickets.*,client_url_info.*,')
                ->from('tickets')
                ->where('ticket_id',$refval)
                ->join('client_url_info','client_url_info.project_id=tickets.project_id') 
                ->where('tickets.assigned_flag',0)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array(); 

	        /*$res = $this->db->select('tickets.*,client_url_info.clint_id,client_url_info.url_link,clients_profile.clint_company_name')
	                ->from('tickets')
	                ->join('client_url_info','client_url_info.project_id=tickets.project_id')
	                ->join('clients_profile','clients_profile.clint_uid=client_url_info.clint_id')
	                ->where('tickets.ticket_id',$refval)->get()->result_array();*/
	        return $res;  
	    }

    function schedul__()
    {
       $res=$this->db->select('ta.created,tick.ticket_sub,rt.reqirement_type,pl.priority_type,emp.emp_name')
            ->from('ticket_assigned ta')
            ->join('tickets tick','tick.ticket_id=ta.ticket_id')
            ->join('request_type rt','rt.id=tick.request_type')
            ->join('priority_list pl','pl.id=tick.ticket_priority')
            ->join('employee_profile emp','emp.emp_id=ta.emp_id')
            ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get();
           // ->join('priority_info pi','pi.ticket_id=ta.ticket_id')
            
        return($res);
    }
    
    function resolved__() 
    {
      $res=$this->db->select('thistory.updated,tick.ticket_sub,rt.reqirement_type,pl.priority_type')
           ->from('tickets_history thistory')
           ->join('tickets tick','tick.ticket_id=thistory.ticket_id')
           ->join('request_type rt','rt.id=tick.request_type')
           ->join('priority_list pl','pl.id=tick.ticket_priority')
           ->where('status_id',4)
           >group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get();
        return($res);
    }
       /* function admin_tic_assigned_flag_change($ticket_id)
        {
             $this->db->set('assigned_flag','1')
              ->set('tick_ststus','2')
             ->where('tickets.ticket_id',$ticket_id)
             ->update('tickets');
                  
        }*/
        function admin_tic_assigned_flag_change($ticket_id,$tick_ststus)
        {
             $this->db->set('assigned_flag','1')
              ->set('tick_ststus',$tick_ststus)
             ->where('tickets.ticket_id',$ticket_id)
             ->update('tickets');
                  
        }

       function get_techemp() 
    {
        $res=$this->db->select('employee_profile.emp_id,employee_profile.emp_name')    
            ->from('employee_profile')
            ->where('user_role_id',1)
            ->group_by('employee_profile.emp_id')
                ->order_by("employee_profile.id","desc")->get()->result_array();
            return($res);  
    }   
     function admin_tic_assigned_emp($data) 
    {
        $res=$this->db->insert('ticket_assigned',$data);
            return($res);  
    }
    function get_roles_for_create_emp() 
    {
        $res=$this->db->select('user_roles.id,user_roles.user_name')    
             ->from('user_roles')
            ->where('id!=',7)->get()->result();
            return($res);  
    }
    function get_clint_name() 
    {
        $res=$this->db->select('clients_profile.clint_uid,clients_profile.clint_company_name')    
            ->get('clients_profile');
            return($res);  
    }
    function inst_role_tbl($data) 
    {
       $res=$this->db->insert('user_roles',$data); 
       return($res);
    }
    function inst_emp_tbl($data) 
    {
       $res=$this->db->insert('employee_profile',$data); 
       return($res);
    }
    function inst_userlogin_for_emp($data) 
    {
       $res=$this->db->insert('userlogin',$data); 
       return($res);
    }
    function inst_clint_tbl($data) 
    {
       $res=$this->db->insert('clients_profile',$data); 
       return($res);
    }
    function inst_project_tbl($data_url_info,$data_proj_details) 
    {
       $res_url_info=$this->db->insert('client_url_info',$data_url_info);    
       $res_details=$this->db->insert('clients_project_details',$data_proj_details); 
       if($res_url_info && $res_details)
         return(true);
       else
         return(false);
       
    }
    function get_Clientdetails() 
      {
        $res=$this->db->select('clients_profile.clint_uid,clients_profile.clint_company_head_name,clients_profile.clint_company_name,clients_profile.clint_company_logo')
            ->from('clients_profile')
            ->group_by('clients_profile.clint_uid')
                ->order_by("clients_profile.id","desc")
            ->get()->result_array();
           //var_dump($res);exit();
            return($res);  
       }
       function get_view_employeedetails() 
      {
        $res=$this->db->select('employee_profile.emp_id,employee_profile.emp_name,employee_profile.emp_pic,user_roles.user_name')    
            ->from('employee_profile')
            ->join('user_roles','user_roles.id =employee_profile.user_role_id')
            ->group_by('employee_profile.emp_id')
                ->order_by("employee_profile.id","desc")
                ->get()->result_array();
           // var_dump($res);exit();
            return($res);  
       }
       function get_view_roledetails() 
      {
        $res=$this->db->select('user_roles.user_name,user_roles.user_logo')    
            ->from('user_roles')
            ->get()->result_array();
           // var_dump($res);exit();
            return($res);  
       }
        function get_view_proritylist() 
      {
        $res=$this->db->select('priority_list.priority_type')    
            ->from('priority_list')
            ->get()->result_array();
           // var_dump($res);exit();
            return($res);  
       }
       function get_client_proj_view_for_a_view() 
    {        
         
        $res = $this->db->select('clients_profile.clint_company_name,clients_profile.clint_company_logo,clients_profile.clint_company_head_name,client_url_info.clint_id,client_url_info.project_id,client_url_info.c_url_link,client_url_info.created')
                ->from('client_url_info')
                ->join('clients_profile','clients_profile.clint_uid = client_url_info.clint_id')
                ->group_by('client_url_info.clint_id')
                ->order_by("client_url_info.clint_id", "desc")->get()->result_array(); 
                return($res);   
    }
      function get_topay_company_bank_update_detailspre() 
    {        
        $res = $this->db->select('*')
                ->from('web_grid_info')->get()->result_array(); 
                return($res);   
    }
    function companybankinfo_update_details($data)
    {
        $this->db->where('id',1); 
        $this->db->update('web_grid_info', $data);
    }
    function get_a_emps_count()
    {
        return($this->db->count_all_results('employee_profile'));
    }
    function get_a_clients_count()
        {
            return($this->db->count_all_results('clients_profile'));
        }
    function get_a_projectcs_count()
        {
            return($this->db->count_all_results('client_url_info'));
        }
    function get_a_userrole_count()
        {
            return($this->db->count_all_results('user_roles'));
        }
    function get_a_priority_count()
        {
            $rr=$this->db->count_all_results('priority_list');
            return $rr;
           //echo "string"; var_dump($rr);exit();
        }
        function get_a_compbank_info_count()
        {
            $rr=$this->db->count_all_results('web_grid_info');
            return $rr;
           //echo "string"; var_dump($rr);exit();
        }
      function get_a_total_tickets_count()
        {
            $rr=$this->db->count_all_results('tickets');
            return $rr;
             
        }
        function get_A_empinfoview_updatepre($refval) 
    {        
        $res = $this->db->select('*')

                ->from('employee_profile')
                 ->where('emp_id',$refval)->get()->result_array(); 
                return($res);   
    }
    function A_empinfoview_update_details($data,$id)
    {
        $this->db->where('emp_id',$id); 
        $this->db->update('employee_profile', $data);
    }

 
function get_clientinfoview_updatepre($refvalue) 
    {        
        $res = $this->db->select('*')

                ->from('clients_profile')
                 ->where('clint_uid',$refvalue)->get()->result_array(); 
                return($res);   
    }
    function A_clientinfoview_update_details($data,$id)
    {
        $this->db->where('clint_uid',$id); 
        $this->db->update('clients_profile',$data);
    } 
 

function get_A_view_projinfo_details($pro_id) 
    {        
        $res = $this->db->select('*')

                ->from('clients_project_details')
                 ->where('clients_project_details.project_id',$pro_id)->get()->result_array(); 
                return($res);   
    } 

     function A_view_projinfo_update_details($data,$id)
    {
        $this->db->where('project_id',$id); 
        $this->db->update('clients_project_details',$data);
    }

    function get_A_view_projcpinfo_details($pro_id) 
    {        
        $res = $this->db->select('*')

                ->from('client_url_info')
                 ->where('client_url_info.project_id',$pro_id)->get()->result_array(); 
                return($res);   
    } 
    function A_view_projcpinfo_update_details($data,$id)
    {
        $this->db->where('project_id',$id); 
        $this->db->update('client_url_info',$data);
    }

    /*function get_Admin_profiledetails() 
    {        
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('employee_profile.emp_id,employee_profile.emp_name,employee_profile.emp_pic,employee_profile.emp_mail,employee_profile.emp_gender,employee_profile.user_role_id,employee_profile.phone_no,employee_profile.dob,employee_profile.doj,employee_profile.address' )
                ->from('employee_profile') 
                ->where('employee_profile.emp_id',$user_id)
                ->group_by('employee_profile.emp_id')
                ->order_by("employee_profile.id","desc")->get()->result_array(); 
                return($res);   
    }*/
    function get_Admin_profiledetails() 
    {        
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('employee_profile.emp_id,employee_profile.emp_name,employee_profile.emp_pic,employee_profile.emp_mail,gender.gender_type,user_roles.user_name,employee_profile.phone_no,employee_profile.dob,employee_profile.doj,employee_profile.address' )
                ->from('employee_profile') 
                ->where('employee_profile.emp_id',$user_id)
                ->join('gender','gender.id=employee_profile.emp_gender')
                  ->join('user_roles','user_roles.id=employee_profile.user_role_id')
                ->group_by('employee_profile.emp_id')
                ->order_by("employee_profile.id","desc")->get()->result_array(); 
                return($res);   
    }

    function get_admin_profile_update_detailspre($id) 
    {        
        $res = $this->db->select('*')
                ->from('employee_profile')
                ->where('employee_profile.emp_id',$id)->get()->result_array(); 
                return($res);   
    }
    function adminprofileinfo_update_details($data,$emp_uid)
    {
        $this->db->where('emp_id',$emp_uid); 
        $this->db->update('employee_profile', $data);
    }
    function ins_tick_msg($data)
        {
          $res=$this->db->insert('ticket_messages',$data);
          return($res);
        }
        function get_onprog_single_ticket($refval)
    {
         $res = $this->db->select('tickets.*,client_url_info.*,employee_profile.emp_name,ticket_assigned.admin_time')
          ->from('tickets')
          ->where('tickets.ticket_id',$refval)
          ->join('client_url_info','client_url_info.project_id=tickets.project_id')
          ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
          ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id') 
          ->where('tickets.tick_ststus',2)
          ->group_by('tickets.ticket_id')
          ->order_by("tickets.id","desc")->get()->row_array(); 
          return($res);
    }    
    function get_resolved_single_ticket($refval)
    {
         $res = $this->db->select('tickets.*,client_url_info.*,employee_profile.emp_name,ticket_assigned.admin_time,ticket_assigned.emp_estimate_time,ticket_assigned.emp_cmp_time')
          ->from('tickets')
          ->where('tickets.ticket_id',$refval)
          ->join('client_url_info','client_url_info.project_id=tickets.project_id')
          ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
          ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id') 
          ->where('tickets.tick_ststus',4)
          ->group_by('tickets.ticket_id')
          ->order_by("tickets.id","desc")->get()->row_array(); 
          return($res);
    } 
    function get_closed_single_ticket($refval)
    {
         $res = $this->db->select('tickets.*,client_url_info.*,employee_profile.emp_name,ticket_assigned.admin_time,ticket_assigned.emp_estimate_time,ticket_assigned.emp_cmp_time')
          ->from('tickets')
          ->where('tickets.ticket_id',$refval)
          ->join('client_url_info','client_url_info.project_id=tickets.project_id')
          ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
          ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id') 
          ->where('tickets.tick_ststus',5)
          ->group_by('tickets.ticket_id')
          ->order_by("tickets.id","desc")->get()->row_array(); 
          return($res);
    }
    function get_onhold_single_ticket($refval)
    {
         $res = $this->db->select('tickets.*,client_url_info.*,ticket_assigned.updated,ticket_messages.message')
          ->from('tickets')
          ->where('tickets.ticket_id',$refval)
          ->join('client_url_info','client_url_info.project_id=tickets.project_id')
          ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
          ->join('ticket_messages','ticket_messages.ticket_id=tickets.ticket_id') 
          ->where('ticket_assigned.emp_id',1)
          ->group_by('tickets.ticket_id')
          ->order_by("tickets.id","desc")->get()->row_array(); 
          return($res);
    }
    function onhold_tic_update($data,$ticket_id)
    {
        $res=$this->db->set($data)
        ->where('ticket_id',$ticket_id)
        ->update('ticket_assigned');     
        return($res);
    }
    function onhold_tick_ststus_update($ticket_id)
    {
        $res=$this->db->set('tick_ststus',2)
        ->where('ticket_id',$ticket_id)
        ->update('tickets');     
        return($res);
    }
    function onhold_tick_history_update($data)
    {
      $res=$this->db->insert('tickets_history',$data);
    }
    public function a_emp_deactivate_employee_profile($emp_uid)
    { 
       $res=$this->db->set('emp_status',0)
            ->where('emp_id',$emp_uid)
            ->update('employee_profile');
           return($res);
    }
    public function a_emp_deactivate_userlogin($user_id)
    {
       $res=$this->db->set('status',0)
            ->where('user_id',$user_id)
            ->update('userlogin');
           return($res);
    }
    public function a_proj_deactivate_clients_project_details($proj_uid)
    {   
       $res=$this->db->set('project_status',0)
            ->where('project_id',$proj_uid)
            ->update(' clients_project_details');
           return($res);
    }
    public function a_proj_deactivate_proj_url($proj_uid)
    {
      $res=$this->db->set('url_status',0)
            ->where('project_id',$proj_uid)
            ->update('client_url_info');
          return($res);
    }
    public function a_client_deactivate_clients_profile($clint_uid)
    {
      $res=$this->db->set('client_status',0)
           ->where('clint_uid',$clint_uid)
           ->update('clients_profile');
     return($res);
    }

     public function a_client_deactivate_userlogin($user_id)
    {
      $res=$this->db->set('status',0)
           ->where('user_id',$user_id)
           ->update('userlogin');
     return($res);
    }











               







}
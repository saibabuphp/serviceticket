
<?php
class Clientmodel extends CI_Model
{    
    function __construct() {
        parent::__construct();
    }
    
    function get_project_id(){ 
            $res = $this->db->select('project_id,c_url_link')
                ->from('client_url_info') 
                ->where('clint_id',$this->session->userdata('user_id'))->get(); 
       return($res); 
    } 
    function get_priority_list(){
        $res=$this->db->get('priority_list');
        return($res);
    }
    function get_request_list(){
        $res=$this->db->get('request_type');
        return($res);
    }
    
    function ticket_insert($data){
        $rs=$this->db->insert('tickets',$data);
       
    }
    
    /*function active_tick()
    {
        $res= $this->db->select('tickets_history.created,tickets_history.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.url_link')
         ->from('tickets_history')
         ->join('tickets','tickets.ticket_id=tickets_history.ticket_id')    
         ->where('status_id !=',4)
         ->where('status_id !=',5)
         ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")
         ->get();
         return($res);
    }*/
   /* function resolved_tick()
    {
       $res= $this->db->select('tickets_history.created,tickets_history.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.url_link')
         ->from('tickets_history')
         ->join('tickets','tickets.ticket_id=tickets_history.ticket_id')    
         ->where('status_id',4)
         ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")
         ->get();
         return($res); 
    }*/
    /*function closed_tick()
    {
      $res= $this->db->select('tickets_history.created,tickets_history.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.url_link')
         ->from('tickets_history')
         ->join('tickets','tickets.ticket_id=tickets_history.ticket_id')    
         ->where('status_id',5)
         ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")
         ->get();
         return($res);   
    }*/
    function act_count_tick()
    {
       return $res=$this->db->select('count(status_id) as total_tick')
        ->from('tickets_history')
        ->where('status_id !=',4)
        ->where('status_id !=',5)

        ->get()->result_array();
    }
    function resolved_count_tick()
    {
       return $res=$this->db->select('count(status_id) as resolved_tick')
        ->from('tickets_history')
        ->where('status_id',4)
        ->get()->result_array();
    }
    function closed_count_tick()
    {
       return $res=$this->db->select('count(status_id) as closed_tick')
        ->from('tickets_history')
        ->where('status_id',5)
        ->get()->result_array();
    }
    
     function get_clnt_new_ticket() 
    {        
        $user_id=$this->session->userdata('user_id'); 
         $res = $this->db->select('tickets.ticket_id,tickets.request_type,tickets.ticket_sub,tickets.ticket_discription,tickets.ticket_priority,tickets.created,tickets.project_id')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')  
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.assigned_flag',0)
                 ->group_by('tickets.ticket_id')
                 ->order_by('ticket_id', 'DESC')->get()->result_array();
                return($res);   
            /*     $res = $this->db->select('tickets.ticket_id,tickets.request_type,tickets.ticket_sub,tickets.ticket_discription,tickets.ticket_priority,tickets.created,tickets.project_id,ticket_assigned.emp_id,ticket_assigned.admin_assigned_toemp')
                ->from('client_url_info') 
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->where('tickets.assigned_flag',0)
                ->where('client_url_info.clint_id',$user_id)->get()->result_array();
                 $res1 = $this->db->select('client_url_info.*,tickets.*,ticket_assigned.emp_id,ticket_assigned.admin_assigned_toemp')
                ->from('client_url_info') 
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')

                ->where('client_url_info.clint_id',$user_id)->get()->result_array(); 
                echo "<pre>"; var_dump($res);var_dump($res1);   exit();*/
     } 
    //to do pagination  Client-Client_newtickets ..starts
    function get_clnt_new_ticket_pn($limit,$offset) 
    {        
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,tickets.project_id')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')
                ->join('priority_list','priority_list.id=tickets.ticket_priority')
                ->join('request_type','request_type.id=tickets.request_type')
  
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.assigned_flag',0)
                 ->group_by('tickets.ticket_id')
                 ->order_by('ticket_id', 'DESC')
                 ->limit($limit,$offset)->get()->result_array();
                return($res);  
          
    }
    function get_clnt_new_ticket_num_rows() 
    {        
        $user_id=$this->session->userdata('user_id'); 
         $res = $this->db->select('tickets.ticket_id,tickets.request_type,tickets.ticket_sub,tickets.ticket_discription,tickets.ticket_priority,tickets.created,tickets.project_id')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')  
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.assigned_flag',0)
                 ->group_by('tickets.ticket_id')
                 ->order_by('tickets.ticket_id', 'DESC')->get(); 
                return $res->num_rows(); 
          
    }
    //to do pagination  Client-Client_newtickets ..ends
     function get_clnt_scheduled_ticket() 
    {        
        $user_id=$this->session->userdata('user_id'); 
                $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,tickets.project_id,employee_profile.emp_name as emp_id,ticket_assigned.admin_assigned_toemp')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('request_type','request_type.id=tickets.request_type')
                ->join('priority_list','priority_list.id=tickets.ticket_priority')
                ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id')
                ->where('tickets.tick_ststus',2)
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.assigned_flag',1)
                 ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')
                ->get()->result_array();
                return($res);   
    }
     function get_clnt_onhold_ticket() 
    {        
        $user_id=$this->session->userdata('user_id'); 
       $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,tickets.project_id,ticket_assigned.emp_id,ticket_assigned.admin_assigned_toemp')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('request_type','request_type.id=tickets.request_type')
                ->join('priority_list','priority_list.id=tickets.ticket_priority')

                ->where('client_url_info.clint_id',$user_id)
                ->where('ticket_assigned.emp_id',1)
                ->where('tickets.assigned_flag',1)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')
                ->get()->result_array();
                return($res);   
    }
    function get_clnt_resolved_ticket() 
    {        
        $user_id=$this->session->userdata('user_id'); 
      $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,tickets.project_id,employee_profile.emp_name as emp_id,ticket_assigned.admin_assigned_toemp,tickets.updated')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
                ->join('request_type','request_type.id=tickets.request_type')
                ->join('priority_list','priority_list.id=tickets.ticket_priority')
                ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id') 
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.tick_ststus',4)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array();
                return($res);   
    }
    function get_clnt_closed_ticket() 
    {        
        $user_id=$this->session->userdata('user_id'); 
                $res = $this->db->select('tickets.ticket_id,request_type.reqirement_type as request_type,tickets.ticket_sub,tickets.ticket_discription,priority_list.priority_type as ticket_priority,tickets.created,tickets.project_id,employee_profile.emp_name as emp_id ,ticket_assigned.admin_assigned_toemp,tickets.updated')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id') 
                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
                ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id')
				->join('request_type','request_type.id=tickets.request_type')
                ->join('priority_list','priority_list.id=tickets.ticket_priority')
                ->where('tickets.tick_ststus',5)
                ->where('client_url_info.clint_id',$user_id)
                ->where('ticket_assigned.emp_cmp_time!=',0) 
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")
                ->get()->result_array();
                return($res);   
    }
    function get_Client_profileview() 
    {        
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('client_url_info.clint_id,client_url_info.project_id,client_url_info.c_url_link,client_url_info.url_config_file_uid,client_url_info.url_config_file_pwd,client_url_info.url_config_db_uid,client_url_info.url_config_db_pwd,client_url_info.created')
                ->from('client_url_info')
                ->join('clients_profile','clients_profile.clint_uid = client_url_info.clint_id')
                ->where('client_url_info.clint_id',$user_id)
                ->group_by('client_url_info.clint_id')
                ->order_by("client_url_info.clint_id", "asc")->get()->result_array(); 
                return($res);   
    }
    function get_Client_companydetails() 
    {        
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('clients_profile.clint_company_name,clients_profile.clint_uid,clients_profile.clint_company_logo,clients_profile.clint_company_adres,clients_profile.clint_company_head_name,clients_profile.clint_company_head_pic,clients_profile.clint_company_head_mail,clients_profile.clint_company_head_ph')
                ->from('clients_profile') 
                ->where('clients_profile.clint_uid',$user_id)
                ->group_by('clients_profile.clint_uid')
                ->order_by("clients_profile.id","desc")->get()->result_array(); 
                return($res);   
    }
     function get_webgrid_details() 
    {        
        $res = $this->db->select('*')
                ->from('web_grid_info')->get()->result_array(); 
                return($res);   
    }
    function clnt_profile_view_menu() 
    {     
         $user_id=$this->session->userdata('user_id');    
        $res = $this->db->select('clint_company_name,clint_company_logo,clint_company_adres')
                ->from('clients_profile')
                ->where('clients_profile.clint_uid',$user_id)
                ->group_by('clients_profile.clint_uid')
                ->order_by("clients_profile.id","desc")->get()->result_array(); 
                return($res);   
    }
    function clnt_rise_tic_menu($tic_id) 
    {     
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,tickets.project_id,tickets.url_link,tickets.ticket_pics,priority_list.priority_type,request_type.reqirement_type,client_url_info.c_url_link,clients_profile.clint_company_name')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')
                ->join('priority_list','priority_list.id = tickets.ticket_priority')
                ->join('request_type','request_type.id = tickets.request_type')
                ->join('clients_profile','client_url_info.clint_id=clients_profile.clint_uid')
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.ticket_id',$tic_id)
                ->where('tickets.assigned_flag',0)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
                return($res);
    }
    function approved_tic_info($tic_id) 
    {     
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,tickets.project_id,tickets.url_link,tickets.ticket_pics,priority_list.priority_type,request_type.reqirement_type,client_url_info.c_url_link,clients_profile.clint_company_name,ticket_assigned.admin_assigned_toemp,employee_profile.emp_name')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')
                ->join('priority_list','priority_list.id = tickets.ticket_priority')
                ->join('request_type','request_type.id = tickets.request_type')
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('clients_profile','client_url_info.clint_id=clients_profile.clint_uid')
                ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id')
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.ticket_id',$tic_id)
                ->where('tickets.assigned_flag',1)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
                return($res);
    }
    function onhold_tic_info($tic_id) 
    {     
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,tickets.project_id,tickets.url_link,tickets.ticket_pics,priority_list.priority_type,request_type.reqirement_type,client_url_info.c_url_link,clients_profile.clint_company_name,ticket_assigned.admin_assigned_toemp,ticket_assigned.emp_id as emp_name,')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')
                ->join('priority_list','priority_list.id = tickets.ticket_priority')
                ->join('request_type','request_type.id = tickets.request_type')
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('clients_profile','client_url_info.clint_id=clients_profile.clint_uid')
                //->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id')
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.ticket_id',$tic_id)
                ->where('ticket_assigned.emp_id',1)
                ->where('tickets.assigned_flag',1)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
                return($res);
        
    }
    function resolved_tic_info($tic_id) 
    {     
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,tickets.project_id,tickets.url_link,tickets.ticket_pics,priority_list.priority_type,request_type.reqirement_type,client_url_info.c_url_link,clients_profile.clint_company_name,ticket_assigned.admin_assigned_toemp,employee_profile.emp_name')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')
                ->join('priority_list','priority_list.id = tickets.ticket_priority')
                ->join('request_type','request_type.id = tickets.request_type')
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('clients_profile','client_url_info.clint_id=clients_profile.clint_uid')
                ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id')
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.ticket_id',$tic_id)
                ->where('ticket_assigned.emp_cmp_time!=',0)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
                return($res); 
    }
    function closed_tic_info($tic_id) 
    {     
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,tickets.project_id,tickets.url_link,tickets.ticket_pics,priority_list.priority_type,request_type.reqirement_type,client_url_info.c_url_link,clients_profile.clint_company_name,ticket_assigned.admin_assigned_toemp,employee_profile.emp_name')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id')
                ->join('priority_list','priority_list.id = tickets.ticket_priority')
                ->join('request_type','request_type.id = tickets.request_type')
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('clients_profile','client_url_info.clint_id=clients_profile.clint_uid')
                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
                ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id')
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.ticket_id',$tic_id)
                ->where('tickets_history.status_id!=',5)
                ->where('ticket_assigned.emp_cmp_time!=',0)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
                return($res); 
    }
    function get_c_assigned_tickets_count() 
    {     
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->where('client_url_info.clint_id',$user_id)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
                return($res); 
    }
    function get_client_profile_update_detailspre($id) 
    {        
        $res = $this->db->select('*')
                ->from('clients_profile')
                ->where('clients_profile.clint_uid',$id)->get()->result_array(); 
                return($res);   
    }
    function companyprofileinfo_update_details($data,$clint_uid)
    {
        $this->db->where('clint_uid',$clint_uid); 
        $this->db->update('clients_profile', $data);
    }
    
    
function close_tick($data)
    {
        $res=$this->db->insert('ticket_messages',$data);
        if($res)
            return(true);
        else
            return(false);

    }
    function clint_on_emp()
    {
        $user_id=$this->session->userdata('user_id'); 
        $res=$this->db->select('employee_profile.emp_name,user_roles.user_name')
             ->from('clients_project_details')
             ->join('tickets','tickets.project_id=clients_project_details.project_id')
             ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
             ->join('employee_profile','employee_profile.emp_id=ticket_assigned.emp_id')
             ->join('user_roles','user_roles.id=employee_profile.user_role_id')
             ->where('clients_project_details.clint_id',$user_id)
             ->group_by('employee_profile.emp_name')
             ->order_by("employee_profile.id","desc")->get()->result_array();
        return($res);
    }
function c_closeing_tic_ststus_to5($clint_uid)
    {
        $this->db->set('tick_ststus','5')
         ->where('ticket_id',$clint_uid) 
         ->update('tickets');
    }
    function active_tick()
    {
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,client_url_info.c_url_link')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('request_type','request_type.id=tickets.request_type')
                ->join('priority_list','priority_list.id=tickets.ticket_priority')
                ->where('tickets.tick_ststus',2)
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.assigned_flag',1)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')
                ->limit(2)
                ->get()->result_array();
                return($res);

    }
    function resolved_tick()
    {
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,client_url_info.c_url_link')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id')
                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
                ->join('request_type','request_type.id=tickets.request_type')
                ->join('priority_list','priority_list.id=tickets.ticket_priority') 
                ->where('client_url_info.clint_id',$user_id)
                ->where('tickets.tick_ststus',4)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array();
                return($res);   
    }
    function closed_tick()
    {
      $user_id=$this->session->userdata('user_id'); 
                $res = $this->db->select('tickets.ticket_id,tickets.ticket_sub,tickets.ticket_discription,tickets.created,client_url_info.c_url_link')
                ->from('client_url_info')
                ->join('tickets','client_url_info.project_id = tickets.project_id') 
                ->join('ticket_assigned','ticket_assigned.ticket_id=tickets.ticket_id') 
                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
                ->join('request_type','request_type.id=tickets.request_type')
                ->join('priority_list','priority_list.id=tickets.ticket_priority') 
                ->where('tickets.tick_ststus',5)
                ->where('client_url_info.clint_id',$user_id)
                ->where('ticket_assigned.emp_cmp_time!=',0) 
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")
                ->limit(2)
                ->get()->result_array();
                return($res);    
    }
    function this_week(){  
        $res=$this->db->select('tickets_history.*,tickets.ticket_sub,tickets.ticket_discription,ticket_status.status')
                  ->from('tickets_history')
                  ->join('tickets','tickets.ticket_id = tickets_history.ticket_id','left')
                  ->join('ticket_status','ticket_status.id = tickets_history.status_id','left')
                  ->where('tickets_history.updated>=' ,'DATE(NOW()) - INTERVAL 7 DAY')
                  ->group_by('tickets.ticket_id')
                  ->order_by('tickets_history.id','desc')
                  ->limit(5,10)->get()->result_array();
            return($res);
    }
    function last_day(){  
        $res=$this->db->select('tickets_history.*,tickets.ticket_sub,tickets.ticket_discription,ticket_status.status')
                  ->from('tickets_history')
                  ->join('tickets','tickets.ticket_id = tickets_history.ticket_id','left')
                  ->join('ticket_status','ticket_status.id = tickets_history.status_id','left')
                  ->where('tickets_history.updated>=' ,'DATE(NOW()) - INTERVAL 1 DAY')
                  ->group_by('tickets.ticket_id')
                  ->order_by('id','desc')
                  ->limit(5)->get()->result_array();
                 return($res);
    }
    function last_day_tick_count(){
           $res=$this->db->where('client_url_info.clint_id' ,$this->session->userdata('user_id'))
                ->from('client_url_info')
                ->join('tickets','tickets.project_id=client_url_info.project_id')
                ->where('tickets.created>=' ,'DATE(NOW()) - INTERVAL 1 DAY')
                ->count_all_results();
            return($res);
    }
    function last_week_tick_count(){
             $res=$this->db->where('client_url_info.clint_id' ,$this->session->userdata('user_id'))
                ->from('client_url_info')
                ->join('tickets','tickets.project_id=client_url_info.project_id')
                ->where('tickets.created>=' ,'DATE(NOW()) - INTERVAL 7 DAY')
                ->count_all_results();
                return($res);
    }



    
  
    
}
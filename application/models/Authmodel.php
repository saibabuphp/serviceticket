
<?php
class Authmodel extends CI_Model
{    
    function __construct() {
        parent::__construct();
    }

    function loginverfication($data)
    {
        $identity    = $data['Username'];
        $password    = $data['Password'];
        if($identity != "" && $password != "") 
          	{
                $res = $this->db->select('user_id,user_pwd,user_type')
                ->from('userlogin')
                ->where('userlogin.user_id',$identity)
                ->where('userlogin.user_pwd',$password)
                ->where('userlogin.status', 1)->get()->result_array();
                //var_dump($res);echo "string";echo $res[0]['user_id']; exit;    
                if(count($res) > 0) 
                       { 
                         return $res;
                       }
                       else
                       {
                         return false;
                       }
                
            } 
    }
    function get_client_profiledetails_superhead()
    {
    	$res = $this->db->select('clint_company_name as name,clint_company_logo as pic,clint_company_adres as address')
                ->from('clients_profile')
                ->where('clients_profile.clint_uid',$this->session->userdata('user_id'))->get()->row_array();
        return $res;
    }  
     function get_emp_profiledetails_superhead()
    {
      $user_id=$this->session->userdata('user_id');    
        $res = $this->db->select('emp_name as name,emp_pic as pic,address as address')
                ->from('employee_profile')
                ->where('employee_profile.emp_id',$user_id)->get()->row_array(); 
                return($res); 
        
    }    
}
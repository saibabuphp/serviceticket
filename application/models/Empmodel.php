
<?php
class Empmodel extends CI_Model
{    
    function __construct() {
        parent::__construct();
    }

    function get_emp_newtickets()   
    {
    	 
        $user_id=$this->session->userdata('user_id');         
        $res = $this->db->select('ticket_assigned.ticket_id,ticket_assigned.admin_priority,ticket_assigned.admin_assigned_toemp,tickets.ticket_sub,tickets.ticket_discription,priority_type,tickets.url_link,tickets.created')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->where('emp_id',$user_id)
                ->where('tickets.emp_accept',0)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array();
                return($res);   
    }
    
    function get_emp_inprogress() 
    {
        
         $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('ticket_assigned.ticket_id,ticket_sub,ticket_discription,priority_type,emp_estimate_time,emp_accept_time')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->where('emp_id',$user_id)
                ->where('tickets.emp_accept',1)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array();
                return($res);   
    }
    function get_emp_resolved() 
    {
        
         $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('ticket_assigned.ticket_id,ticket_sub,ticket_discription,priority_type,emp_estimate_time,emp_accept_time,emp_cmp_time')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
				->where('tickets_history.status_id',4)
                ->where('tickets_history.status_id!=',5)
                ->where('emp_id',$user_id)
                ->where('ticket_assigned.emp_cmp_time !=',0)
                ->where('tickets.tick_ststus',4)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array();
                return($res);   
    }

	function get_emp_closed_tickets() 
    {
        
         $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('ticket_assigned.ticket_id,ticket_sub,ticket_discription,priority_type,emp_estimate_time,emp_accept_time,emp_cmp_time')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->join('tickets_history','tickets_history.ticket_id=tickets.ticket_id')
				->where('tickets_history.status_id',5)
                ->where('emp_id',$user_id)
                ->where('ticket_assigned.emp_cmp_time !=',0)
                ->group_by('tickets.ticket_id')
                ->order_by("tickets.id","desc")->get()->result_array();
                return($res);   
    }
    function emp_profile_view_menu() 
    {     
         $user_id=$this->session->userdata('user_id');    
        $res = $this->db->select('emp_name,emp_pic,address,user_role_id')
                ->from('employee_profile')
                ->where('employee_profile.emp_id',$user_id)->get()->result_array(); 
                return($res);   
    }
    function get_e_assigned_tickets_count() 
    {     
         $user_id=$this->session->userdata('user_id');    
        $res = $this->db->select('emp_id')
                ->from(' ticket_assigned')
                ->where(' ticket_assigned.emp_id',$user_id)->get()->result_array(); 
                return($res);   
    }
    function get_tic_info($refval)  
    {
         
        $user_id=$this->session->userdata('user_id');         
        $res = $this->db->select('tickets.ticket_pics,ticket_assigned.ticket_id,ticket_assigned.emp_estimate_time,ticket_assigned.admin_priority,ticket_assigned.admin_time,ticket_assigned.admin_assigned_toemp,tickets.ticket_sub,tickets.ticket_discription,priority_type,tickets.url_link,tickets.created,client_url_info.c_url_link,client_url_info.url_config_file_uid,client_url_info.url_config_file_pwd,client_url_info.url_config_db_uid,client_url_info.url_config_db_pwd')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->join('client_url_info','tickets.project_id=client_url_info.project_id')
                ->where('emp_id',$user_id)
                ->where('ticket_assigned.ticket_id',$refval)
                ->where('tickets.emp_accept',0)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
               return($res);     
    }
    function emp_accept($refval,$data)
    {

       $res1= $this->db->set('ticket_assigned.emp_estimate_time',$data['emp_estimate_time'])
             ->set('ticket_assigned.emp_accept_time',$data['emp_accept_time'])
             ->where('ticket_assigned.ticket_id',$refval)
             ->update('ticket_assigned');

        $res2=$this->db->set('emp_accept','1')
        ->where('tickets.ticket_id',$refval)
        ->update('tickets');
        if($res1 && $res2)
            return(true);
        else
            return(false);
    } 
    function get_progress_info($refval)  
    {
         
        $user_id=$this->session->userdata('user_id');         
        $res = $this->db->select('tickets.ticket_pics,ticket_assigned.ticket_id,ticket_assigned.emp_estimate_time,ticket_assigned.admin_priority,ticket_assigned.admin_time,ticket_assigned.admin_assigned_toemp,tickets.ticket_sub,tickets.ticket_discription,priority_type,tickets.url_link,tickets.created,client_url_info.c_url_link,client_url_info.url_config_file_uid,client_url_info.url_config_file_pwd,client_url_info.url_config_db_uid,client_url_info.url_config_db_pwd')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->join('client_url_info','tickets.project_id=client_url_info.project_id')
                ->where('emp_id',$user_id)
                ->where('ticket_assigned.ticket_id',$refval)
                ->where('tickets.emp_accept',1)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();
               return($res);     
    }
    function emp_solved($refval,$emp_cmp_time)
    {
       $res1= $this->db->set('ticket_assigned.emp_cmp_time',$emp_cmp_time)
             ->where('ticket_assigned.ticket_id',$refval)
             ->update('ticket_assigned');
             if($res1)
            return(true);
        else
            return(false);      
    }
    function emp_solved_history($data)
    {
       $res1=$this->db->insert('tickets_history',$data);
       if($res1)
            return(true);
        else
            return(false); 
    }
    function get_Employee_profiledetails() 
    {        
        $user_id=$this->session->userdata('user_id'); 
        $res = $this->db->select('employee_profile.emp_id,employee_profile.emp_name,employee_profile.emp_pic,employee_profile.emp_mail,employee_profile.emp_gender,employee_profile.user_role_id,employee_profile.phone_no,employee_profile.dob,employee_profile.doj,employee_profile.address' )
                ->from('employee_profile') 
                ->where('employee_profile.emp_id',$user_id)
                ->group_by('employee_profile.emp_id')
                ->order_by("employee_profile.id","desc")->get()->result_array(); 
                return($res);   
    }

     function get_employee_profile_update_detailspre($id) 
    {        
        $res = $this->db->select('*')
                ->from('employee_profile')
                ->where('employee_profile.emp_id',$id)
                ->group_by('employee_profile.emp_id')
                ->order_by("employee_profile.id","desc")->get()->result_array(); 
                return($res);   
    }
    function employeeprofileinfo_update_details($data,$emp_uid)
    {
        $this->db->where('emp_id',$emp_uid); 
        $this->db->update('employee_profile', $data);
    }
   
    
    function get_work_clint_proj()
    {
         $user_id=$this->session->userdata('user_id'); 
         $res=$this->db->select('client_url_info.*')
              ->from('ticket_assigned')
              ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
              ->join('client_url_info','client_url_info.project_id=tickets.project_id')
              ->where('ticket_assigned.emp_id',$user_id)
              ->group_by('client_url_info.project_id')
              ->order_by("client_url_info.clint_id", "desc")
              ->get()->result_array();
              return($res);
    }
    function emp_tick_msg($data)
    {
        $res=$this->db->insert('ticket_messages',$data);
        return($res);
    }  
    function emp_ticks_emp_accept_2($refval)
    {   
        $res1= $this->db->set('emp_accept',"2")
            ->set('tick_ststus','4')
          ->where('ticket_id',$refval)
         ->update('tickets');
    }
    function get_s_tic_solved_info($refval)  
    {
        
        $user_id=$this->session->userdata('user_id');         
        $res = $this->db->select('tickets.ticket_pics,ticket_assigned.ticket_id,ticket_assigned.emp_estimate_time,ticket_assigned.admin_priority,ticket_assigned.admin_time,ticket_assigned.admin_assigned_toemp,ticket_assigned.emp_cmp_time,ticket_assigned.updated,tickets.ticket_sub,tickets.ticket_discription,priority_type,tickets.url_link,tickets.created,client_url_info.c_url_link,client_url_info.url_config_file_uid,client_url_info.url_config_file_pwd,client_url_info.url_config_db_uid,client_url_info.url_config_db_pwd')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->join('client_url_info','tickets.project_id=client_url_info.project_id')
                ->where('emp_id',$user_id)
                ->where('ticket_assigned.ticket_id',$refval)
                 
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array(); 
                 
               return($res);     
    }
    function get_s_tic_closed_info($refval)  
    {
        
        $user_id=$this->session->userdata('user_id');         
        $res = $this->db->select('tickets.ticket_pics,ticket_assigned.ticket_id,ticket_assigned.emp_estimate_time,ticket_assigned.admin_priority,ticket_assigned.admin_time,ticket_assigned.admin_assigned_toemp,ticket_assigned.emp_cmp_time,ticket_assigned.updated,tickets.ticket_sub,tickets.ticket_discription,priority_type,tickets.url_link,tickets.created,client_url_info.c_url_link,client_url_info.url_config_file_uid,client_url_info.url_config_file_pwd,client_url_info.url_config_db_uid,client_url_info.url_config_db_pwd')
                ->from('ticket_assigned')
                ->join('tickets','tickets.ticket_id=ticket_assigned.ticket_id')
                ->join('priority_list','priority_list.id=ticket_assigned.admin_priority')
                ->join('client_url_info','tickets.project_id=client_url_info.project_id')
                ->where('emp_id',$user_id)
                ->where('ticket_assigned.ticket_id',$refval)
                ->group_by('tickets.ticket_id')
                ->order_by('tickets.ticket_id', 'DESC')->get()->result_array();  
               return($res);     
    }







  
    
}
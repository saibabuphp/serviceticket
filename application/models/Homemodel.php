<?php

class Homemodel extends CI_Model
{
    
    function __construct() {
        parent::__construct();
    }

    function get_all_no_clients() {
    	return $this->db->count_all_results('clients_profile');
      /*$this->db->select('*'); 
      $this->db->from('clients_profile');   
      $query = $this->db->get();
      return $query->result();*/
    }
    function get_all_no_scheduled() {
    	return $this->db->count_all_results('ticket_assigned');      
    }
    function get_all_tickets() {
    	return $this->db->count_all_results('tickets');      
    }
    function get_all_projects() {
    	return $this->db->count_all_results('client_url_info');      
    }
    function get_all_closed() {
    	$this->db->where('status_id',5);
  		return $this->db->count_all_results('tickets_history');      
    }
    

 }
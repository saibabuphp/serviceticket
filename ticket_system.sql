-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2019 at 04:20 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ticket_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients_budget`
--

CREATE TABLE IF NOT EXISTS `clients_budget` (
`id` int(3) NOT NULL,
  `clint_id` varchar(50) NOT NULL,
  `project_id` varchar(50) NOT NULL,
  `paid_amount` int(7) NOT NULL,
  `date` datetime NOT NULL,
  `emp_id` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `clients_budget`
--

INSERT INTO `clients_budget` (`id`, `clint_id`, `project_id`, `paid_amount`, `date`, `emp_id`) VALUES
(1, '1905soum215', 'p1905soum215 	', 22000, '2019-05-24 07:00:12', '1905tehd325');

-- --------------------------------------------------------

--
-- Table structure for table `clients_profile`
--

CREATE TABLE IF NOT EXISTS `clients_profile` (
`id` int(3) NOT NULL,
  `clint_uid` varchar(50) NOT NULL,
  `clint_company_name` varchar(50) NOT NULL,
  `clint_pwd` varchar(50) NOT NULL,
  `clint_company_logo` varchar(60) NOT NULL,
  `clint_company_adres` varchar(200) NOT NULL,
  `clint_company_head_name` varchar(50) NOT NULL,
  `clint_company_head_pic` varchar(50) NOT NULL,
  `clint_company_head_mail` varchar(60) NOT NULL,
  `clint_company_head_ph` varchar(13) NOT NULL,
  `clint_company_contact_persion` varchar(50) NOT NULL,
  `clint_company_contact_persion_mail` varchar(60) NOT NULL,
  `clint_company_contact_persion_phone` varchar(13) NOT NULL,
  `client_status` int(3) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `clients_profile`
--

INSERT INTO `clients_profile` (`id`, `clint_uid`, `clint_company_name`, `clint_pwd`, `clint_company_logo`, `clint_company_adres`, `clint_company_head_name`, `clint_company_head_pic`, `clint_company_head_mail`, `clint_company_head_ph`, `clint_company_contact_persion`, `clint_company_contact_persion_mail`, `clint_company_contact_persion_phone`, `client_status`, `created`, `updated`) VALUES
(1, '1905soum145', 'soumya consul', '1905soum145', '1905soum145.jpg', 'hyderabad', 'ramesh', '1905soum145.jpg', 'ramsh@gmail.com', '9898989898', 'rajesh', 'rajesh@gmail.com', '9898989898', 0, '2019-05-23 06:17:21', '2019-05-24 11:16:54'),
(2, '1905moun495', 'sakshi news', '1905moun495', '1905moun495.png', 'Banjara hills road no 11', 'jagan', '1905moun495.jpg', 'jagan@sakshi.com', '9849158545', 'kamesh', 'info@sakshi.com', '852369775', 0, '2019-05-24 10:23:11', '2019-05-24 12:12:53'),
(3, '1905anup951', 'mm trends', '1905anup951', '1905anup951.png', 'madura nagar 2 line', 'anup kumar', '1905anup951.jpg', 'anup@gamil.com', '7539514869', 'hamesh', 'info@gamil.com', '2684357954', 0, '2019-05-24 11:20:51', '2019-05-24 12:13:01'),
(4, '1905hema264', 'man power', '1905hema264', '1905hema264.png', 'sai nagar 4 line', 'hema kumari', '1905hema264.jpg', 'hema@gamil.com', '3698741256', 'jarash', 'info@gamil.com', '2684357954', 0, '2019-05-24 01:53:51', '2019-05-24 12:13:07');

-- --------------------------------------------------------

--
-- Table structure for table `clients_project_details`
--

CREATE TABLE IF NOT EXISTS `clients_project_details` (
`id` int(3) NOT NULL,
  `project_id` varchar(50) NOT NULL,
  `clint_id` varchar(50) NOT NULL,
  `clint_loc_adres` varchar(200) NOT NULL,
  `clint_loc_head` varchar(50) NOT NULL,
  `clint_loc_head_mail` varchar(50) NOT NULL,
  `clint_loc_head_ph` int(13) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `clients_project_details`
--

INSERT INTO `clients_project_details` (`id`, `project_id`, `clint_id`, `clint_loc_adres`, `clint_loc_head`, `clint_loc_head_mail`, `clint_loc_head_ph`, `created`, `updated`) VALUES
(1, 'p1905soum215', '1905soum215', '1905soum145', 'rakesh', 'rakesh@gmail.com', 999999999, '2019-05-23 11:19:12', '2019-05-24 11:34:59'),
(2, 'p1905moun852', '1905moun495', 'hitech city', 'mounika', 'mounika@gmail.com', 841357226, '2019-05-24 20:56:22', '2019-05-24 00:51:53'),
(3, 'p1905anup159', '1905anup951', 'madhapur', 'venkatesh', 'venki@gmail.com', 753951488, '2019-05-24 22:26:02', '2019-05-24 00:51:53'),
(4, 'p1905soum753', '1905soum215', 'kukatpali', 'naga', 'nag@gmail.com', 214748364, '2019-05-24 12:06:42', '2019-05-24 12:03:12');

-- --------------------------------------------------------

--
-- Table structure for table `client_url_info`
--

CREATE TABLE IF NOT EXISTS `client_url_info` (
`id` int(3) NOT NULL,
  `project_id` varchar(50) NOT NULL,
  `clint_id` varchar(50) NOT NULL,
  `url_link` varchar(70) NOT NULL,
  `url_config_file_uid` varchar(50) NOT NULL,
  `url_config_file_pwd` varchar(50) NOT NULL,
  `url_config_db_uid` varchar(50) NOT NULL,
  `url_config_db_pwd` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `client_url_info`
--

INSERT INTO `client_url_info` (`id`, `project_id`, `clint_id`, `url_link`, `url_config_file_uid`, `url_config_file_pwd`, `url_config_db_uid`, `url_config_db_pwd`, `created`, `updated`) VALUES
(1, 'p1905soum215', '1905soum215', 'http://localhost/phpmyadmin/tbl_change.php?db=ticket_system&table=clie', 'fsdaasdf', 'dfsdfasd', 'jkjnbjkn', 'fsdfadsf', '2019-05-23 06:11:00', '2019-05-24 12:14:32'),
(2, 'p1905moun852', '1905moun495', 'https://www.webgrid.co.in/about.php', 'bgb564', 'dfsdf56', 'weafw', '123', '2019-05-24 22:15:05', '2019-05-24 11:39:20'),
(3, 'p1905anup159', '1905anup951', 'https://www.webgrid.co.in/website-designing.php', 'hfghfgd54', 'fdfasdf', 'ewed', '123', '2019-05-24 21:15:05', '2019-05-24 11:39:13'),
(4, 'p1905hema753', '1905hema264', 'https://www.webgrid.co.in/inventory-management-system.php', 'fgsfdg454', 'dsfsdf423423', 'erty', '123', '2019-05-24 18:15:05', '2019-05-24 11:39:11');

-- --------------------------------------------------------

--
-- Table structure for table `employee_profile`
--

CREATE TABLE IF NOT EXISTS `employee_profile` (
`id` int(3) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `emp_name` varchar(50) NOT NULL,
  `emp_pic` varchar(60) NOT NULL,
  `emp_mail` varchar(50) NOT NULL,
  `emp_gender` int(2) NOT NULL,
  `user_id` int(2) NOT NULL,
  `phone_no` varchar(13) NOT NULL,
  `dob` date NOT NULL,
  `doj` date NOT NULL,
  `address` varchar(150) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employee_profile`
--

INSERT INTO `employee_profile` (`id`, `emp_id`, `emp_name`, `emp_pic`, `emp_mail`, `emp_gender`, `user_id`, `phone_no`, `dob`, `doj`, `address`, `created`, `updated`) VALUES
(1, '1905tech256', 'sandeep', '1905sanpweb.jpg', 'sandeep@gmail.com', 1, 1, '9988998899', '0000-00-00', '0000-00-00', '', '2019-05-24 00:00:00', '2019-05-25 10:24:04'),
(2, '1905testl852', 'anil', '1905anil852.jpg', 'anil@gmail.com', 1, 2, '9988998898', '0000-00-00', '0000-00-00', '', '2019-05-24 00:00:00', '2019-05-25 10:24:17'),
(3, '1905tehd325', 'sidhu', '1905sidhu325.jpg', 'sidhu@gmail.com', 1, 4, '9988998888', '0000-00-00', '0000-00-00', '', '2019-05-24 00:00:00', '2019-05-25 10:24:23');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(3) NOT NULL,
  `gender_type` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `gender_type`) VALUES
(1, 'male'),
(2, 'female'),
(3, 'transgende');

-- --------------------------------------------------------

--
-- Table structure for table `priority_info`
--

CREATE TABLE IF NOT EXISTS `priority_info` (
`id` int(3) NOT NULL,
  `ticket_id` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `priority` varchar(3) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `priority_info`
--

INSERT INTO `priority_info` (`id`, `ticket_id`, `emp_id`, `priority`, `created`, `updated`) VALUES
(1, '1905tick8811', '1905anil852', '2', '2019-05-24 00:00:00', '2019-05-24 07:22:20'),
(2, '1905tick7561', '1905sanp256', '1', '2019-05-24 00:00:00', '2019-05-24 12:22:06'),
(3, '1905tick3578', '1905sanp256', '2', '2019-05-24 00:00:00', '2019-05-24 07:22:20');

-- --------------------------------------------------------

--
-- Table structure for table `priority_list`
--

CREATE TABLE IF NOT EXISTS `priority_list` (
`id` int(3) NOT NULL,
  `priority_type` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `priority_list`
--

INSERT INTO `priority_list` (`id`, `priority_type`) VALUES
(1, 'critical'),
(2, 'urgent'),
(3, 'high'),
(4, 'medium'),
(5, 'low');

-- --------------------------------------------------------

--
-- Table structure for table `request_type`
--

CREATE TABLE IF NOT EXISTS `request_type` (
`id` int(3) NOT NULL,
  `reqirement_type` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `request_type`
--

INSERT INTO `request_type` (`id`, `reqirement_type`) VALUES
(1, 'no isue'),
(2, 'UI issue'),
(3, 'functionality'),
(4, 'page not opening'),
(5, 'slow loading'),
(6, 'server configuration'),
(7, 'new reqirement');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
`id` int(3) NOT NULL,
  `ticket_id` varchar(50) NOT NULL,
  `project_id` varchar(50) NOT NULL,
  `request_type` varchar(3) NOT NULL,
  `url_link` varchar(150) NOT NULL,
  `ticket_sub` varchar(50) NOT NULL,
  `ticket_discription` text NOT NULL,
  `ticket_pics` varchar(60) NOT NULL,
  `ticket_priority` varchar(3) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `ticket_id`, `project_id`, `request_type`, `url_link`, `ticket_sub`, `ticket_discription`, `ticket_pics`, `ticket_priority`, `created`, `updated`) VALUES
(1, '1905tick3561', 'p1905soum215', '3', 'http://generatorshub.co.in/ecom/account-addresses.php', 'issue with upload', 'Trying to add address not working', '1905tick7561.png', '1', '2019-05-24 00:00:00', '2019-05-24 13:17:47'),
(2, '1905tick4811', 'p1905moun852', '4', 'http://generatorshub.co.in/ecom/product_details.php', 'image', 'Image Not Displaying', '1905tick8811.png', '1', '2019-05-24 00:00:00', '2019-05-24 13:17:37'),
(3, '1905tick3578', 'p1905hema753', '5', 'http://generatorshub.co.in/ecom/account_orders_history.php', 'data', 'wrong showing data', '1905tick3578.png', '2', '2019-05-24 00:00:00', '2019-05-24 12:12:15'),
(4, '1905tick2562', 'p1905soum215', '2', 'http://generatorshub.co.in/ecom/account-addresses.php', 'issue with upload', 'Trying to add address not working', '1905tick7561.png', '1', '2019-05-24 00:00:00', '2019-05-24 13:17:54'),
(5, '1905tick4812', 'p1905moun852', '4', 'http://generatorshub.co.in/ecom/product_details.php', 'image', 'Image Not Displaying', '1905tick8811.png', '1', '2019-05-24 00:00:00', '2019-05-24 13:17:29'),
(6, '1905tick5577', 'p1905anup159', '5', 'http://generatorshub.co.in/ecom/account_orders_history.php', 'data', 'wrong showing data', '1905tick3578.png', '2', '2019-05-24 00:00:00', '2019-05-24 13:17:14'),
(7, '1905tick2563', 'p1905soum215', '2', 'http://generatorshub.co.in/ecom/account-addresses.php', 'issue with upload', 'Trying to add address not working', '1905tick7561.png', '1', '2019-05-24 00:00:00', '2019-05-24 13:17:09'),
(8, '1905tick1813', 'p1905moun852', '1', 'http://generatorshub.co.in/ecom/product_details.php', 'image', 'Image Not Displaying', '1905tick8811.png', '1', '2019-05-24 00:00:00', '2019-05-24 13:17:03'),
(9, '1905tick1579', 'p1905soum215', '1', 'http://generatorshub.co.in/ecom/account_orders_history.php', 'data', 'wrong showing data', '1905tick3578.png', '2', '2019-05-24 00:00:00', '2019-05-24 13:16:58');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_history`
--

CREATE TABLE IF NOT EXISTS `tickets_history` (
`id` int(3) NOT NULL,
  `ticket_id` varchar(50) NOT NULL,
  `status_id` varchar(3) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tickets_history`
--

INSERT INTO `tickets_history` (`id`, `ticket_id`, `status_id`, `created`, `updated`) VALUES
(1, '1905tick7561', '3', '2019-05-24 00:00:00', '2019-05-24 07:04:14'),
(2, '1905tick8811', '2', '2019-05-24 00:00:00', '2019-05-24 07:04:14'),
(3, '1905tick3578', '2', '2019-05-24 00:00:00', '2019-05-25 04:15:23'),
(4, '1905tick2562', '2', '0000-00-00 00:00:00', '2019-05-25 04:21:41'),
(5, '1905tick4812', '2', '0000-00-00 00:00:00', '2019-05-25 04:21:41'),
(6, '1905tick5577', '2', '0000-00-00 00:00:00', '2019-05-25 04:22:28'),
(7, '1905tick1813', '2', '0000-00-00 00:00:00', '2019-05-25 04:22:28');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_assigned`
--

CREATE TABLE IF NOT EXISTS `ticket_assigned` (
`id` int(3) NOT NULL,
  `ticket_id` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `emp_roll_id` varchar(3) NOT NULL,
  `assigned_by` varchar(50) NOT NULL,
  `emp_tk_status` varchar(3) NOT NULL,
  `ticket_status` varchar(3) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `ticket_assigned`
--

INSERT INTO `ticket_assigned` (`id`, `ticket_id`, `emp_id`, `emp_roll_id`, `assigned_by`, `emp_tk_status`, `ticket_status`, `created`, `updated`) VALUES
(1, '1905tick8811', '1905anil852', '1', '3', '1', '1', '2019-05-24 00:00:00', '2019-05-25 04:14:29'),
(2, '1905tick3578', '1905sidhu325', '2', '3', '1', '1', '2019-05-24 00:00:00', '2019-05-25 04:14:38'),
(3, '1905tick3561', '1905testl852', '1', '3', '2', '2', '0000-00-00 00:00:00', '2019-05-25 04:18:00'),
(4, '1905tick2562', '1905tech256', '2', '3', '2', '2', '0000-00-00 00:00:00', '2019-05-25 04:20:16'),
(5, '1905tick4812', '1905testl852', '1', '3', '2', '2', '0000-00-00 00:00:00', '2019-05-25 04:19:14'),
(6, '1905tick5577', '1905tech256', '2', '3', '2', '2', '0000-00-00 00:00:00', '2019-05-25 04:19:14'),
(7, '1905tick1813', '1905testl852', '1', '3', '2', '2', '0000-00-00 00:00:00', '2019-05-25 04:19:56'),
(8, '1905tick2563', '1905tech256', '2', '3', '2', '2', '0000-00-00 00:00:00', '2019-05-25 04:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_messages`
--

CREATE TABLE IF NOT EXISTS `ticket_messages` (
`id` int(3) NOT NULL,
  `ticket_id` varchar(50) NOT NULL,
  `emp_id` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ticket_messages`
--

INSERT INTO `ticket_messages` (`id`, `ticket_id`, `emp_id`, `message`, `created`, `updated`) VALUES
(1, '1905tick8811', '1905sidhu325', 'look in to it', '2019-05-24 00:00:00', '2019-05-24 07:17:34'),
(2, '1905tick7561', '1905sanp256', 'approved kyc and showing back', '2019-05-24 00:00:00', '2019-05-24 07:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_status`
--

CREATE TABLE IF NOT EXISTS `ticket_status` (
`id` int(3) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ticket_status`
--

INSERT INTO `ticket_status` (`id`, `status`) VALUES
(1, 'on hold'),
(2, 'prograssin'),
(3, 'testing'),
(4, 'resolved'),
(5, 'closed');

-- --------------------------------------------------------

--
-- Table structure for table `userlogin`
--

CREATE TABLE IF NOT EXISTS `userlogin` (
`id` int(3) NOT NULL,
  `user_id` varchar(50) NOT NULL,
  `user_pwd` varchar(50) NOT NULL,
  `user_type` int(3) NOT NULL,
  `status` int(3) NOT NULL,
  `logintimes` int(3) NOT NULL,
  `last_login` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `userlogin`
--

INSERT INTO `userlogin` (`id`, `user_id`, `user_pwd`, `user_type`, `status`, `logintimes`, `last_login`, `updated`) VALUES
(1, '1905soum145', '1905soum145', 7, 1, 1, '2019-05-24 00:00:00', '2019-05-25 10:30:50'),
(2, '1905moun495', '1905moun495', 7, 1, 2, '2019-05-23 00:00:00', '2019-05-25 10:30:50'),
(3, '1905anup951', '1905anup951', 7, 1, 1, '2019-05-24 00:00:00', '2019-05-25 10:31:31'),
(4, '1905hema264', '1905hema264', 7, 1, 2, '2019-05-23 00:00:00', '2019-05-25 10:31:31'),
(5, '1905tech256', '1905tech256', 1, 1, 1, '2019-05-22 00:00:00', '2019-05-25 10:36:17'),
(6, '1905testl852', '1905testl852', 2, 1, 1, '2019-05-22 00:00:00', '2019-05-25 10:37:25'),
(7, '1905tehd325', '1905tehd325', 4, 1, 1, '2019-05-22 00:00:00', '2019-05-25 10:37:11');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
`id` int(3) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_logo` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_name`, `user_logo`, `created`, `updated`) VALUES
(1, 'deveploment', 'dfd.png', '2019-05-24 00:00:00', '2019-05-24 12:21:19'),
(2, 'testing', 'dfd.png', '2019-05-24 00:00:00', '2019-05-24 12:21:14'),
(3, 'designing', 'dfd.png', '2019-05-24 00:00:00', '2019-05-24 12:21:09'),
(4, 'technical lead', 'dfd.jpg', '2019-05-15 00:00:00', '2019-05-24 12:20:53'),
(5, 'project manager', 'dfd.jpg', '2019-05-23 00:00:00', '2019-05-24 12:20:57'),
(7, 'clients', 'clients.png', '2019-05-17 00:00:00', '2019-05-25 10:32:51');

-- --------------------------------------------------------

--
-- Table structure for table `web_grid_info`
--

CREATE TABLE IF NOT EXISTS `web_grid_info` (
`id` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `company_logo` varchar(60) NOT NULL,
  `url` varchar(150) NOT NULL,
  `mail_id` varchar(60) NOT NULL,
  `phone_no` varchar(50) NOT NULL,
  `adsress` varchar(200) NOT NULL,
  `ifcs_code` varchar(15) NOT NULL,
  `account_no` int(30) NOT NULL,
  `bank_name` varchar(30) NOT NULL,
  `branch` varchar(30) NOT NULL,
  `created` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `web_grid_info`
--

INSERT INTO `web_grid_info` (`id`, `name`, `company_logo`, `url`, `mail_id`, `phone_no`, `adsress`, `ifcs_code`, `account_no`, `bank_name`, `branch`, `created`, `updated`) VALUES
(1, 'web gride', '', 'webgrid.co.in', ' info@webgrid.co.in', '9100508543', 'A - 46, Madhuranagar, Ameerpet, Hyderabad', 'SBI0844545', 4554654, 'state bank of india', 'hyderabad', '2019-05-23 00:00:00', '2019-05-24 11:54:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clients_budget`
--
ALTER TABLE `clients_budget`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients_profile`
--
ALTER TABLE `clients_profile`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients_project_details`
--
ALTER TABLE `clients_project_details`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_url_info`
--
ALTER TABLE `client_url_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_profile`
--
ALTER TABLE `employee_profile`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priority_info`
--
ALTER TABLE `priority_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priority_list`
--
ALTER TABLE `priority_list`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_type`
--
ALTER TABLE `request_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets_history`
--
ALTER TABLE `tickets_history`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_assigned`
--
ALTER TABLE `ticket_assigned`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_messages`
--
ALTER TABLE `ticket_messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ticket_status`
--
ALTER TABLE `ticket_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userlogin`
--
ALTER TABLE `userlogin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_grid_info`
--
ALTER TABLE `web_grid_info`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clients_budget`
--
ALTER TABLE `clients_budget`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `clients_profile`
--
ALTER TABLE `clients_profile`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clients_project_details`
--
ALTER TABLE `clients_project_details`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `client_url_info`
--
ALTER TABLE `client_url_info`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee_profile`
--
ALTER TABLE `employee_profile`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `priority_info`
--
ALTER TABLE `priority_info`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `priority_list`
--
ALTER TABLE `priority_list`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `request_type`
--
ALTER TABLE `request_type`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tickets_history`
--
ALTER TABLE `tickets_history`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ticket_assigned`
--
ALTER TABLE `ticket_assigned`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ticket_messages`
--
ALTER TABLE `ticket_messages`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ticket_status`
--
ALTER TABLE `ticket_status`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `userlogin`
--
ALTER TABLE `userlogin`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `web_grid_info`
--
ALTER TABLE `web_grid_info`
MODIFY `id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
